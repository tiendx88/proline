<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tinh_thanh_pho".
 *
 * @property integer $id
 * @property string $ten_tinh
 * @property string $ten_viet_tat
 *
 * @property QuanHuyen[] $quanHuyens
 */
class TinhThanhPho extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tinh_thanh_pho';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ten_tinh'], 'required'],
            [['ten_tinh'], 'string', 'max' => 100],
            [['ten_viet_tat'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ten_tinh' => 'Tên tỉnh, thành phố',
            'ten_viet_tat' => 'Ten Viet Tat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuanHuyens()
    {
        return $this->hasMany(QuanHuyen::className(), ['id_tinh_tp' => 'id']);
    }

    public static function getList() {
        return TinhThanhPho::find()->all();
    }

    public static function getName($id) {
        $model = TinhThanhPho::findOne(['id' => $id]);
        return (!empty($model) ? $model->ten_tinh: '');
    }
}
