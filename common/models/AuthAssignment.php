<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "v2_auth_assignment".
 *
 * @property  $item_name
 * @property  $user_id
 * @property  $created_at
 *
 * @property AuthItem $1
 */
class AuthAssignment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_asm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'integer']
        ];
    }

    public function beforeSave($insert) {
        $this->created_at = time();
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_name' => Yii::t('app', 'Item Name'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get1()
    {
        return $this->hasOne(AuthItem::className(), ['name' => '1']);
    }
}
