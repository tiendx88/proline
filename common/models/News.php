<?php

namespace common\models;

use common\helpers\VietnameseUtils;
use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $title_en
 * @property string $title_ascii
 * @property string $content
 * @property string $content_en
 * @property string $short_description
 * @property string $short_description_en
 * @property integer $honor
 * @property string $img_thumbnail
 * @property integer $view_count
 * @property integer $status
 * @property integer $weight
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    const STATUS_ACTIVE = 10;//da duyet
    const STATUS_DISABLE = 2;//xoa logic khoi he thong
    const STATUS_INACTIVE_NAME = 'Chưa duyệt';
    const STATUS_ACTIVE_NAME = 'Đã duyệt';
    const STATUS_DISABLE_NAME = 'Đã khóa';

    public static $STATUS = [
        0=> self::STATUS_INACTIVE_NAME,
        Product::STATUS_ACTIVE =>  self::STATUS_ACTIVE_NAME,
        Product::STATUS_DISABLE => Product::STATUS_DISABLE_NAME
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'title_en', 'content', 'content_en', 'img_thumbnail'], 'string'],
            [['honor', 'view_count', 'status', 'weight', 'created_at', 'updated_at', 'created_by'], 'integer'],
            [['title_ascii'], 'string', 'max' => 100],
            [['short_description', 'short_description_en'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Tiêu đề (vi)',
            'title_en' => 'Tiêu đề (en)',
            'title_ascii' => 'Title Ascii',
            'content' => 'Nội dung (vi)',
            'content_en' => 'Nội dung (en)',
            'short_description' => 'Mô tả ngắn (vi)',
            'short_description_en' => 'Mô tả ngắn (en)',
            'honor' => 'Honor',
            'img_thumbnail' => 'Ảnh đại diện',
            'view_count' => 'Số lượt view',
            'status' => 'Trạng thái',
            'weight' => 'Weight',
            'created_at' => 'Ngày tạo',
            'updated_at' => 'Ngày cập nhật',
            'created_by' => 'Người tạo',
        ];
    }

    public function getStatusLabel() {
        if($this->status == self::STATUS_ACTIVE) {
            return self::STATUS_ACTIVE_NAME;
        } else if($this->status == self::STATUS_DISABLE) {
            return self::STATUS_DISABLE_NAME;
        } else {
            return self::STATUS_INACTIVE_NAME;
        }
    }

    public function getThumb()
    {
        if(!empty($this->img_thumbnail)) {
            return Yii::$app->params['be'] . 'uploads/images/' . $this->img_thumbnail;
        }
        return Yii::$app->params['be'] . 'images/default-image.png';
    }

    public function getName() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->title_en;
        } else {
            return $this->title;
        }
    }

    public function getShortDesc() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->short_description_en;
        } else {
            return $this->short_description;
        }
    }

    public function getDescription() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->short_description_en;
        } else {
            return $this->short_description;
        }
    }

    public function getContent() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->content_en;
        } else {
            return $this->content;
        }
    }

    public function getTitle() {
        $str = strtolower(str_replace('“',"",VietnameseUtils::removeSigns($this->getName())));
        $str = strtolower(str_replace("”","",$str));
        $str = strtolower(str_replace("","",$str));
        $str = strtolower(str_replace("`","",$str));
        $str = strtolower(str_replace("\\","",$str));
        $str = strtolower(str_replace("/","",$str));
        $str = strtolower(str_replace("@","",$str));
        $str = strtolower(str_replace("#","",$str));
        $str = strtolower(str_replace("&","",$str));
        $str = strtolower(str_replace("%","",$str));
        $str = strtolower(str_replace("*","",$str));
        $str = strtolower(str_replace("'","",$str));

        return str_replace(' ','-',$str);
    }
}
