<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\rbac\Item;
use yii\redis\ActiveQuery;

/**
 * This is the model class for table "v2_auth_item".
 *
 * @property  $name
 * @property  $type
 * @property  $description
 * @property  $rule_name
 * @property  $data
 * @property  $created_at
 * @property  $updated_at
 *
 * @property AuthRule $1
 * @property AuthItemChild[] $authItemChildren
 * @property AuthItem[] $children
 * @property AuthItem[] $parent
 * @property AuthAssignment[] $authAssignments
 */
class AuthItem extends \yii\db\ActiveRecord
{
    const TYPE_ROLE = Item::TYPE_ROLE;
    const TYPE_PERMISSION = Item::TYPE_PERMISSION;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'name'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string', 'max' => 4000],
            [['rule_name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
            'rule_name' => Yii::t('app', 'Rule Name'),
            'data' => Yii::t('app', 'Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public static function findPermission() {
        return AuthItem::find()->andWhere(['type' => AuthItem::TYPE_PERMISSION]);
    }

    /**
     * @return ActiveQuery
     */
    public static function findRole() {
        return AuthItem::find()->andWhere(['type' => AuthItem::TYPE_ROLE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get1()
    {
        return $this->hasOne(AuthRule::className(), ['name' => '1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren()
    {
        return $this->hasMany(AuthItemChild::className(), ['1' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['1' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'child'])->viaTable('auth_item_child', ['parent' => 'name']);
    }


    /**
     * @return ActiveDataProvider
     */
    public function getChildrenProvider()
    {
        return new ActiveDataProvider([
            'query' => $this->getChildren()
        ]);
    }

    /**
     * @return ActiveDataProvider
     */
    public function getParentProvider()
    {
        return new ActiveDataProvider([
            'query' => $this->getParent()
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'parent'])->viaTable('auth_item_child', ['child' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMissingRoles()
    {
        return AuthItem::find()->andWhere(['type' => AuthItem::TYPE_ROLE])
            ->andWhere('name != :name',[':name' => $this->name])
            ->andWhere('name not in (select child from auth_item_child where parent = :name)',[':name' => $this->name])
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMissingPermissions()
    {
        return AuthItem::find()->andWhere(['type' => AuthItem::TYPE_PERMISSION])
            ->andWhere('name != :name',[':name' => $this->name])
            ->andWhere('name not in (select child from auth_item_child where parent = :name)',[':name' => $this->name])
            ->all();
    }
}
