<?php

namespace common\models;

use common\helpers\VietnameseUtils;
use Yii;

/**
 * This is the model class for table "about".
 *
 * @property integer $id
 * @property integer $status
 * @property string $title
 * @property string $image
 * @property string $content
 * @property string $title_en
 * @property string $content_en
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about';
    }


    const STATUS_ACTIVE = 10;//da duyet
    const STATUS_DISABLE = 2;//xoa logic khoi he thong
    const STATUS_INACTIVE_NAME = 'Chưa duyệt';
    const STATUS_ACTIVE_NAME = 'Đã duyệt';
    const STATUS_DISABLE_NAME = 'Đã khóa';

    public static $STATUS = [
        0=> self::STATUS_INACTIVE_NAME,
        Product::STATUS_ACTIVE =>  self::STATUS_ACTIVE_NAME,
        Product::STATUS_DISABLE => Product::STATUS_DISABLE_NAME
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'content_en'], 'string'],
            [['status','title', 'image', 'title_en'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Tiêu đề (vi)',
            'image' => 'Ảnh đại diện',
            'content' => 'Nội dung (vi)',
            'title_en' => 'Tiêu đề (en)',
            'content_en' => 'Nội dung (en)',
            'status' => 'Trạng thái',
        ];
    }

    public function getStatusLabel() {
        if($this->status == self::STATUS_ACTIVE) {
            return self::STATUS_ACTIVE_NAME;
        } else if($this->status == self::STATUS_DISABLE) {
            return self::STATUS_DISABLE_NAME;
        } else {
            return self::STATUS_INACTIVE_NAME;
        }
    }

    public function getThumb()
    {
        if(!empty($this->image)) {
            return Yii::$app->params['be'] . 'uploads/images/' . $this->image;
        }
        return Yii::$app->params['be'] . 'images/default-image.png';
    }

    public function getTitle() {
        $str = strtolower(str_replace('“',"",VietnameseUtils::removeSigns($this->title)));
        $str = strtolower(str_replace("”","",$str));
        $str = strtolower(str_replace("'","",$str));

        return str_replace(' ','-',$str);
    }

    public function getName() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->title_en;
        } else {
            return $this->title;
        }
    }

    public function getPartner() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return "Our partners";
        } else {
            return "Đối tác của chúng tôi";
        }
    }

    public function getContent() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->content_en;
        } else {
            return $this->content;
        }
    }
}
