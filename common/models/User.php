<?php

namespace common\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $full_name
 * @property string $ascii_name
 * @property integer $type
 * @property string $password_hash
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $email
 * @property integer $email_verified
 * @property string $address
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $phone_number
 * @property string $avartar
 * @property string $cover_photo
 * @property integer $role
 * @property string $facebook_id
 * @property string $facebook_data
 * @property string $google_id
 * @property string $google_data
 * @property integer $content_count
 * @property integer $follower_count
 * @property integer $followee_count
 * @property double $balance
 *
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const ROLE_ADMIN = 1;
    const ROLE_ACCPOUNTER = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password_hash', 'auth_key'], 'required'],
            [['type', 'status', 'created_at', 'updated_at', 'email_verified', 'role', 'content_count', 'follower_count', 'followee_count'], 'integer'],
            [['facebook_data', 'google_data'], 'string'],
            [['balance'], 'number'],
            [['username', 'full_name', 'ascii_name', 'password_hash', 'password_reset_token', 'facebook_id', 'google_id'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 1024],
            [['auth_key'], 'string', 'max' => 32],
            [['phone_number'], 'string', 'max' => 45],
            [['avartar', 'cover_photo'], 'string', 'max' => 512],
            [['email'], 'unique'],
            [['facebook_id'], 'unique'],
            [['google_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Tên đăng nhập',
            'full_name' => 'Tên đầy đủ',
            'ascii_name' => 'Ascii Name',
            'type' => 'Loại người dùng',
            'password_hash' => 'Mật khẩu',
            'status' => 'Trạng thái',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'email' => 'Email',
            'email_verified' => 'Email Verified',
            'address' => 'Address',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'phone_number' => 'Số điện thoại',
            'avartar' => 'Avartar',
            'cover_photo' => 'Cover Photo',
            'role' => 'Vai trò',
            'facebook_id' => 'Facebook ID',
            'facebook_data' => 'Facebook Data',
            'google_id' => 'Google ID',
            'google_data' => 'Google Data',
            'content_count' => 'Content Count',
            'follower_count' => 'Follower Count',
            'followee_count' => 'Followee Count',
            'balance' => 'Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAsms()
    {
        return $this->hasMany(AuthAsm::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('auth_asm', ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
