<?php

namespace common\models;

use common\helpers\VietnameseUtils;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "menu".
 *
 * @property integer $category_id
 * @property string $category_name
 * @property string $category_name_en
 * @property integer $weight
 * @property integer $status
 * @property string $img_thumbnail
 * @property string $url
 * @property integer $parent_id
 * @property integer $type
 *
 *
 */
class Menu extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 10;//da duyet
    const STATUS_DISABLE = 2;//xoa logic khoi he thong
    const STATUS_INACTIVE_NAME = 'Chưa duyệt';
    const STATUS_ACTIVE_NAME = 'Đã duyệt';
    const STATUS_DISABLE_NAME = 'Đã khóa';
    const TYPE_LEVEL_PRODUCT = 1;
    const TYPE_LEVEL_PRODUCT_NAME = 'Trình độ học vấn';
    const TYPE_LEVEL_OTHERS = -1;
    const TYPE_LEVEL_OTHERs_NAME = 'Danh mục khác';

    public static $_LIST_TYPE = [self::TYPE_LEVEL_OTHERS => self::TYPE_LEVEL_OTHERs_NAME, self::TYPE_LEVEL_PRODUCT => self::TYPE_LEVEL_PRODUCT_NAME];
    public static $_LIST_STATUS = ['0' => self::STATUS_INACTIVE_NAME, self::STATUS_ACTIVE => self::STATUS_ACTIVE_NAME, self::STATUS_DISABLE => self::STATUS_DISABLE_NAME, ];


    public $children = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name', 'status'], 'required'],
            [['weight', 'status', 'parent_id','type'], 'integer'],
            //[['img_thumbnail'],'required', 'on' => 'create'],
            [['category_name', 'status','category_name_en'],'required', 'on' => 'create'],
            [['img_thumbnail','category_name_en'], 'string'],
            [['img_thumbnail'], 'safe'],
            [['category_name'], 'string', 'max' => 100],
            [['url'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'ID',
            'category_name' => 'Tên menu(VI)',
            'category_name_en' => 'Tên menu (EN)',
            'weight' => 'Trọng số sắp xếp',
            'status' => 'Trạng thái',
            'img_thumbnail' => 'Ảnh đại diện',
            'parent_id' => 'Menu cha',
            'children' => 'Menu con',
            'type' => 'Loại menu',
            'url' => 'Link truy cập',
        ];
    }

    public function getName() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            $str = VietnameseUtils::removeSigns(str_replace(" ","-", strtolower($this->category_name_en)));
        } else {
            $str = VietnameseUtils::removeSigns(str_replace(" ","-", strtolower($this->category_name)));
        }

        return str_replace(' ','-',$str);
    }

    public static function getListCateParent()
    {
        $arrayCate = [];
        $list = Menu::find()->where(['status' => self::STATUS_ACTIVE,'type'=> self::TYPE_LEVEL_PRODUCT])->andWhere(['is','parent_id', null])->all();
        $lang = Yii::$app->session->get("lang","vi");
        if (null != $list) {
            foreach ($list as $cate) {
                /**@var $cate Menu */
                $arrayCate[$cate->category_id] = ($lang == 'vi') ? $cate->category_name : $cate->category_name_en;
            }
        }

        return $arrayCate;
    }

    public static function getListChildByParent2($parent)
    {
        $arrayCate = [];
        $list = Menu::find()->where(['status' => self::STATUS_ACTIVE,'parent_id'=> $parent])->orderBy('weight asc, category_name asc')->all();
        $lang = Yii::$app->session->get("lang","vi");
        if (null != $list) {
            foreach ($list as $cate) {
                /**@var $cate Menu */
                /**@var $child Menu */
                //$arrayCate[$cate->category_id] = $cate->category_name;
                $child = Menu::find()->where(['parent_id' => $cate->category_id])->orderBy('weight asc, category_name asc')->all();
                if(!empty($child)) {
                    foreach($child as $c) {
                        $arrayCate["$c->category_id"] = ($lang == 'vi') ? $cate->category_name . '--' . $c->category_name : $cate->category_name_en . '--' . $c->category_name_en;
                    }
                } else {
                    $arrayCate["$cate->category_id"] = ($lang == 'vi') ?$cate->category_name : $cate->category_name_en;
                }

            }
        }

        return $arrayCate;
    }


    public static function getListOther($id = 0)
    {
        $lang = Yii::$app->session->get("lang","vi");
        $arrayCate = [];
        $list = null;
        if (0 != $id) {
            $list = Menu::find()->where('1=1')->andWhere(['!=', 'category_id', $id])->andWhere([' = ', 'status', Category::STATUS_ACTIVE])->all();
        } else {
            $list = Menu::findAll(['status' => 10]);
        }

        if (null != $list) {
            foreach ($list as $cate) {
                /**@var $cate Menu */
                $arrayCate[$cate->category_id] = ($lang == 'vi') ? $cate->category_name : $cate->category_name_en;
            }
        }

        return $arrayCate;
    }

    public function getStatusLabel()
    {
        if ($this->status == self::STATUS_ACTIVE) {
            return self::STATUS_ACTIVE_NAME;
        } else if ($this->status == self::STATUS_DISABLE) {
            return self::STATUS_DISABLE_NAME;
        } else {
            return self::STATUS_INACTIVE_NAME;
        }
    }

    public function getCategoryName($id)
    {
        if (empty($id)) {
            $cate = new Menu();
            $cate->category_name = 'root';
            $cate->category_name_en = 'root';
            $cate->category_id = 0;
            return $cate;
        }
        return Menu::findOne(['category_id' => $id]);
    }

    public function getThumbnail()
    {
        return Yii::$app->params['be'] . $this->img_thumbnail;
    }

    public static function getStringMenuLeft() {
        $strUrl = '';
        $aryCategories = Menu::find()->andWhere(['status' => Menu::STATUS_ACTIVE])->orderBy('weight asc, category_name asc, parent_id asc')->all();
        if (!empty($aryCategories)) {
            $parentGroup = self::groupParent($aryCategories);//get group parent
            $listData = self::groupParentOfParent($parentGroup);//buil tree
            self::buildCategories($strUrl, $listData);
        }
        return $strUrl;
    }

    /**
     * @param $data
     * @param $listCategories
     * @return array
     */
    public static function buildCategories(&$str, &$listCategories)
    {
        $lang = Yii::$app->session->get("lang","vi");
        $route = Yii::$app->controller->route;
        /** @var $item Menu */
        foreach ($listCategories as $item) {
            $str .= "<li class='nav-item ";
            $tm = ($route == $item->url) ? "active" : "";
            $str .= $tm;
            $str .="'><a href='" . Url::toRoute([$item->url]) . "'>";
            $str .= $item->category_name;
            $str .= '</a>';
            if (!empty($item->children)) {
                $str .= '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a><div class="dropdown-menu" aria-labelledby="navbarDropdown">';
                $str .= '<a class="dropdown-item" href="#" >';
                self::buildCategories($str, $item->children);
                $str .= '</div>';
                $str .= '</a>';
            }
            $str .= '</li>';
        }
    }

    /**
     * Thực hiện
     * @param $parentGroup
     * @return mixed
     */
    public static function groupParentOfParent(&$parentGroup)
    {
        foreach ($parentGroup as $item1) {
            foreach ($parentGroup as &$item2) {
                if (!empty($item2->children)) {
                    /** @var $it Category */
                    foreach ($item2->children as &$it) {
                        if ($item1->category_id == $it->category_id) {
                            $it->children = $item1->children;
                            $parentGroup = self::removeItemArray($parentGroup, $item1);
                            self::groupParentOfParent($parentGroup);
                        }
                    }
                }

            }
        }
        return $parentGroup;
    }

    /**
     * Build lai mang
     *
     * @param $array
     * @param $item
     * @return array
     */
    public static function removeItemArray(&$array, $item)
    {
        $data = array();
        if (count($array) > 0) {
            foreach ($array as $it) {
                if ($item->category_id != $it->category_id) {//khong lay phan tu da duoc dua vao trong children
                    array_push($data, $it);
                }
            }
        }
        return $data;

    }

    /**
     * tra ve group parent
     *
     * @param $listCategory
     * @return array
     */
    public static function groupParent($listCategory)
    {
        $lang = Yii::$app->session->get("lang","vi");
        //var_dump($lang);exit;
        $arrayGroup = array();
        /** @var $cate Menu */
        foreach ($listCategory as $cate) {
            $children = array();
            $i = 0;
            /** @var  $item Menu */
            foreach ($listCategory as $item) {
                if ($cate->category_id == $item->parent_id) {
                    $i++;
                    array_push($children, $item);
                    unset($listCategory[$i - 1]);
                }
            }
            if (!empty($children) || empty($cate->parent_id)) {
                $ca = new Menu();
                $ca->category_id = $cate->category_id;
                $ca->category_name = ($lang == 'vi') ? $cate->category_name : $cate->category_name_en;
                $ca->img_thumbnail = $cate->img_thumbnail;
                $ca->children = $children;
                $ca->url = $cate->url;
                array_push($arrayGroup, $ca);
            }
        }

        return $arrayGroup;
    }
}
