<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property integer $id
 * @property string $contact_us_vi
 * @property string $contact_us_en
 * @property string $visit_vi
 * @property string $visit_en
 * @property string $work_with_vi
 * @property string $work_with_en
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_us_vi', 'contact_us_en', 'visit_vi', 'visit_en', 'work_with_vi', 'work_with_en'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contact_us_vi' => 'Liên hệ với chúng tôi (vi)',
            'contact_us_en' => 'Liên hệ với chúng tôi (en)',
            'visit_vi' => 'Địa chỉ (vi)',
            'visit_en' => 'Địa chỉ (en)',
            'work_with_vi' => 'Làm việc với chúng tôi (vi)',
            'work_with_en' => 'Làm việc với chúng tôi (en)',
        ];
    }

    public function getTitle() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return 'Contact with Profline';
        } else {
            return 'Thông tin liên hệ - Profline';
        }
    }

    public function getVisit() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->visit_en;
        } else {
            return $this->visit_vi;
        }
    }

    public function getContactUs() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->contact_us_en;
        } else {
            return $this->contact_us_vi;
        }
    }

    public function getWorkWithUs() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->work_with_en;
        } else {
            return $this->work_with_vi;
        }
    }
}
