<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;

/**
 * ProductSearch represents the model behind the search form about `common\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'like_count', 'status'], 'integer'],
            [['size_en','name_vi', 'name_en', 'name_ascii', 'short_description_vi', 'short_description_en', 'description_vi', 'description_en', 'size_vi', 'feature_vi', 'feature_en'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'like_count' => $this->like_count,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name_vi', $this->name_vi])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'name_ascii', $this->name_ascii])
            ->andFilterWhere(['like', 'short_description_vi', $this->short_description_vi])
            ->andFilterWhere(['like', 'short_description_en', $this->short_description_en])
            ->andFilterWhere(['like', 'description_vi', $this->description_vi])
            ->andFilterWhere(['like', 'description_en', $this->description_en])
            ->andFilterWhere(['like', 'size_vi', $this->size_vi])
            ->andFilterWhere(['like', 'size_en', $this->size_en])
            ->andFilterWhere(['like', 'feature_vi', $this->feature_vi])
            ->andFilterWhere(['like', 'feature_en', $this->feature_en]);

        return $dataProvider;
    }
}
