<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $type
 * @property string $url
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images';
    }

    const _THUMBNAIL = 0;
    const _SCREENS_SHORT = 1;
    const _SLIDER = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'type'], 'integer'],
            [['url'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'type' => 'Type',
            'url' => 'Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getImage() {
        return Yii::$app->params['be'] . 'uploads/images/' . $this->url;
    }
}
