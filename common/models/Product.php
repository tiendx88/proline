<?php

namespace common\models;

use common\helpers\VietnameseUtils;
use Yii;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name_vi
 * @property string $name_en
 * @property string $name_ascii
 * @property string $short_description_vi
 * @property string $short_description_en
 * @property string $description_vi
 * @property string $description_en
 * @property string $size_vi
 * @property string $size_en
 * @property string $size
 * @property string $feature_vi
 * @property string $feature_en
 * @property integer $like_count
 * @property integer $status
 * @property Image[] $images
 * @property ProductCateAsm[] $productCateAsm
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public $category_id;

    /**
     * @var UploadedFile[]
     */
    public $img_slider;
    /**
     * @var UploadedFile
     */
    public $img_thumbnail;
    /**
     * @var UploadedFile
     */

    public $cate_parent;



    const STATUS_ACTIVE = 10;//da duyet
    const STATUS_DISABLE = 2;//xoa logic khoi he thong
    const STATUS_INACTIVE_NAME = 'Chưa duyệt';
    const STATUS_ACTIVE_NAME = 'Đã duyệt';
    const STATUS_DISABLE_NAME = 'Đã khóa';

    public static $STATUS = [
        0=> self::STATUS_INACTIVE_NAME,
        Product::STATUS_ACTIVE =>  self::STATUS_ACTIVE_NAME,
        Product::STATUS_DISABLE => Product::STATUS_DISABLE_NAME
    ];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description_vi', 'description_en', 'feature_vi', 'feature_en'], 'string'],
            [['like_count', 'status'], 'integer'],
            [['size','size_en','name_vi', 'name_en', 'name_ascii', 'short_description_vi', 'short_description_en', 'size_vi'], 'string', 'max' => 512],
            [['img_slider','img_thumbnail','category_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_vi' => 'Tên (vi)',
            'name_en' => 'Tên (en)',
            'name_ascii' => 'Name Ascii',
            'short_description_vi' => 'Mô tả ngắn (vi)',
            'short_description_en' => 'Mô tả ngắn (en)',
            'description_vi' => 'Mô tả (vi)',
            'description_en' => 'Mô tả (en)',
            'size' => 'Các kích cỡ',
            'size_vi' => 'Mô tả kích cỡ (vi)',
            'size_en' => 'Mô tả kích cỡ (en)',
            'feature_vi' => 'Đặc điểm (vi)',
            'feature_en' => 'Đặc điểm (en)',
            'like_count' => 'Số lượt thích',
            'status' => 'Trạng thái',
            'img_slider' => 'Ảnh slide',
            'img_thumbnail' => 'Ảnh đại diện',
            'category_id' => 'Danh mục sản phẩm',
        ];
    }

    public static function getList($id = null) {
        $sql = 'SELECT c.category_id , a.* from product a ';
        $sql .='INNER JOIN (product_cate_asm c inner join category ct on c.category_id=ct.category_id) on a.id=c.product_id ';
        $list_product = Product::findBySql($sql)->where(['1' => '1'])->andWhere(['=', 'ct.parent_id', $id])->all();
        return $list_product;
    }

    public function upload()
    {
        if ($this->validate()) {
            $index = 4;
            foreach ($this->img_slider as $file) {
                //$filename =  time() . "_" . CUtils::splitTitle(6, VietnameseUtils::removeSigns($file->baseName)). "_" . Yii::$app->security->generateRandomString(12) . '.'  . $file->extension;
                $filename = VietnameseUtils::removeSigns($this->getTitle()) . '-' . $index . '-' . time() . '.'  . $file->extension;
                $img = new Images();
                $img->url = $filename;
                $img->product_id = $this->id;
                $img->type = Images::_SLIDER;
                if($img->save()) {
                    //save file img_slider
                    $mime = $file->type;
                    $file->saveAs(Yii::getAlias('@webroot')."/tmp/{$filename}");//save origin image
                    if($mime == 'image/png') {
                        Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                            ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 9]);
                    } else {
                        Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                            ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 90]);
                    }

                    if (!empty($filename) && file_exists(Yii::getAlias('@webroot') . "/tmp/{$filename}")) {
                        unlink(Yii::getAlias('@webroot') . "/tmp/{$filename}");
                    }
                } else {
                    $this->addError('img_slider','Có lỗi khi upload ảnh screen short');
                    return false;
                }
                $index++;
            }

            //thumbnail
            $thumb = new Images();
            $thumb->product_id = $this->id;
            $thumb->type = Images::_THUMBNAIL;
            //$filename_thumb = time() . "_" . CUtils::splitTitle(6, VietnameseUtils::removeSigns($this->img_thumbnail->baseName)) . "_" . Yii::$app->security->generateRandomString(12) . '.' . $this->img_thumbnail->extension;
            $filename_thumb = VietnameseUtils::removeSigns($this->getTitle()) . '-'. Images::_THUMBNAIL . '-' . time() . '.' . $this->img_thumbnail->extension;
            $this->img_thumbnail->saveAs(Yii::getAlias('@webroot') . "/tmp/{$filename_thumb}");//save origin image
            $thumb->url = $filename_thumb;
            if(!$thumb->save()) {
                $this->addError('img_thumbnail','Có lỗi xảy ra khi upload ảnh thumbnail');
                return false;
            }
            //save file thumbnail
            $mime = $this->img_thumbnail->type;
            if($mime == 'image/png') {
                Image::thumbnail("@webroot/tmp/{$filename_thumb}", 600, 500)
                    ->save(Yii::getAlias("@webroot/uploads/images/{$filename_thumb}"), ['quality' => 9]);
            } else {
                Image::thumbnail("@webroot/tmp/{$filename_thumb}", 600, 500)
                    ->save(Yii::getAlias("@webroot/uploads/images/{$filename_thumb}"), ['quality' => 90]);
            }

            if (!empty($filename) && file_exists(Yii::getAlias('@webroot') . "/tmp/{$filename_thumb}")) {
                unlink(Yii::getAlias('@webroot') . "/tmp/{$filename_thumb}");
            }


            return true;
        } else {
            return false;
        }
    }

    public function uploadData()
    {
        if ($this->validate()) {
            if(null != $this->img_slider) {//screen short
                /** @var  $listScreen Images*/
                $listScreen = Images::findAll(['product_id' => $this->id,'type'=> Images::_SLIDER]);
                if(!empty($listScreen)) {
                    Images::deleteAll(['product_id' => $this->id,'type'=> Images::_SLIDER]);
                }

                $index = 4;
                foreach ($this->img_slider as $file) {
                    //$filename = time() . "_" . CUtils::splitTitle(6, VietnameseUtils::removeSigns($file->baseName)) . "_" . Yii::$app->security->generateRandomString(12) . '.' . $file->extension;
                    $filename = VietnameseUtils::removeSigns($this->getTitle()) . '-' . $index . '-' . time() . '.' . $file->extension;
                    $img = new Images();
                    $img->url = $filename;
                    $img->product_id = $this->id;
                    $img->type = Images::_SLIDER;
                    if ($img->save()) {
                        $index++;
                        //save file _SLIDER
                        $mime = $file->type;
                        $file->saveAs(Yii::getAlias('@webroot') . "/tmp/{$filename}");//save origin image
                        if ($mime == 'image/png') {
                            Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                                ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 9]);
                        } else {
                            Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                                ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 90]);
                        }

                        if (!empty($filename) && file_exists(Yii::getAlias('@webroot') . "/tmp/{$filename}")) {
                            unlink(Yii::getAlias('@webroot') . "/tmp/{$filename}");
                        }
                    } else {
                        $this->addError('imageFiles', 'Có lỗi khi upload ảnh screen short');
                        return false;
                    }
                }
            }

            //thumbnail
            if(null != $this->img_thumbnail) {
                $thumb = Images::findOne(['product_id' => $this->id, 'type' => Images::_THUMBNAIL]);
                if(null == $thumb) {
                    $thumb = new Images();
                }
                $thumb->product_id = $this->id;
                $thumb->type = Images::_THUMBNAIL;
                //$filename_thumb = time() . "_" . CUtils::splitTitle(6, VietnameseUtils::removeSigns($this->img_thumbnail->baseName)) . "_" . Yii::$app->security->generateRandomString(12) . '.' . $this->img_thumbnail->extension;
                $filename_thumb = VietnameseUtils::removeSigns($this->getTitle()) . '-' . Images::_THUMBNAIL . '-' . time() . '.' . $this->img_thumbnail->extension;
                $this->img_thumbnail->saveAs(Yii::getAlias('@webroot') . "/tmp/{$filename_thumb}");//save origin image
                $thumb->url = $filename_thumb;
                if (!$thumb->save()) {
                    $this->addError('img_thumbnail', 'Có lỗi xảy ra khi upload ảnh thumbnail');
                    if (!empty($filename) && file_exists(Yii::getAlias('@webroot') . "/tmp/{$filename_thumb}")) {
                        unlink(Yii::getAlias('@webroot') . "/tmp/{$filename_thumb}");
                    }
                    return false;
                }
                //save file thumbnail
                $mime = $this->img_thumbnail->type;
                if ($mime == 'image/png') {
                    Image::thumbnail("@webroot/tmp/{$filename_thumb}", 600, 500)
                        ->save(Yii::getAlias("@webroot/uploads/images/{$filename_thumb}"), ['quality' => 9]);
                } else {
                    Image::thumbnail("@webroot/tmp/{$filename_thumb}", 600, 500)
                        ->save(Yii::getAlias("@webroot/uploads/images/{$filename_thumb}"), ['quality' => 90]);
                }

                if (!empty($filename) && file_exists(Yii::getAlias('@webroot') . "/tmp/{$filename_thumb}")) {
                    unlink(Yii::getAlias('@webroot') . "/tmp/{$filename_thumb}");
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function getStatusLabel() {
        if($this->status == self::STATUS_ACTIVE) {
            return self::STATUS_ACTIVE_NAME;
        } else if($this->status == self::STATUS_DISABLE) {
            return self::STATUS_DISABLE_NAME;
        } else {
            return self::STATUS_INACTIVE_NAME;
        }
    }

    public function getThumb()
    {
        /** @var $thumb Images*/
        $thumb = Images::findOne(['product_id' => $this->id,'type' => Images::_THUMBNAIL]);
        if(!empty($thumb)) {
            return Yii::$app->params['be'] . 'uploads/images/' . $thumb->url;
        }
        return Yii::$app->params['be'] . 'images/default-image.png';
    }

    public function getScreenShort()
    {
        $listImage = Images::findAll(['product_id' => $this->id, 'type' => Images::_SCREENS_SHORT]);
        return $listImage;
    }
    public function getImageSlides()
    {
        $listImage = Images::findAll(['product_id' => $this->id, 'type' => Images::_SLIDER]);
        return $listImage;
    }

    public function getUrlScreenShort($url) {
        if(!empty($url)) {
            return Yii::$app->params['be'] . 'uploads/images/' . $url;
        }
        return Yii::$app->params['be'] . 'uploads/image/default-image.png';
    }

    public function getTitle() {
        $str = strtolower(str_replace('“',"",$this->name_ascii));
        $str = strtolower(str_replace("”","",$str));
        $str = strtolower(str_replace("","",$str));
        $str = strtolower(str_replace("`","",$str));
        $str = strtolower(str_replace("\\","",$str));
        $str = strtolower(str_replace("/","",$str));
        $str = strtolower(str_replace("@","",$str));
        $str = strtolower(str_replace("#","",$str));
        $str = strtolower(str_replace("&","",$str));
        $str = strtolower(str_replace("%","",$str));
        $str = strtolower(str_replace("*","",$str));
        $str = strtolower(str_replace("'","",$str));

        return str_replace(' ','-',$str);
    }

    public function getName() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->name_en;
        } else {
            return $this->name_vi;
        }
    }

    public function getDescription() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->description_en;
        } else {
            return $this->description_vi;
        }
    }

    public function getFeature() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->feature_en;
        } else {
            return $this->feature_vi;
        }
    }
    public function getSize() {
        $lang = Yii::$app->session->get("lang","vi");
        if($lang == "en") {
            return $this->size_en;
        } else {
            return $this->size_vi;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCateAsm()
    {
        return $this->hasMany(ProductCateAsm::className(), ['product_id' => 'id']);
    }
}
