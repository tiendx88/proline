<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Contacts;

/**
 * ContactsSearch represents the model behind the search form about `common\models\Contacts`.
 */
class ContactsSearch extends Contacts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['contact_us_vi', 'contact_us_en', 'visit_vi', 'visit_en', 'work_with_vi', 'work_with_en'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contacts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'contact_us_vi', $this->contact_us_vi])
            ->andFilterWhere(['like', 'contact_us_en', $this->contact_us_en])
            ->andFilterWhere(['like', 'visit_vi', $this->visit_vi])
            ->andFilterWhere(['like', 'visit_en', $this->visit_en])
            ->andFilterWhere(['like', 'work_with_vi', $this->work_with_vi])
            ->andFilterWhere(['like', 'work_with_en', $this->work_with_en]);

        return $dataProvider;
    }
}
