<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "quan_huyen".
 *
 * @property integer $id
 * @property string $ten_quan_huyen
 * @property string $ten_viet_tat
 * @property integer $id_tinh_tp
 *
 * @property TinhThanhPho $idTinhTp
 */
class QuanHuyen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quan_huyen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ten_quan_huyen'], 'required'],
            [['id_tinh_tp'], 'integer'],
            [['ten_quan_huyen'], 'string', 'max' => 512],
            [['ten_viet_tat'], 'string', 'max' => 200],
            [['id_tinh_tp'], 'exist', 'skipOnError' => true, 'targetClass' => TinhThanhPho::className(), 'targetAttribute' => ['id_tinh_tp' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ten_quan_huyen' => 'Tên quận, huyện',
            'ten_viet_tat' => 'Ten Viet Tat',
            'id_tinh_tp' => 'Tỉnh, thành phố',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTinhTp()
    {
        return $this->hasOne(TinhThanhPho::className(), ['id' => 'id_tinh_tp']);
    }
}
