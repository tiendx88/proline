<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'be'=>'http://be.profline.lc/',
    'lang_vi' =>'vi',
    'lang_en' =>'en'
];
