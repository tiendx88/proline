<?php
/**
 *
 * @author Nguyen Chi Thuc
 */

namespace common\helpers;

class ObjectUtils {

    /**
     * @param $obj
     * @param String[] $attrNames
     * @return array
     */
    public static function getObjectAttributes($obj, $attrNames)
    {
        $res = [];
        foreach ($attrNames as $key) {
            if (isset($obj[$key])) {
                $res[$key] = $obj[$key];
            }
        }
        return $res;
    }
}

?>
