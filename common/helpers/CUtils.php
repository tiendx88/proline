<?php
/**
 * Created by PhpStorm.
 * User: XuanTien
 * Date: 1/27/2016
 * Time: 9:58 PM
 */

namespace common\helpers;


use DateTime;
use Yii;
use yii\helpers\Json;

class CUtils
{

    /**
     * @param $requestId (TransactionId) Mã yêu cầu do CP tự sinh, là chuỗi duy nhất trong hệ thống của CP
     * @param $returnurl Giá trị này phải được URL encode và lower
     * @param $backurl giá trị này phải được URL encode và lower
     * @param string $Cp Cp ID do Trung tâm CNTT cung cấp
     * @param string $Service Mã dịch vụ  do Trung tâm CNTT cung cấp
     * @param string $Package Mã gói  do Trung tâm CNTT cung cấp
     * @param $requestdatetime theo định dạng: yyyyMMddHHmmss
     * @param $securecode MD5(requestid +  returnurl +  backurl  + cp + service +package +  requestdatetime +  channel  + securepass securepass: là mật mã an toàn  do Trung tâm CNTT cung cấp)
     * @param string $language ngôn ngữ cho wapgate
     */
    public static function vasgateRegisterVNP($requestId, $returnurl, $backurl, $requestdatetime, $channel='wap', $language='vi'){
        //MD5(requestid +  returnurl +  backurl  + cp + service +package +  requestdatetime +  channel  + securepass)
        $service = Yii::$app->params['service'];
        $cp = Yii::$app->params['cp'];
        $package = Yii::$app->params['package'];
        $secure = Yii::$app->params['secure'];
        $domain = Yii::$app->params['vasgate_reg_domain'];

        $securecode = md5($requestId . $returnurl . $backurl . $cp . $service . $package . $requestdatetime . $channel . $secure);
        //http://dk.vinaphone.com.vn/reg.jsp?requestid=[requestid]&returnurl=[returnurl]&backurl=[backurl]&cp=[cpid]&service=[service]&package=[package]
        // &requestdatetime=[requestdatetime] ]&channel=[web/wap/client]&securecode=[securecode]
        $url = $domain . 'requestid=' . $requestId . '&returnurl=' . $returnurl . '&backurl=' . $backurl;
        $url .= '&cp=' . $cp . '&service=' . $service . '&package=' . $package;
        $url .= '&requestdatetime=' . $requestdatetime . '&channel=' . $channel . '&securecode=' . $securecode;
        return $url;
    }

    /**
     * @param $requestId (TransactionId) Mã yêu cầu do CP tự sinh, là chuỗi duy nhất trong hệ thống của CP
     * @param $returnurl Giá trị này phải được URL encode và lower
     * @param $backurl giá trị này phải được URL encode và lower
     * @param string $Cp Cp ID do Trung tâm CNTT cung cấp
     * @param string $Service Mã dịch vụ  do Trung tâm CNTT cung cấp
     * @param string $Package Mã gói  do Trung tâm CNTT cung cấp
     * @param $requestdatetime theo định dạng: yyyyMMddHHmmss
     * @param $securecode MD5(requestid +  returnurl +  backurl  + cp + service +package +  requestdatetime +  channel  + securepass securepass: là mật mã an toàn  do Trung tâm CNTT cung cấp)
     * @param string $language ngôn ngữ cho wapgate
     */
    public static function vasgateUnRegisterVNP($requestId, $returnurl, $backurl, $requestdatetime, $channel='wap', $language='vi'){
        //MD5(requestid +  returnurl +  backurl  + cp + service +package +  requestdatetime +  channel  + securepass)
        $service = Yii::$app->params['service'];
        $cp = Yii::$app->params['cp'];
        $package = Yii::$app->params['package'];
        $secure = Yii::$app->params['secure'];
        $domain = Yii::$app->params['vasgate_unreg_domain'];

        $securecode = md5($requestId . $returnurl . $backurl . $cp . $service . $package . $requestdatetime . $channel . $secure);
        //http://dk.vinaphone.com.vn/unreg.jsp?requestid=[requestid]&returnurl=[returnurl]
        //&backurl=[backurl]&cp=[cpid]&service=[service]&package=[package]
        // &requestdatetime=[requestdatetime] ]&channel=[web/wap/client]&securecode=[securecode]
        $url = $domain . 'requestid=' . $requestId . '&returnurl=' . $returnurl . '&backurl=' . $backurl;
        $url .= '&cp=' . $cp . '&service=' . $service . '&package=' . $package;
        $url .= '&requestdatetime=' . $requestdatetime . '&channel=' . $channel . '&securecode=' . $securecode;
        return $url;
    }

    public static function formatResult($error, $message){
        return ['error'=> 0, 'message'=> $message];
    }

    public static function curlRequest($url, $params, $verb = 'GET') {
        header('Content-type: text/html; charset=utf-8');

        if(!empty(Yii::$app->user->id)) {
            $params['user_id'] = Yii::$app->user->id;
        }
        $ch = new MyCurl();
        $access = Yii::$app->session->get('access_token');
        if(!empty($access)) {
            //$params['access_token'] = urlencode($access);
            $ch->headers = [
                'Authorization' => 'Bearer ' . $access,
                'Accept' => 'application/json'
            ];
        }

        $results = null;
        if ($verb == 'GET') {
            $response = $ch->get($url, $params);
        }
        else {
            $response = $ch->post($url, $params);
        }

        if(!empty($response)) {
            $results = json_decode($response->body, true);
        }

        return $results;
    }

    public static function postUrl($url, $params) {
        return CUtils::curlRequest($url, $params, "POST");
    }

    public static function getUrl($url, $params) {
        return CUtils::curlRequest($url, $params, "GET");
    }

    /**
     * split title
     *
     * @param $length
     * @param $title
     * @return string
     */
    public static function splitTitle($length, $title) {
        if (strlen(trim($title)) > $length) {
            $title = mb_substr(trim($title), 0, $length - 3,"UTF-8") . '...';
        }
        return trim($title);
    }

    /**
     * tiendx
     * sql injection
     *
     * @param $param
     * @return unknown_type
     */
    public static function replace_string_injection($param) {
        if (empty($param)) return $param;
        $param = str_replace("\\", "\\\\\\", $param);
        $param = str_replace("'", "\'", $param);
        $param = str_replace("%", "\%", $param);
        return $param;
    }


    /**
     * list day
     *
     * @return array
     */
    public static function listDay() {
        $day = [];
        for($i = 1; $i <= 31 ; $i++) {
            $day[$i] = $i;
        }
        return $day;
    }

    /**
     * get list month
     * @return array
     */
    public static function listMonth() {
        $month = [];
        for($i = 1; $i <= 12 ; $i++) {
            $month[$i] = $i;
        }
        return $month;
    }

    /**
     * list year
     * @return array
     */
    public static function listYear() {
        $year = [];
        for($i = date('Y') - 70; $i <= date('Y') ; $i++) {
            $year[$i] = $i;
        }
        return $year;
    }
    /**
     * Log a msg as custom level "CUtils::Debug"
     * You need to add this level ("CUtils::Debug") to log component in config/main.php :
     * <code>
     * 'log'=>array(
     *		'class'=>'CLogRouter',
     *		'routes'=>array(
     *			array(
     *				'class'=>'CFileLogRoute',
     *				'levels'=>'error, warning, <b>CUtils::Debug</b>',
     *			),
     *			array('class'=>'CWebLogRoute',),
     *		),
     * </code>
     * @param string $msg
     */
    public static function Debug($msg, $category="Debug") {
        Yii::log($msg, 'CUtils::Debug', $category);
    }

    public static function randomString($length=32, $chars="abcdefghijklmnopqrstuvwxyz0123456789") {
        $max_ind = strlen($chars)-1;
        $res = "";
        for ($i =0; $i < $length; $i++) {
            $res .= $chars{rand(0, $max_ind)};
        }

        return $res;
    }

    public static function checkIPRange($ip) {
        $ipRanges = Yii::$app->params['ipRanges'];
        foreach ($ipRanges as $range) {
            if (CUtils::cidrMatch($ip, $range)) {
                return true;
            }
        }
        return false;
    }


    public static function encrypt($str) {
        return md5($str);
    }

    public static function timeElapsedString($ptime) {
        $etime = time() - $ptime;

        if ($etime < 1) {
            return '0 giây';
        }

        $a = array( 12 * 30 * 24 * 60 * 60  => 'năm',
            30 * 24 * 60 * 60       => 'tháng',
            24 * 60 * 60            => 'ngày',
            60 * 60                 => 'giờ',
            60                      => 'phút',
            1                       => 'giây'
        );

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . $str . ' trước';
            }
        }
    }

    public static function httpStatusOK($status_code){
        $pattern = '/^[54].*/';
        if(preg_match($pattern,$status_code)){
            return false;
        }else{
            return true;
        }
    }

    public static function getThemeURL(){
        return Yii::$app->theme->baseUrl;
    }

    public static function httpBuildQuery($url, $params){
        $determine = '?';
        $pos = strrpos($url, '?');
        if($pos > 1){
            if($pos < strlen($url)){
                $determine = '&';
            }else{
                $determine = '';
            }
        }
        $query = $url.$determine;
        foreach($params as $key=>$val){
            $query .= $key.'='.$val.'&';
        }
        $query = substr($query,0,-1);
        return $query;
    }

    /**
     * @param Unixtimestamp $timestampt
     * @return string
     */
    public static function convertMysqlTime($timestampt){
        return date("Y-m-d H:m:s", $timestampt);
    }

    public static function convertMysqlToTimestamp($dateString) {
        $format = '@^(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2}) (?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})$@';
        preg_match($format, $dateString, $dateInfo);
        $unixTimestamp = mktime(
            $dateInfo['hour'], $dateInfo['minute'], $dateInfo['second'],
            $dateInfo['month'], $dateInfo['day'], $dateInfo['year']
        );
        return $unixTimestamp;
    }

    public static function timeElapsedStringFromMysql($dateString) {
        $ptime = CUtils::convertMysqlToTimestamp($dateString);
        return CUtils::timeElapsedString($ptime);
    }

    public static function getUserIP() {
        if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
                $addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
                return trim($addr[0]);
            } else {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
        else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    public static function cidrMatch($ip, $range) {
        list ($subnet, $bits) = explode('/', $range);
        $ip = ip2long($ip);
        $subnet = ip2long($subnet);
        $mask = -1 << (32 - $bits);
        $subnet &= $mask; # nb: in case the supplied subnet wasn't correctly aligned
        return ($ip & $mask) == $subnet;
    }

    public static function truncateWords($text, $length = 10){
        if(strlen($text) > $length){
            $text_temp = substr($text, 0, $length);
            $end_point = strrpos($text_temp, ' ');
            $text_fi = substr($text_temp, 0, $end_point).'...';
            return $text_fi;
        }else{
            return $text;
        }
    }

    public static function getStartDate($startDate){
        $date = new DateTime($startDate);
        $date->setTime(00, 00, 00);
        return $date->format('Y-m-d H:i:s');
    }

    public static function getEndDate($endDate){
        $date = new DateTime($endDate);
        $date->setTime(23, 59, 59);
        return $date->format('Y-m-d H:i:s');
    }

    public static function messageNot3G(){
        return "Tính năng chỉ sử dụng trên  GPRS/3G  của MobiFone. Xin lỗi quý khách vì sự bất tiện này";
    }

    /**
     * validate string date
     *
     * @param $date
     * @param $separator: / or -
     * @return bool
     */
    public static function isDate($date, $separator) {
        $arrayDate =  explode($separator, $date);
        if(count($arrayDate) < 3) {
            return false;
        }
        $day = (int) $arrayDate[0];
        $month = (int) $arrayDate[1];
        $year = (int) $arrayDate[2];
        //validate ngay hop le trong nam
        if(!checkdate($month, $day, $year)) {
            return false;
        }
        return true;
    }

    public static function encryptVMSLinkConfirm($params){
        $str = implode('&', $params);
        return base64_encode(self::aes128_ecb_encrypt(VMS_ENCRYPT_KEY,$str,""));

    }

    public static function decryptVMSLinkConfirm($str){
        $str = str_replace(' ', '+', $str);
        $clean_str =  self::aes128_ecb_decrypt(VMS_ENCRYPT_KEY,base64_decode($str),"");
        $params = explode('&', $clean_str);
        if($params){
            return array(
                'trans_id' => $params[0],
                'msisdn' => $params[1],
                'status' => $params[2],
            );
        }else{
            return false;
        }
    }
    public static function decryptVMSLinkConfirm2($str){
        $str = str_replace(' ', '+', $str);
        $clean_str =  self::aes128_ecb_decrypt(VMS_ENCRYPT_KEY,base64_decode($str),"");
        $params = explode('&', $clean_str);
        if($params){
            return $params;
        }else{
            return false;
        }
    }

    // Hàm nhúng
    public static function aes128_ecb_encrypt($key, $data, $iv = '') {
        if(16 !== strlen($key)) $key = hash('MD5', $key, true);
        if(16 !== strlen($iv)) $iv = hash('MD5', $iv, true);
        $padding = 16 - (strlen($data) % 16);
        $data .= str_repeat(chr($padding), $padding);
        return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_ECB, $iv);
    }
    public static function aes256_ecb_encrypt($key, $data, $iv = '') {
        if(32 !== strlen($key)) $key = hash('SHA256', $key, true);
        if(16 !== strlen($iv)) $iv = hash('MD5', $iv, true);
        $padding = 16 - (strlen($data) % 16);
        $data .= str_repeat(chr($padding), $padding);
        return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_ECB, $iv);
    }
    public static function aes128_ecb_decrypt($key, $data, $iv = '') {
        if(16 !== strlen($key)) $key = hash('MD5', $key, true);
        if(16 !== strlen($iv)) $iv = hash('MD5', $iv, true);
        $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_ECB, $iv);
        $padding = ord($data[strlen($data) - 1]);
        return substr($data, 0, -$padding);
    }
    public static function aes256_ecb_decrypt($key, $data, $iv = '') {
        if(32 !== strlen($key)) $key = hash('SHA256', $key, true);
        if(16 !== strlen($iv)) $iv = hash('MD5', $iv, true);
        $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_ECB, $iv);
        $padding = ord($data[strlen($data) - 1]);
        return substr($data, 0, -$padding);
    }

    public static function duplicateCharacter($x, $char){
        $str = '';
        if(is_numeric($x)) {
            for($i=1;$i<=$x;$i++) {
                $str .= $char;
            }
        }
        return $str;
    }

    /**
     * replace message
     *
     * @param $message
     * @param $params
     * @return mixed
     */
    public static function replaceParam($message, $params) {
        if (is_array($params)) {
            $cnt = count($params);
            for( $i=1; $i <= $cnt; $i++ ){
                $message = str_replace('{'.$i.'}', $params[$i-1] , $message);
            }
        }
        return $message;
    }

    /**
     * is empty
     *
     * @param $strParam
     * @return bool
     */
    public static function isEmpty($strParam) {
        if($strParam == '' || $strParam == null) {
            return true;
        }
        return false;
    }

    public static function clientIP() {
    	if (!empty($_SERVER['HTTP_CLIENTIP'])) {
    		return $_SERVER['HTTP_CLIENTIP'];
    	}
    
    	if (!empty($_SERVER['X_REAL_ADDR'])) {
    		return $_SERVER['X_REAL_ADDR'];
    	}
    
    	if (isset($_SERVER['REMOTE_ADDR'])) {
    		return $_SERVER['REMOTE_ADDR'];
    	}
    	return gethostbyname(gethostname()); // tra ve ip local khi chay CLI
    }
    
    /**
     * 
     * @param unknown $obj
     * @param unknown $isSuccess
     * @param unknown $errorCode
     * @param string $msg
     */
    public static function jsonResponse($obj, $isSuccess, $errorCode = 0, $msg = '')
    {
    	$obj['success'] = $isSuccess;
    	if ($isSuccess) {
    		$obj['error'] = 0;
    	} else {
    		if (!isset($obj['error'])) {
    			$obj['error'] = $errorCode;
    		}
    	}
    
    	if (!isset($obj['message']) && !empty($msg)) {
    		$obj['message'] = $msg;
    	}
    	
    	echo Json::encode($obj);
    	return;
    }
    
    /**
     *
     * @param unknown $headers
     * @return string
     */
    public static function getRequestHeaders(){
    	$headers = apache_request_headers ();
    	$headerObject = null;
    	foreach ( $headers as $header => $value ) {
    		if (strtoupper ( $header ) == 'MSISDN') {
    			$headerObject['msisdn'] = trim ( $value );
    		}
    		if (strtoupper ( $header ) == 'X-IPADDRESS') {
    			$headerObject['xipaddress'] = trim ( $value );
    		}
    		if (strtoupper ( $header ) == 'X-FORWARDED-FOR') {
    			$headerObject['xforwarded'] = trim ( $value );
    		}
    		if (strtoupper ( $header ) == 'X-WAP-MSISDN') {
    			$headerObject['xwapmsisdn'] = trim ( $value );
    		}
    		if (strtoupper ( $header ) == 'USER-IP') {
    			$headerObject['userip'] = trim ( $value );
    		}
    	}
    	return $headerObject;
    }


    /**
     *
     * @param string $mobileNumber
     * @param int type format: 0: format 84xxx, 1: format 0xxxx, 2: format xxxx
     * @return String valid mobile
     */
    public static function validatePhoneNumber($mobileNumber, $typeFormat = 0){
    	$valid_number = '';
    
    	// Remove string "+"
    	$mobileNumber = str_replace('+84', '84', $mobileNumber);
    
    
    	//TODO: for testing: dung so dung cua VMS goi qua charging test ko thanh cong
    	if(preg_match('/^(84|0)(986636879)$/', $mobileNumber, $matches)){
    		return "84986636879";
    	}

    	//91|94|88|123|124|125|127|129 VINA
    	//90|93|120|121|122|126|128 MOBI
    	if(preg_match('/^(84|0|)(91|94|88|123|124|125|127|129)\d{7}$/', $mobileNumber, $matches)){
    		/**
    		 * $typeFormat == 0: 8491xxxxxx
    		 * $typeFormat == 1: 091xxxxxx
    		 * $typeFormat == 2: 91xxxxxx
    		 */
    		if($typeFormat == 0){
    			if ($matches[1] == '0' || $matches[1] == ''){
    				$valid_number = preg_replace('/^(0|)/', '84', $mobileNumber);
    			}else{
    				$valid_number = $mobileNumber;
    			}
    		}else if($typeFormat == 1){
    			if ($matches[1] == '84' || $matches[1] == ''){
    				$valid_number = preg_replace('/^(84|)/', '0', $mobileNumber);
    			}else{
    				$valid_number = $mobileNumber;
    			}
    		}else if ($typeFormat == 2){
    			if ($matches[1] == '84' || $matches[1] == '0'){
    				$valid_number = preg_replace('/^(84|0)/', '', $mobileNumber);
    			}else{
    				$valid_number = $mobileNumber;
    			}
    		}
    
    	}
    	return $valid_number;
    }

    public static function arraySyntaxRegister() {
    	return array(
    			'dk game'
    	);
    }
    
    public static function arrayPackageIDs() {
    	return array(
    			'dk game'=>1
    	);
    }

    /**
     * 
     * @param unknown $code
     * @return number
     */
    public static function getPackageCode($code){
    	if (empty($code)) {
    		return 0;
    	}
    	$packageCodeList = array (
    			'GAME_DAILY',
    			'GAME_WEEKLY'
    	);
    	if(in_array($code,$packageCodeList)) {
    		$packageList = array (
    				'GAME_DAILY' => 1,
    				'GAME_WEEKLY' => 2
    		);
    		return $packageList [$code];
    	} else {
    		return 0;
    	}
    }

    /**
     * @param array $params
     * promotion: 0: dang ky binh thuong
     *        Nc: mien cuoc N chu ky.
     *        Nd: mien cuong N ngay
     *        Nw: mien cuoc dung N tuan
     *        Nm: mien cuoc dung N thang
     * trial :0: dang ky binh thuong
     *    Nc: duoc dung thu N chu ky dau
     *    Nd: duoc dung thu N ngay
     *    Nw: duoc dung thu N tuan
     *   Nm: duoc dung thu N thang
     * bundle: 0: dang ky minh thuong
     *    1: dang ky kieu goi bundle (khong tru cuoc dang ky, khong gia han)
     * @return int
     */
    public static function getPromotion($params = array()) {
    	//check bundle parameter
    	$bundle = $params['bundle'];
    	$trial = $params['trial'];
    	$strPromotion = $params['promotion'];
    	if (preg_match("/[1|0]$/",$bundle)) {
    		return (int) $bundle;
    	}
    
    	//check trial parameter
    	if (preg_match("/[0-9][c|d|w|m]$/", $trial)) {
    		if ($trial <= 0) return 0;
    		else
    			return $trial;
    	}
    
    	//check promotion parameter
    	if (preg_match("/[0-9][c|d|w|m]$/", $strPromotion)) {
    		if ($strPromotion <= 0) return 0;
    		else
    			return $strPromotion;
    	}
    	return 0;
    }
    
    /**
     * tiendx
     *
     * get number of free days
     *
     * @param $freeDays
     * @param $promotion
     * @param $price_set_duration
     */
    public static function getFreeDays($freeDays, $promotion, $price_set_duration) {
        $days = CUtils::getNumberDaysForPromotion($promotion, $price_set_duration);
        if($days == -1) {
            return $price_set_duration;
        } else if($days == 0) {

        } else {
            return $days;
        }
        return $freeDays;
    }
    /**
     * 20140814 tiendx
     * tinh so ngay duoc mien phi
     *
     * @param $promotion
     * @param $price_set_duration
     * @return int|string
     */
    public static function getNumberDaysForPromotion($promotion, $price_set_duration) {
        if($promotion === 0) {
            //dang ky binh thuong
            return 0;
        } else if ($promotion === 1) {
            return -1;// dang ky kieu bundle
        } else {
            $length = strlen($promotion);
            $firstVariable = substr($promotion, 0, $length - 1);
            if(empty($firstVariable) || !is_numeric($firstVariable) || $firstVariable <= 0) {
                return 0;//dang ky binh thuong
            }

            $secondString =  strtolower(substr($promotion, $length - 1, $length));
            if ($secondString == 'c') {
                return $price_set_duration * $firstVariable;
            } else if ($secondString == 'd') {
                return $firstVariable;
            }  else if ($secondString == 'w') {
                return $firstVariable * 7;
            }  else if ($secondString == 'm') {
                return $firstVariable * 30;
            }  else {
                return 0;//dang ky binh thuong
            }
        }
    }

    /**
     * kiem tra user co duoc nhan MT hay khong
     *
     * @param $application
     * @param $vas
     * @return bool
     */
    public static function isReceiveMT($application, $vas) {
    	if(!in_array($application, $vas)) {
    		return true;
    	}
    	return false;
    }
    
    /**
     * Nhan mt khi thanh cong
     * @return multitype:string
     */
    public static function getListVas1() {
    	return array('vasvoucher');//,'cpplaybox'
    }
    
    /**
     * Nhan mt khi that bai
     * @return multitype:string
     */
    public static function getListVas2() {
    	return array('ccos','vasvoucher');
    }

    /**
     * kiem tra ip trong danh sach cho phep
     */
    public static function checkValidIp($remoteIp){
    	$validIps = array();
    	if (empty($validIps)) {
    		return true;
    	} else {
    		if (in_array($remoteIp, $validIps)) {
    			return true;
    		} else {
    			return false;
    		}
    	}
    }
}