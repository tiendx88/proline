<?php
/**
 *
 * @author Nguyen Chi Thuc
 */

namespace common\helpers;

class StringUtils {
	const GIA_HAN_THAT_BAI_HUY_GOI_CUOC = "";
	const GIA_HAN_THANH_CONG = "";
	const DK_LAN_DAU_THANH_CONG_THUE_BAO_MOI = "";
	const DK_LAN_DAU_THANH_CONG = "";
	const DK_THANK_CONG = "";
	const DK_KHONG_DU_TIEN = "";
	const LOI_HE_THONG = "";
	const DK_GOI_CUOC_DA_SU_DUNG = "";
	const HUY_GOI_CHUA_DK = "";
	const HUY_GOI_THANH_CONG = "";
	const HUY_GOI_THAT_BAI = "";

    public static function startsWith($s, $prefix)
    {
        return empty($prefix) || strpos($s, $prefix) === 0;
    }

    public static function endsWith($s, $suffix)
    {
        return empty($suffix) || substr($s, -strlen($suffix)) === $suffix;
    }

    public static function contain($s, $subStr)
    {
        if (empty($subStr)) return true;
        return strpos($s, $subStr) !== false;
    }

    /**
     * @param $s1
     * @param $s2
     * @return bool
     */
    public static function equal($s1, $s2) {
       return strcmp($s1, $s2) === 0;
    }

    public static function removeTail($s, $tail) {
        if (empty($tail) || !StringUtils::endsWith($s, $tail)) {
            return $s;
        }

        return substr($s, 0, - strlen($tail));
    }

    public static function removeHead($s, $head) {
        if (empty($head) ||!StringUtils::startsWith($s, $head)) {
            return $s;
        }

        return substr($s,strlen($head));
    }

    /**
     * Convert camel string (BeUser) to dash format (be-user)
     * @param $s
     * @param $head
     * @return string
     */
    public static function camel2Dash($s) {

        $s2 = preg_replace("%([A-Z])%se", "'-' . strtolower('\\1')",$s);
        if (!StringUtils::startsWith($s, '-')) {
            $s2 = StringUtils::removeHead($s2, '-');
        }
        return $s2;
    }
}

?>
