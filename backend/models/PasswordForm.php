<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2/2/2016
 * Time: 3:22 PM
 */

namespace backend\models;


use common\models\User;
use Yii;
use yii\base\Model;

class PasswordForm extends Model{
    public $oldpass;
    public $newpass;
    public $repeatnewpass;

    public function rules(){
        return [
            [['oldpass','newpass','repeatnewpass'],'required'],
            ['oldpass','findPasswords'],
            ['repeatnewpass','compare','compareAttribute'=>'newpass'],
        ];
    }

    public function findPasswords($attribute, $params){
        /** @var User $user */
        $user = User::find()->where([
            'username'=>Yii::$app->user->identity->username
        ])->one();
        $password = $user->password_hash;

        if (!$user || !$user->validatePassword($this->oldpass)) {
            $this->addError($attribute, 'Old password is incorrect.');
        }
    }

    public function attributeLabels(){
        return [
            'oldpass'=>'Old Password',
            'newpass'=>'New Password',
            'repeatnewpass'=>'Repeat New Password',
        ];
    }
}