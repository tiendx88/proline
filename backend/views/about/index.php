<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\AboutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quản lý thông tin giới thiệu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Thêm mới thông tin giới thiệu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'title_en',
            //'image',
            //'content:ntext',

            // 'content_en:ntext',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'status',
                'value' => function ($data) {
                    /**@var $data \common\models\Product*/
                    return $data->getStatusLabel();
                },
                'filter' => Html::activeDropDownList($searchModel, 'status', \common\models\About::$STATUS,['class'=>'form-control','prompt' => '--Select--']),
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
