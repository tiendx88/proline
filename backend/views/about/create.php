<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\About */

$this->title = 'Tạo thông tin giới thiệu';
$this->params['breadcrumbs'][] = ['label' => 'Quản lý thông tin giới thiệu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
