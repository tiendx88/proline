<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\About */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý thông tin giới thiệu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Quay lại', ['index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'title_en',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => "<img src='" . $model->getThumb() ."' width=80px />",
            ],
            [
                'attribute' => 'content',
                'format' => 'html',
                'value' => $model->content,
            ],
            [
                'attribute' => 'content_en',
                'format' => 'html',
                'value' => $model->content_en,
            ],
            [
                'attribute' => 'status',
                'format'=>'text',
                'value' => $model->getStatusLabel(),
            ],
        ],
    ]) ?>

</div>
