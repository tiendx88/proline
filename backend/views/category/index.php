<?php

use common\models\Category;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quản lý danh mục';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Thêm mới danh mục', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'category_id',
//            [
//                'class' => 'yii\grid\DataColumn',
//                //'attribute' => 'img_thumbnail',
//                'format'=>'html',
//                'value' => function ($data) {
//                    if(!empty($data->img_thumbnail)) {
//                        return Html::img($data->getThumbnail(),['width' => '30px','height'=> '30px']);
//                    }
//                    return '';
//
//                },
//            ],
            'category_name',
            'category_name_en',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'parent_id',
                'value' => function ($data) {
                    /** @var $data  Category*/
                    $cate = $data->getCategoryName($data->parent_id);
                    return !empty($cate) ? $cate->category_name: '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'parent_id', Category::getListOther(),['class'=>'form-control','prompt' => '--Select--']),
            ],
            'weight',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'status',
                'value' => function ($data) {
                    /** @var $date Category */
                    return $data->getStatusLabel();
                },
                'filter' => Html::activeDropDownList($searchModel, 'status', [0=>'InActive', 10=>"active", 2=>"Block"],['class'=>'form-control','prompt' => '--Select--']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
