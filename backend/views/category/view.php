<?php

use common\models\Category;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = $model->category_name;
$this->params['breadcrumbs'][] = ['label' => 'Danh mục', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Quay lại', ['index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Cập nhật', ['update', 'id' => $model->category_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->category_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn có chắc chắn xóa danh mục này?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'category_id',
            'category_name',
            'category_name_en',
            'weight',
            [
                'label' => 'status',
                'format'=>'html',
                'value' => $model->getStatusLabel()
            ],
            [
                'attribute' => 'parent_id',
                'format'=>'text',
                'value' => Category::getNameById($model->parent_id),
            ],
//            [// image
//                'label' => 'img_thumbnail',
//                'format'=>'html',
//                'value' => Html::img($model->getThumbnail($model->img_thumbnail),['width' => '30px','height'=> '30px']),
//            ],
        ],
    ]) ?>

</div>
