<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ContactsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quản lý thông tin liên hệ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Thêm thông tin liên hệ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['class' => 'yii\grid\ActionColumn'],
            'id',
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'contact_us_vi',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:250px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Contacts */
                    return $data->contact_us_vi;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'contact_us_en',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:250px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Contacts */
                    return $data->contact_us_en;
                },
            ],

            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'visit_vi',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:250px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Contacts */
                    return $data->visit_vi;
                },
            ],

            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'visit_en',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:250px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Contacts */
                    return $data->visit_en;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'work_with_vi',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:250px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Contacts */
                    return $data->work_with_vi;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'work_with_en',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:250px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Contacts */
                    return $data->work_with_en;
                },
            ],


        ],
    ]); ?>
<?php Pjax::end(); ?></div>
