<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Contacts */

$this->title = "Thông tin liên hệ";
$this->params['breadcrumbs'][] = ['label' => 'Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Quay lại', ['index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'contact_us_vi',
                'format' => 'html',
                'value' => $model->contact_us_vi,
            ],
            [
                'attribute' => 'contact_us_en',
                'format' => 'html',
                'value' => $model->contact_us_en,
            ],
            [
                'attribute' => 'visit_vi',
                'format' => 'html',
                'value' => $model->visit_vi,
            ],
            [
                'attribute' => 'visit_en',
                'format' => 'html',
                'value' => $model->visit_en,
            ],
            [
                'attribute' => 'work_with_vi',
                'format' => 'html',
                'value' => $model->work_with_vi,
            ],
            [
                'attribute' => 'work_with_en',
                'format' => 'html',
                'value' => $model->work_with_en,
            ],
        ],
    ]) ?>

</div>
