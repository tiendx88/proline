<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Contacts */

$this->title = 'Quản lý thông tin liên hệ';
$this->params['breadcrumbs'][] = ['label' => 'Quản lý thông tin liên hệ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
