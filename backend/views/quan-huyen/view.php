<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\QuanHuyen */

$this->title = 'Chi tiết quận, huyện: ' . $model->ten_quan_huyen;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách quận huyện', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quan-huyen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ten_quan_huyen',
            [
                'attribute' => 'id_tinh_tp',
                'format' => 'html',
                'value' => \common\models\TinhThanhPho::getName($model->id_tinh_tp),
            ],
        ],
    ]) ?>

</div>
