<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\QuanHuyenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách quận, huyện';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quan-huyen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Thêm quận,huyện', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ten_quan_huyen',
            //'ten_viet_tat',
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'id_tinh_tp',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 300px;min-width:100px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\QuanHuyen */
                    return \common\models\TinhThanhPho::getName($data->id_tinh_tp);
                },
                'filter' => Html::activeDropDownList($searchModel, 'id_tinh_tp', \yii\helpers\ArrayHelper::map(\common\models\TinhThanhPho::getList(),'id','ten_tinh'),['class'=>'form-control','prompt' => '--Select--']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
