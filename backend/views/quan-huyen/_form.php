<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\QuanHuyen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="quan-huyen-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL,
        'fullSpan' => 12,
        'formConfig' => [
            'showLabels' => true,
            'labelSpan' => 2,
            'deviceSize' => ActiveForm::SIZE_SMALL,
        ],
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= $form->field($model, 'ten_quan_huyen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_tinh_tp')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\TinhThanhPho::getList(),'id','ten_tinh'), ['prompt' => 'Chọn tỉnh, thành phố']) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Hủy bỏ', Url::toRoute(['index']), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
