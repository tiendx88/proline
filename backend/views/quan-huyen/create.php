<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\QuanHuyen */

$this->title = 'Create Quan Huyen';
$this->params['breadcrumbs'][] = ['label' => 'Quan Huyens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quan-huyen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
