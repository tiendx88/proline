<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Profline Management',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
//        $menuItems = [
//            ['label' => 'Quản lý tin tức', 'url' => ['/news/index']],
//        ];
        $menuItems[] = [
            'label' => "Quản lý Danh mục",
            'items' => [
                ['label' => "Quản lý menu", 'url' => ['/menu']],
                ['label' => "Quản lý danh Mục", 'url' => ['/category']],
                ['label' => "Quản lý tỉnh, thành phố", 'url' => ['/tinh-thanh-pho']],
                ['label' => "Quản lý quận, huyện", 'url' => ['/quan-huyen']],
                ['label' => 'Quản lý thông tin giới thiệu', 'url' => ['/about']],
                ['label' => 'Quản lý thông tin liên hệ', 'url' => ['/contacts']],
//                ['label' => "Quản Lý Nhà Vận Chuyển", 'url' => ['/transporter']],
                // ['label' => "Quản Lý Kích Thước", 'url' => ['/size']],
//                ['label' => "Quản Lý Màu", 'url' => ['/Color']],
            ],
        ];
        /*$menuItems[] = [
            'label' => "Quản lý Hàng Hóa",
            'items' => [
                ['label' => "Quản Lý Sản Phẩm", 'url' => ['/product']],
            ],
        ];*/
        $menuItems[] = ['label' => 'Quản lý Sản Phẩm', 'url' => ['/product']];
        $menuItems[] = ['label' => 'Quản lý tin tức', 'url' => ['/news']];

//        $menuItems[] = [
//            'label' => "Quản lý đơn hàng",
//            'items' => [
////                ['label' => "Quản Lý Khách Hàng", 'url' => ['/customer']],
//                ['label' => "Quản Lý đơn hàng", 'url' => ['/order']],
//            ],
//        ];
        $menuItems[] = ['label' => 'Quản trị người dùng', 'url' => ['/user/index']];
        $menuItems[] = [
            'label' =>  Yii::$app->user->identity->username,
            'items' => [
                ['label' => "Đăng xuất", 'url' => ['/site/logout']],
                ['label' => "Cập nhật thông tin", 'url' => \yii\helpers\Url::toRoute(['user/update', 'id' => Yii::$app->user->id])],
                ['label' => "Thay đổi mật khẩu", 'url' => \yii\helpers\Url::toRoute(['user/change-password', 'id' => Yii::$app->user->id])],
            ],
        ];
        /*$menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>';*/
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Profline <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
