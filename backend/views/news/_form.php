<?php

use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form \kartik\form\ActiveForm*/
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL,
        'fullSpan' => 12,
        'formConfig' => [
            'showLabels' => true,
            'labelSpan' => 2,
            'deviceSize' => ActiveForm::SIZE_SMALL,
        ],
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?php if (!$model->isNewRecord) { ?>
        <span style="margin-left: 210px">
            <?= Html::img($model->getThumb(), ['height' => 120, 'width' => 120]) ?>
        </span>
    <?php } ?>
    <?= $form->field($model, 'img_thumbnail')->widget(FileInput::className(), []) ?>


    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserUploadUrl' => '?r=news/description'
        ]
    ]) ?>
    <?= $form->field($model, 'content_en')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserUploadUrl' => '?r=news/description'
        ]
    ]) ?>


    <?= $form->field($model, 'short_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_description_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\Category::$_LIST_STATUS, ['prompt' => 'Chọn trạng thái']) ?>

    <div class="form-group" style="text-align: center;">
        <?= Html::submitButton($model->isNewRecord ? 'Lưu' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Hủy bỏ', Url::toRoute(['index']), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
