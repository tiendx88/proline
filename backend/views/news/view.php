<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Quay lại', ['index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Cập nhật', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'img_thumbnail',
                'format' => 'html',
                'value' => "<img src='" . $model->getThumb() ."' width=80px />",
            ],
            [
                'attribute' => 'title',
                'format' => 'html',
                'value' => $model->title,
            ],
            [
                'attribute' => 'title_en',
                'format' => 'html',
                'value' => $model->title_en,
            ],
            [
                'attribute' => 'content',
                'format' => 'html',
                'value' => $model->content,
            ],
            [
                'attribute' => 'content_en',
                'format' => 'html',
                'value' => $model->content_en,
            ],
            'short_description',
            'short_description_en',
            [
                'attribute' => 'status',
                'format'=>'text',
                'value' => $model->getStatusLabel(),
            ],
            'view_count',
            [
                'attribute' => 'created_at',
                'format'=>'text',
                'value' => date('d-m-Y H:i:s', $model->created_at),
            ],
            [
                'attribute' => 'updated_at',
                'format'=>'text',
                'value' => date('d-m-Y H:i:s', $model->updated_at),
            ],
        ],
    ]) ?>

</div>
