<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quanr lý tin tức';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Thêm tin tức', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            'id',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'img_thumbnail',
                'format'=>'html',
                'value' => function ($data) {
                    /**@var $data \common\models\News*/
                    return Html::img($data->getThumb(),['width' => '80px','height'=> '60px']);

                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'title',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 200px;min-width:100px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\News */
                    return $data->title;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'title_en',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 200px;min-width:100px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\News */
                    return $data->title_en;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'content',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:300px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\News */
                    return $data->content;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'content_en',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:300px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\News */
                    return $data->content_en;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'short_description',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:300px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\News */
                    return $data->short_description;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'short_description_en',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:300px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\News */
                    return $data->short_description_en;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'status',
                'value' => function ($data) {
                    /**@var $data \common\models\Product*/
                    return $data->getStatusLabel();
                },
                'filter' => Html::activeDropDownList($searchModel, 'status', \common\models\News::$STATUS,['class'=>'form-control','prompt' => '--Select--']),
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'created_at',
                'value' => function ($data) {
                    /**@var $data \common\models\News */
                    return date('d-m-Y H:i:s', $data->created_at);
                },
                // 'filter' => \yii\jui\DatePicker::widget(['language' => 'vi', 'dateFormat' => 'dd-MM-yyyy', 'attribute' => 'created_at', 'model' => $searchModel]),
                'format' => 'html',
            ],

            // ':ntext',
            // 'short_description',
            // 'short_description_en',
            // 'description:ntext',
            // 'description_en:ntext',
            // 'honor',
            // 'img_thumbnail:ntext',
            // 'view_count',
            // 'status',
            // 'weight',
            // 'created_at',
            // 'updated_at',
            // 'created_by',


        ],
    ]); ?>
<?php Pjax::end(); ?></div>
