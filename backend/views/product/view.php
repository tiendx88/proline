<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name_vi;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Quay lại', ['index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name_vi',
            'name_en',
            [
                'attribute' => 'img_thumbnail',
                'format' => 'html',
                'value' => "<img src='" . $model->getThumb() ."' width=80px />",
            ],
            'short_description_vi',
            'short_description_en',
            [
                'attribute' => 'description_vi',
                'format' => 'html',
                'value' => $model->description_vi,
            ],
            [
                //'label' => 'Mô tả',
                'attribute' => 'description_en',
                'format' => 'html',
                'value' => $model->description_en,
            ],
            'size',
            'size_vi',
            'size_en',
            [
                'attribute' => 'feature_vi',
                'format' => 'html',
                'value' => $model->feature_vi,
            ],
            [
                'attribute' => 'feature_en',
                'format' => 'html',
                'value' => $model->feature_en,
            ],
            'like_count',
            [
                'attribute' => 'status',
                'format'=>'text',
                'value' => $model->getStatusLabel(),
            ],
        ],
    ]) ?>

</div>
