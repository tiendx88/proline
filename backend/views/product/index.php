<?php

use common\models\Product;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách sản phẩm';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Thêm mới sản phẩm', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            'id',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'img_thumbnail',
                'format'=>'html',
                'value' => function ($data) {
                    /**@var $data \common\models\Product*/
                    return Html::img($data->getThumb(),['width' => '80px','height'=> '60px']);

                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'name_vi',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 300px;min-width:100px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Product */
                    return $data->name_vi;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'name_en',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 300px;min-width:100px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Product */
                    return $data->name_en;
                },
            ],
            //'name_ascii',
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'short_description_vi',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:250px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Product */
                    return $data->short_description_vi;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'short_description_en',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:250px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Product */
                    return $data->short_description_en;
                },
            ],
            // 'description_vi:ntext',
            // 'description_en:ntext',
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'size',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 200px;min-width:100px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Product */
                    return $data->size;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'size_vi',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 200px;min-width:100px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Product */
                    return $data->size_vi;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'size_en',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 200px;min-width:100px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Product */
                    return $data->size_en;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'feature_vi',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:300px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Product */
                    return $data->feature_vi;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, default
                'attribute' => 'feature_en',
                'format' =>'html',
                //'headerOptions' => ['width' => '450px'],
                'contentOptions' => ['class' => 'text-wrap','style'=>'max-width: 350px;min-width:300px;white-space: initial;'],
                'value' => function ($data) {
                    /** @var $data \common\models\Product */
                    return $data->feature_en;
                },
            ],
            // 'like_count',
            // 'status',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'status',
                'value' => function ($data) {
                    /**@var $data \common\models\Product*/
                    return $data->getStatusLabel();
                },
                'filter' => Html::activeDropDownList($searchModel, 'status', Product::$STATUS,['class'=>'form-control','prompt' => '--Select--']),
            ],

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
