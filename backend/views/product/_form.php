<?php

use common\models\Category;
use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form kartik\form\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL,
        'fullSpan' => 12,
        'formConfig' => [
            'showLabels' => true,
            'labelSpan' => 2,
            'deviceSize' => ActiveForm::SIZE_SMALL,
        ],
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= $form->field($model, 'name_vi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->checkboxlist(Category::getListOther()) ?>

    <?php if (!$model->isNewRecord) { ?>
        <span style="margin-left: 210px">
            <?= Html::img($model->getThumb(), ['height' => 120, 'width' => 120]) ?>
        </span>
    <?php } ?>
    <?= $form->field($model, 'img_thumbnail')->widget(FileInput::className(), []) ?>

    <?php if (!$model->isNewRecord) { ?>
        <?php $listScreen = $model->getImageSlides(); ?>
        <?php if (!empty($listScreen)) { ?>
            <span style="margin-left: 210px">
        <?php
        /** @var $im \common\models\Images */
        foreach ($listScreen as $im) { ?>

            <?= Html::img($im->getImage(), ['height' => 120, 'width' => 120]) ?>

        <?php } ?>
         </span>
        <?php } ?>
    <?php } ?>

    <?= $form->field($model, 'img_slider[]')->widget(FileInput::className(), [
        'pluginOptions' => ['showUpload' => false],
        'options' => ['multiple' => true, 'accept' => 'image/*']
    ]) ?>

    <?= $form->field($model, 'feature_vi')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserUploadUrl' => '?r=product/description'
        ]
    ]) ?>

    <?= $form->field($model, 'feature_en')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserUploadUrl' => '?r=product/description'
        ]
    ]) ?>


    <?= $form->field($model, 'short_description_vi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'short_description_en')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description_vi')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserUploadUrl' => '?r=product/description'
        ]
    ]) ?>
    <?= $form->field($model, 'description_en')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserUploadUrl' => '?r=product/description'
        ]
    ]) ?>

    <?= $form->field($model, 'size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'size_vi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'size_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\Product::$STATUS, ['prompt' => 'Chọn trạng thái']) ?>


    <div class="form-group" style="text-align: center;">
        <?= Html::submitButton($model->isNewRecord ? 'Tạo mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Hủy bỏ', Url::toRoute(['product/index']), ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
