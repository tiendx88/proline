<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TinhThanhPho */

$this->title = 'Update Tinh Thanh Pho: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tinh Thanh Phos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tinh-thanh-pho-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
