<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TinhThanhPho */

$this->title = 'Create Tinh Thanh Pho';
$this->params['breadcrumbs'][] = ['label' => 'Tinh Thanh Phos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tinh-thanh-pho-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
