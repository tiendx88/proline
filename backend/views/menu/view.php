<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = "Tên menu:" . $model->category_name;
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Quay lại', ['index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Cập nhật', ['update', 'id' => $model->category_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->category_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn có chắc chắn xóa menu này?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'category_id',
            'category_name',
            'category_name_en',
            'url',
            'weight',
            [// image
                'attribute' => 'status',
                'format'=>'text',
                'value' => $model->getStatusLabel(),
            ],
//            [// image
//                'label' => 'img_thumbnail',
//                'format'=>'html',
//                'value' => Html::img($model->getThumbnail($model->img_thumbnail),['width' => '30px','height'=> '30px']),
//            ],
        ],
    ]) ?>

</div>
