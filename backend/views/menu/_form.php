<?php

use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="category-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL,
        'fullSpan' => 12,
        'formConfig' => [
            'showLabels' => true,
            'labelSpan' => 2,
            'deviceSize' => ActiveForm::SIZE_SMALL,
        ],
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= $form->field($model, 'category_name')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'category_name_en')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'parent_id')->dropDownList($model->getListOther($model->category_id), ['prompt' => 'Chọn danh mục cha']) ?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\Menu::$_LIST_STATUS, ['prompt' => 'Chọn trạng thái']) ?>

    <?= $form->field($model, 'url')->textInput() ?>

    <?= $form->field($model, 'weight')->textInput() ?>


    <div class="form-group" style="text-align: center">
        <?= Html::submitButton($model->isNewRecord ? 'Tạo' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Quay lại', ['index'], ['class' => 'btn btn-info']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
