<?php

namespace backend\controllers;

use common\helpers\VietnameseUtils;
use Yii;
use common\models\About;
use common\models\AboutSearch;
use yii\filters\AccessControl;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AboutController implements the CRUD actions for About model.
 */
class AboutController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ['access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['login', 'error'],
                    'allow' => true,
                ],
                [
                    'actions' => ['logout', 'index','create','update','delete','view'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all About models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AboutSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single About model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new About model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new About();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'image');

            if (!isset($file) || empty($file)) {
                $model->addError('image', 'Ảnh đại diện không được bỏ trống');
            } else {
                $filename = VietnameseUtils::removeSigns($model->getTitle())  . '-' . time() . '.'  . $file->extension;
                $mime = $file->type;
                $file->saveAs(Yii::getAlias('@webroot')."/tmp/{$filename}");//save origin image
                if($mime == 'image/png') {
                    Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                        ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 9]);
                } else {
                    Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                        ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 90]);
                }

                if (!empty($filename) && file_exists(Yii::getAlias('@webroot') . "/tmp/{$filename}")) {
                    unlink(Yii::getAlias('@webroot') . "/tmp/{$filename}");
                }
                $model->image = $filename;
            }

            if($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            return $this->render('create', [
                'model' => $model,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing About model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_image = $model->image;


        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'image');

            if (isset($file) && !empty($file)) {

                $filename = VietnameseUtils::removeSigns($model->getTitle())  . '-' . time() . '.'  . $file->extension;
                $mime = $file->type;
                $file->saveAs(Yii::getAlias('@webroot')."/tmp/{$filename}");//save origin image
                if($mime == 'image/png') {
                    Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                        ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 9]);
                } else {
                    Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                        ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 90]);
                }

                if (!empty($filename) && file_exists(Yii::getAlias('@webroot') . "/tmp/{$filename}")) {
                    unlink(Yii::getAlias('@webroot') . "/tmp/{$filename}");
                }

                if (!empty($old_image) && file_exists(Yii::getAlias('@webroot')."/uploads/images/{$old_image}")) {
                    unlink(Yii::getAlias('@webroot')."/uploads/images/{$old_image}");
                }
                $model->image = $filename;
            } else {
                $model->image = $old_image;
            }

            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
            return $this->render('update', [
                'model' => $model,
            ]);

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing About model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the About model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return About the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = About::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
