<?php

namespace backend\controllers;

use common\helpers\VietnameseUtils;
use Yii;
use common\models\News;
use common\models\NewsSearch;
use yii\filters\AccessControl;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['login', 'error'],
                    'allow' => true,
                ],
                [
                    'actions' => ['description','logout', 'index','create','update','delete','view'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDescription()
    {
        $uploadedFile = UploadedFile::getInstanceByName('upload');

        $file = time() . "_" . $uploadedFile->name;
        //$url = Yii::$app->urlManager->createAbsoluteUrl('/uploads/ckeditor/'.$file);
        $url = Yii::$app->params['be'] . '/uploads/ckeditor/' . $file;
        $uploadPath = Yii::getAlias('@webroot') . '/uploads/ckeditor/' . $file;
        $mime = $uploadedFile->type;
        //extensive suitability check before doing anything with the file…
        if ($uploadedFile == null) {
            $message = "No file uploaded.";
        } else if ($uploadedFile->size == 0) {
            $message = "The file is of zero length.";
        } else if ($mime != "image/jpeg" && $mime != "image/png") {
            $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
        } else if ($uploadedFile->tempName == null) {
            $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
        } else {
            $message = "";
            $move = $uploadedFile->saveAs($uploadPath);
            if (!$move) {
                $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
            }
        }
        $funcNum = $_GET['CKEditorFuncNum'];
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'img_thumbnail');
            if (!isset($file) || empty($file)) {
                $model->addError('img_thumbnail', 'Ảnh đại diện không được bỏ trống');
            } else {
                $filename = $model->getTitle() . '-' . time() . '.'  . $file->extension;
                $mime = $file->type;
                $file->saveAs(Yii::getAlias('@webroot')."/tmp/{$filename}");//save origin image
                if($mime == 'image/png') {
                    Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                        ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 9]);
                } else {
                    Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                        ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 90]);
                }
                if (!empty($filename) && file_exists(Yii::getAlias('@webroot') . "/tmp/{$filename}")) {
                    unlink(Yii::getAlias('@webroot') . "/tmp/{$filename}");
                }
                $model->img_thumbnail = $filename;

            }

            $model->title_ascii = VietnameseUtils::removeSigns($model->title);
            $model->created_at = time();
            if($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_file = $model->img_thumbnail;

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'img_thumbnail');
            if (isset($file) && !empty($file)) {
                $filename = $model->getTitle() . '-' . time() . '.'  . $file->extension;
                $mime = $file->type;
                $file->saveAs(Yii::getAlias('@webroot')."/tmp/{$filename}");//save origin image
                if($mime == 'image/png') {
                    Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                        ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 9]);
                } else {
                    Image::thumbnail("@webroot/tmp/{$filename}", 600, 500)
                        ->save(Yii::getAlias("@webroot/uploads/images/{$filename}"), ['quality' => 90]);
                }
                if (!empty($filename) && file_exists(Yii::getAlias('@webroot') . "/tmp/{$filename}")) {
                    unlink(Yii::getAlias('@webroot') . "/tmp/{$filename}");
                }

                if (!empty($old_file) && file_exists(Yii::getAlias('@webroot') . "/uploads/images/{$old_file}")) {
                    unlink(Yii::getAlias('@webroot') . "/uploads/images/{$old_file}");
                }
                $model->img_thumbnail = $filename;
            } else {
                $model->img_thumbnail = $old_file;
            }
            $model->updated_at = time();
            if($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
