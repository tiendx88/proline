<?php

namespace backend\controllers;

use common\helpers\VietnameseUtils;
use common\models\ProductCateAsm;
use Yii;
use common\models\Product;
use common\models\ProductSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['description','logout', 'index','create','update','delete','view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDescription()
    {
        $uploadedFile = UploadedFile::getInstanceByName('upload');

        $file = time() . "_" . $uploadedFile->name;
        //$url = Yii::$app->urlManager->createAbsoluteUrl('/uploads/ckeditor/'.$file);
        $url = Yii::$app->params['be'] . '/uploads/ckeditor/' . $file;
        $uploadPath = Yii::getAlias('@webroot') . '/uploads/ckeditor/' . $file;
        $mime = $uploadedFile->type;
        //extensive suitability check before doing anything with the file…
        if ($uploadedFile == null) {
            $message = "No file uploaded.";
        } else if ($uploadedFile->size == 0) {
            $message = "The file is of zero length.";
        } else if ($mime != "image/jpeg" && $mime != "image/png") {
            $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
        } else if ($uploadedFile->tempName == null) {
            $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
        } else {
            $message = "";
            $move = $uploadedFile->saveAs($uploadPath);
            if (!$move) {
                $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
            }
        }
        $funcNum = $_GET['CKEditorFuncNum'];
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
    }


    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $lang = Yii::$app->session->get('lang','vi');
        if ($model->load(Yii::$app->request->post())) {
            $model->img_thumbnail = UploadedFile::getInstance($model, 'img_thumbnail');
            $model->img_slider = UploadedFile::getInstances($model, 'img_slider');

            if (!isset($model->img_thumbnail) || empty($model->img_thumbnail)) {
                $model->addError('img_thumbnail', 'Ảnh đại diện không được bỏ trống');
            }

            if (!isset($model->img_slider) || empty($model->img_slider)) {
                $model->addError('img_slider', 'Ảnh slide không được bỏ trống');
            }
            $trans = Yii::$app->db->beginTransaction();
            $model->name_ascii = VietnameseUtils::removeSigns(($lang == 'vi') ? $model->name_vi : $model->name_en);
            if($model->validate() && $model->save()) {
                if($model->upload()) {
                    if (is_array($model->category_id)) {
                        foreach ($model->category_id as $cate) {
                            $proCate = new ProductCateAsm();
                            $proCate->product_id = $model->id;
                            $proCate->category_id = $cate;
                            if(!$proCate->save()){
                                $trans->rollBack();
                                return $this->render('create', [
                                    'model' => $model,
                                ]);
                            }
                        }
                    }
                    $trans->commit();
                } else {
                    $trans->rollBack();
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $lang = Yii::$app->session->get('lang','vi');
        $model = $this->findModel($id);
        $list = $model->productCateAsm;

        $arrayCate = [];
        if (null != $list) {
            foreach ($list as $cate) {
                /**@var $cate ProductCateAsm */
                $arrayCate[$cate->category_id] = $cate->category_id;
            }
        }
        $model->category_id = $arrayCate;

        if ($model->load(Yii::$app->request->post())) {
            $model->img_thumbnail = UploadedFile::getInstance($model, 'img_thumbnail');
            $model->img_slider = UploadedFile::getInstances($model, 'img_slider');

            if (!isset($model->img_thumbnail) || empty($model->img_thumbnail)) {
                $model->addError('img_thumbnail', 'Ảnh đại diện không được bỏ trống');
            }

            if (!isset($model->img_slider) || empty($model->img_slider)) {
                $model->addError('img_slider', 'Ảnh slide không được bỏ trống');
            }
            $trans = Yii::$app->db->beginTransaction();
            $model->name_ascii = VietnameseUtils::removeSigns(($lang == 'vi') ? $model->name_vi : $model->name_en);
           //var_dump($model->category_id);exit;
            if($model->validate() && $model->save()) {
                if ($model->uploadData()) {
                    if (null != $model->productCateAsm) {
                        ProductCateAsm::deleteAll(['product_id' => $model->id]);
                    }
                    if (is_array($model->category_id)) {
                        foreach ($model->category_id as $cate) {
                            $proCate = new ProductCateAsm();
                            $proCate->product_id = $model->id;
                            $proCate->category_id = $cate;
                            if(!$proCate->save()){
                                $trans->rollBack();
                                return $this->render('create', [
                                    'model' => $model,
                                ]);
                            }
                        }
                    }
                    $trans->commit();
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
