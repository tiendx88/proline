<?php

namespace backend\controllers;

use common\helpers\VietnameseUtils;
use common\models\CategorySearch;
use common\models\Menu;
use common\models\MenuSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','create','update','delete','view','load-children','load-children2'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionLoadChildren() {
        $parent = Yii::$app->request->get('parent','');
        if(empty($parent) || !is_numeric($parent)) {
            return json_encode( ['success' => false , 'message' => 'Cấp độ không hợp lệ.','code'=> -1]);
        } else {
            $list = Menu::find()->where(['parent_id' => $parent,'type' => Menu::TYPE_LEVEL_PRODUCT ,'status'=> Menu::STATUS_ACTIVE])->orderBy('weight asc, category_name asc, parent_id asc')->all();
            if(!empty($list)) {
                //return $this->renderPartial('load', ['listCate'=>$list]);
                $data = [];
                /** @var $cate Menu*/
                foreach($list as $cate) {
                    $data[$cate->category_id] = $cate->category_name;
                }
                return json_encode( ['success' => true , 'data' => Json::encode($data),'code'=> 1]);
            } else {
                return json_encode( ['success' => false ,'code'=> 0]);
            }

        }
    }

    public function actionLoadChildren2() {
        $parent = Yii::$app->request->get('parent','');
        if(empty($parent) || !is_numeric($parent)) {
            return json_encode( ['success' => false , 'message' => 'Cấp độ không hợp lệ.','code'=> -1]);
        } else {
            $list = Menu::find()->where(['parent_id' => $parent,'type' => Menu::TYPE_LEVEL_PRODUCT ,'status'=> Menu::STATUS_ACTIVE])->orderBy('weight asc, category_name asc, parent_id asc')->all();
            if(!empty($list)) {
                $data = [];
                /** @var $cate Menu*/
                foreach($list as $cate) {
                    /** @var $child Menu*/
                    $child = Menu::findAll(['parent_id' => $cate->category_id]);
                    if(!empty($child)) {
                        foreach($child as $c) {
                            $data[$c->category_id] = $cate->category_name . '--' . $c->category_name;
                        }
                    } else {
                        $data[$cate->category_id] = $cate->category_name;
                    }
                }
                return json_encode( ['success' => true , 'data' => Json::encode($data),'code'=> 1]);
            } else {
                return json_encode( ['success' => false ,'code'=> 0]);
            }

        }
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
		
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menu();
        $model->scenario = 'create';
        $img = UploadedFile::getInstance($model, 'img_thumbnail');
        $thumbnail = '';
        if(isset($img) && !empty($img)){
            $filename =  time() . "_" . VietnameseUtils::removeSigns($img->baseName) . "_" . Yii::$app->security->generateRandomString(12) . '.'  . $img->extension;
            $thumbnail = "/uploads/image/{$filename}";
            $img->saveAs(Yii::getAlias('@webroot') . $thumbnail);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->img_thumbnail = $thumbnail;
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->category_id]);
            }

            var_dump($model->getFirstErrors());exit;
            return $this->render('create', [
                'model' => $model,
            ]);

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $img = UploadedFile::getInstance($model, 'img_thumbnail');
        $thumbnail = $model->img_thumbnail;
        if(isset($img) && !empty($img)){
            $filename =  time() . "_" . VietnameseUtils::removeSigns($img->baseName) . "_" . Yii::$app->security->generateRandomString(12) . '.'  . $img->extension;
            $thumbnail = "/uploads/image/{$filename}";
            $img->saveAs(Yii::getAlias('@webroot') . $thumbnail);
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->img_thumbnail = $thumbnail;
            $model->save();
            return $this->redirect(['view', 'id' => $model->category_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
