/**
 * Created by Designer on 10/6/2014.
 */
var app = angular.module("numberOnlyInput", []);

app.directive('numberOnlyInput', function () {
    return {
        restrict: 'EA',
        template: '<input name="{{inputName}}" ng-model="inputModel" />',
        scope: {
            inputModel: '=',
            inputName: '='
        },
        link: function (scope) {
            scope.$watch('inputModel', function(newValue,oldValue) {
                var arr = String(newValue).split("");
                if (arr.length === 0) return;
                if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.' )) return;
                if (arr.length === 2 && newValue === '-.') return;
                if (isNaN(newValue)) {
                    scope.inputValue = oldValue;
                }
            });
        }
    };
});