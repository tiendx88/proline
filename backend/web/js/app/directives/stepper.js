/**
 * Created by Designer on 9/30/2014.
 */
angular.module('directives.stepper', [])

    .directive('rnStepper', function () {
        return {
            restrict: 'AE',
            require: 'ngModel',
            scope: {
                min: '=',
                max: '='
            },
            template: '<button type="button" ng-disabled="isOverMin()" ng-click="decrement()"><i class="glyphicon glyphicon-chevron-down"></i></button>' +
            '<input id="stepper-input"  ng-model="stepperInput" ng-change="changeStepperValue()" size="2"/>' +
            '<button type="button" ng-disabled="isOverMax()" ng-click="increment()"><i class="glyphicon glyphicon-chevron-up"></button>',
            link: function (scope, iElement, iAttrs, ngModelController) {
                scope.label = '';
                scope.oldInputValue = '';

                if (angular.isDefined(iAttrs.label)) {
                    iAttrs.$observe('label', function (value) {
                        scope.label = ' ' + value;
                        ngModelController.$render();
                    });
                }

                ngModelController.$render = function () {
                    console.log(ngModelController.$viewValue);

                    scope.stepperInput=ngModelController.$viewValue;
                    // update the validation status
                    checkValidity();
                };

                // when model change, cast to integer
                ngModelController.$formatters.push(function (value) {
                    return parseFloat(value);
                });

                // when view change, cast to integer
                ngModelController.$parsers.push(function (value) {
                    return parseFloat(value);
                });

                function checkValidity() {
                    // check if min/max defined to check validity
                    var valid = !(scope.isOverMin(true) || scope.isOverMax(true));
                    // set our model validity
                    // the outOfBounds is an arbitrary key for the error.
                    // will be used to generate the CSS class names for the errors
                    ngModelController.$setValidity('outOfBounds', valid);
                }

                function updateModel(offset) {
                    // update the model, call $parsers pipeline...
                    ngModelController.$setViewValue(parseFloat(ngModelController.$viewValue) + offset);
                    // update the local view
                    ngModelController.$render();
                }

                scope.isOverMin = function (strict) {
                    var offset = strict ? 0 : 1;
                    return (angular.isDefined(scope.min) && (parseFloat(ngModelController.$viewValue) - offset) < parseFloat(scope.min));
                };
                scope.isOverMax = function (strict) {
                    var offset = strict ? 0 : 1;
                    return (angular.isDefined(scope.max) && (parseFloat(ngModelController.$viewValue) + offset) > parseFloat(scope.max));
                };


                // update the value when user clicks the buttons
                scope.increment = function () {
                    updateModel(+1);
                };
                scope.decrement = function () {
                    updateModel(-1);
                };

                // check validity on start, in case we're directly out of bounds
                checkValidity();

                // watch out min/max and recheck validity when they change
                scope.$watch('min+max', function () {
                    checkValidity();
                });
                // change value when user input
                scope.changeStepperValue = function () {

                    var newValue = scope.stepperInput;


                    var arr = String(newValue).split("");
                    if (arr.length === 0) {// update the model, call $parsers pipeline...
                        ngModelController.$setViewValue(parseFloat(0));
                        // update the local view
                        ngModelController.$render();
                        return;
                    }

                    if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.' )) return;
                    if (arr.length === 2 && newValue === '-.') return;
                    if (isNaN(newValue)) {

                        scope.stepperInput = scope.oldInputValue;
                        return;
                    }
                    scope.oldInputValue = scope.stepperInput;
                    var modelValue = scope.stepperInput;
                    if ((scope.stepperInput.charAt(scope.stepperInput.length - 1)) == '.') {
                        modelValue = scope.stepperInput + '0';
                        console.log(modelValue);
                    }
                    else {
                        // update the model, call $parsers pipeline...
                        ngModelController.$setViewValue(parseFloat(modelValue));
                        // update the local view
                        ngModelController.$render();
                    }
                };
            }
        };
    });
