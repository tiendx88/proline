/**
 * Created by Designer on 9/25/2014.
 */
(function () {
    function run($rootScope) {
        $rootScope.baseUrl = 'http://localhost/mretailer/frontend/web/';
        //$rootScope.baseUrl = 'http://10.84.70.39/mretailer/frontend/web/index.php?r=';
    }

    angular.module('mRetailer', ['ui.bootstrap', 'directives.stepper', 'repeatDone','kendo.directives','ngSanitize']).run(run);

})();