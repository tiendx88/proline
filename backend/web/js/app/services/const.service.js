/**
 * Created by Designer on 9/26/2014.
 */
(function(){
    angular.module('mRetailer').service('ConstService',function($http,$rootScope){

        var model = {
            invoice:{
                INVOICE_TYPE_INVOICE:1,
                INVOICE_TYPE_ORDER:2,
                INVOICE_TYPE_RETURN:3,
                BTN_SAVE_LABEL_INVOICE:'Thanh toán',
                BTN_SAVE_LABEL_ORDER:'Đặt hàng',
                BTN_LABEL_CONVERT_TO_INVOICE:'Tạo hóa đơn',
                BTN_LABEL_CANCEL_ORDER:'Hủy đơn hàng',
                BTN_LABEL_REFUND:'Trả hàng',
                INVOICE_LABEL:'Hóa đơn',
                ORDER_LABEL:'Đặt hàng',
                REFUND_LABEL:'Trả hàng',
                PAGE_CREATE:1,
                PAGE_CONVERT_ORDER:3,
                PAGE_REFUND:4

            }

        };
        return model;
    });
})();