/**
 * Created by Designer on 9/26/2014.
 */
(function(){
    angular.module('mRetailer').service('UtilService',function($http,$rootScope){
        this.requestAjax = function(controller,action,params){
                 return $.ajax({
                    url : $rootScope.baseUrl + controller + "/" + action,
                    type : 'GET',
                    data : params
                });
        };
    });
})();