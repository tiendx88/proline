/**
 * Created by Designer on 10/1/2014.
 */
(function () {

    angular.module('mRetailer').controller('CreateCustomerReceiptCtrl', function ($scope, $rootScope, $modalInstance, $compile, SaleService, ConstService,ReceiptService) {

        $scope.service = SaleService;
        $scope.const = ConstService;
        $scope.receiptService=ReceiptService;

        $scope.close = function () {

            $modalInstance.dismiss('cancel');

        };
        // change value when user input
        $scope.changeCustomerPaying = function () {



            //valid number
            var paying_digits = $scope.receiptService.receipt.customerPaying.replace(/[^0-9.]/g, '');
            if (paying_digits !== $scope.receiptService.receipt.customerPaying) {
                $scope.receiptService.receipt.customerPaying = parseFloat(paying_digits);
            }
            if (paying_digits.length == 0)
                $scope.receiptService.receipt.customerPaying=0;

        };
        // duoc goi khi nguoi dung nhap tien thu cho tung hoa don
        $scope.changeInvoiceReceiptAmount=function(invoice){
            if(!$scope.receiptService.receipt.customerPaying){
                invoice.receipt_amount =0;
                return;
            }
            //valid number
            var receipt_digits = invoice.receipt_amount.replace(/[^0-9.]/g, '');
            var total_receipt=0;
            angular.forEach($scope.receiptService.receipt.listInvoice, function (invoice) {
                total_receipt +=parseFloat(invoice.receipt_amount);
            });
            if (receipt_digits !== invoice.receipt_amount) {
                invoice.receipt_amount = parseFloat(receipt_digits);
            }


            // tong tien thu cua cac hoa don phai < tien phieu thu
            if(total_receipt>$scope.receiptService.receipt.customerPaying){
                invoice.receipt_amount=$scope.receiptService.receipt.customerPaying - (total_receipt - invoice.receipt_amount);
            }
            if(invoice.paid_amount-invoice.paying_amount<0){
                invoice.receipt_amount=0;
            }
            else if(invoice.receipt_amount>(invoice.paid_amount-invoice.paying_amount)){
                invoice.receipt_amount=(invoice.paid_amount-invoice.paying_amount);
            }

        };
        $scope.makePayment=function(){
            if( !$scope.receiptService.receipt.customerPaying){
                $scope.service.activeCart.saveError = true;
                $scope.service.activeCart.saveErrorMessage = 'Danh sách thanh toán rỗng';
            }
            else{
                $scope.receiptService.action.makePayment($scope.receiptService.receipt)
                    .done(function (result) {
                        if (result['error'] == 0) {
                            $scope.service.activeCart.saveSuccess = true;
                            $scope.service.activeCart.saveSuccessMessage = result['message'];
                            $scope.close();
                        }
                        else {
                            $scope.service.activeCart.saveError = true;
                            $scope.service.activeCart.saveErrorMessage = result['message'];
                        }
                    });

            }
        };



    })

})();