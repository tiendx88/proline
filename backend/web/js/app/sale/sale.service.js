/**
 * Created by Designer on 9/26/2014.
 */
(function () {
    angular.module('mRetailer').service('SaleService', function ($http, $rootScope, $q, UtilService,ConstService) {
        var vars = {
            customers: null,
            products: null,
            priceList: null,
            productBloodhound: null,
            storeId:null,
            listOrder:null
        };
        var carts = [];
        var activeCart = {
            customer: {},
            products: []
        };

        var model = {
            carts: carts,
            activeCart: activeCart,
            data: vars,
            invoiceModal:{},

            action: {
                getAllProduct: getAllProduct,
                changeResourceTypeHead: changeResourceTypeHead,
                removeCartProduct: removeCartProduct,
                saveInvoice:saveInvoice,
                saveRefund:saveRefund,
                cancelOrder:cancelOrder,
                changeToInvoice:changeToInvoice,
                getOrderViaFilter:getOrderViaFilter,
                getInvoiceViaFilter:getInvoiceViaFilter
            }
        };

        function getAllProduct(priceListId) {
            var request = UtilService.requestAjax('product', 'products', {'priceId': priceListId});
            return request;
        }
        function getOrderViaFilter(storeId,orderCode,customerName,fromDate,toDate){

            var params={
                storeId:storeId,
                orderCode:orderCode,
                customerName:customerName,
                fromDate: fromDate,
                toDate:toDate
            };
            var  action='get-order-via-filter';

            return $.ajax({
                url : $rootScope.baseUrl + "customer-invoice" + "/" + action,
                type : 'GET',

                data : params
            });
        }
        function getInvoiceViaFilter(storeId,orderCode,customerName,fromDate,toDate,currentPage,postPerPage){

            var params={
                storeId:storeId,
                invoiceCode:orderCode,
                customerName:customerName,
                fromDate: fromDate,
                toDate:toDate,
                currentPage:currentPage,
                postPerPage:postPerPage
            };
            var  action='get-invoice-via-filter';

            return $.ajax({
                url : $rootScope.baseUrl + "customer-invoice" + "/" + action,
                type : 'GET',

                data : params
            });
        }
        function saveInvoice(activeCart,functionType){
             var params = new FormData();
            //console.log(JSON.stringify(activeCart.products));
            params.append('activeCart',JSON.stringify(activeCart));
            var  action='save-invoice';

            return $.ajax({
                url : $rootScope.baseUrl + "customer-invoice" + "/" + action,
                type : 'POST',
                processData:false,
                contentType:false,
                data : params
            });
        }
        function saveRefund(activeCart){
            var params = new FormData();

            params.append('activeCart',JSON.stringify(activeCart));
            var  action='save-refund';

            return $.ajax({
                url : $rootScope.baseUrl + "customer-invoice" + "/" + action,
                type : 'POST',
                processData:false,
                contentType:false,
                data : params
            });
        }
        function cancelOrder(orderId,storeId){
            var params = new FormData();
             params.append('orderId',orderId);
            params.append('storeId',storeId);
            var  action='cancel-invoice';

            return $.ajax({
                url : $rootScope.baseUrl + "customer-invoice" + "/" + action,
                type : 'POST',
                processData:false,
                contentType:false,
                data : params
            });
        }
        function changeToInvoice(orderId,storeId){
            var params = new FormData();
            params.append('orderId',orderId);
            params.append('storeId',storeId);
            var  action='change-to-invoice';

            return $.ajax({
                url : $rootScope.baseUrl + "customer-invoice" + "/" + action,
                type : 'POST',
                processData:false,
                contentType:false,
                data : params
            });
        }
        function changeResourceTypeHead(productBloodhound, data) {
            productBloodhound.clear();
            productBloodhound.local = data;
            productBloodhound.initialize(true);
        }

        function removeCartProduct(products, product) {
            var index = products.indexOf(product)
            products.splice(index, 1);

        }



        return model;

    });
})();
