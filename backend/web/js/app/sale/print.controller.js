/**
 * Created by Designer on 10/4/2014.
 */
(function () {

    angular.module('mRetailer').controller('PrintInvoiceCtrl', function ($scope, $rootScope, $compile, SaleService) {

        $scope.service = SaleService;
        $scope.invoice_date = 0; // 0: vnd, 1: %
        $scope.products=null;
        $scope.cart_name='';
        $scope.customer=null;
        $scope.staff=null;
        $scope.totalAmount=0;
        $scope.discount=0;



        $scope.init = function () {
            $scope.invoice_date = $scope.service.activeCart.invoice_datetime; // 0: vnd, 1: %
            $scope.products=$scope.service.activeCart.products;
            $scope.cart_name=$scope.service.activeCart.name;
            $scope.customer=$scope.service.activeCart.customer;
            $scope.staff=$scope.service.activeCart.staff;
            $scope.totalAmount=$scope.service.activeCart.totalAmount;
            $scope.discount=$scope.service.activeCart.discount;
            console.log($scope.cart_name);
            console.log( $scope.service);

         }


    });


})();