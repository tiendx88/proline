/**
 * Created by Designer on 10/1/2014.
 */
(function () {

    angular.module('mRetailer').controller('ProductSearchCtrl', function ($scope, SaleService) {

        $scope.service = SaleService;

        var autocomplete_products = new Bloodhound({
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.whitespace(d.name + " " + d.code);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: $scope.service.data.products

        });


        autocomplete_products.initialize();
        $scope.service.data.productBloodhound=autocomplete_products;

        $('#autocomplete_products .typeahead').typeahead({
                highlight: true
            },
            {
                name: 'product_search_id',
                displayKey: 'name',
                source: autocomplete_products.ttAdapter(),
                templates: {
                    empty: [
                        '<div class="empty-message">',
                        'Không tồn tại',
                        '</div>'
                    ].join('\n'),
                    suggestion: Handlebars.compile('<div></div><p><strong>{{name}}</strong> </p>' +
                    '<p>Mã: {{code}} Giá: {{price}}</p>' +
                    '<p>Tồn: {{inventory}} Đặt: {{current_order}}</p></div>')
                }
            }).on('typeahead:selected', function (obj, datum, name) {
                for (var i = 0; i < $scope.service.activeCart.products.length; i++) {
                    var item = $scope.service.activeCart.products[i];
                    if ( item.id==datum.id) {
                        item.quantity++;
                        $scope.$apply();return;
                    }
                }
                var product = {};
                //create product
                product.name = datum.name;
                product.code = datum.code;
                product.id=datum.id;
                product.price = datum.price;
                product.adjusted_price = datum.price;
                product.inventory = datum.inventory;
                product.quantity = 1;
                product.discountType=0; //0: vnd, 1: %
                product.discountValue='';
                product.note='';

                $scope.service.activeCart.products.push(product);

                $('#autocomplete_products .typeahead').typeahead('val', null);
                $scope.$apply();
            });



    })

})();