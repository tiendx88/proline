/**
 * Created by Designer on 10/1/2014.
 */
(function () {

    angular.module('mRetailer').controller('SearchInvoiceCtrl', function ($scope, $rootScope, $modalInstance, $compile, $timeout, SaleService, ConstService) {

        $scope.service = SaleService;
        $scope.const = ConstService;
        $scope.service.invoiceModal.postPerPage=5;

        $scope.invoiceFilter = {
            Code: null,
            CustomerName: null,
            fromDate: null,
            toDate: null
        };
        $scope.close = function () {
            console.log($scope.invoiceFilter);
            $modalInstance.dismiss('cancel');

        };
        $scope.pageChanged = function() {
             SaleService.action.getInvoiceViaFilter($scope.service.data.storeId, $scope.invoiceFilter.Code, $scope.invoiceFilter.CustomerName, $scope.invoiceFilter.fromDate, $scope.invoiceFilter.toDate,$scope.service.invoiceModal.currentPage,$scope.service.invoiceModal.postPerPage)
                .done(function (result) {

                    $scope.service.data.listInvoice = result['list_invoice'];
                    $scope.service.invoiceModal.totalItems=result['total_record'];

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });

        };
        $scope.filterByDateRange = function () {
            $scope.service.invoiceModal.currentPage=1;
            SaleService.action.getInvoiceViaFilter($scope.service.data.storeId, $scope.invoiceFilter.Code, $scope.invoiceFilter.CustomerName, $scope.invoiceFilter.fromDate, $scope.invoiceFilter.toDate,$scope.service.invoiceModal.currentPage,$scope.service.invoiceModal.postPerPage)
                .done(function (result) {

                    $scope.service.data.listInvoice = result['list_invoice'];
                    $scope.service.invoiceModal.totalItems=result['total_record'];
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });

        };
        $scope.filterByCode = function () {
            if ($scope.invoiceFilter.Code && $scope.invoiceFilter.Code.length < 3) {
                return;
            }
            $scope.service.invoiceModal.currentPage=1;
            SaleService.action.getInvoiceViaFilter($scope.service.data.storeId, $scope.invoiceFilter.Code, $scope.invoiceFilter.CustomerName, $scope.invoiceFilter.fromDate, $scope.invoiceFilter.toDate,$scope.service.invoiceModal.currentPage,$scope.service.invoiceModal.postPerPage)
                .done(function (result) {

                    $scope.service.data.listInvoice = result['list_invoice'];
                    $scope.service.invoiceModal.totalItems=result['total_record'];
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });
        };
        $scope.filterByCustomer = function () {
            if ($scope.invoiceFilter.CustomerName && $scope.invoiceFilter.CustomerName.length < 3) {
                return;
            }
            $scope.service.invoiceModal.currentPage=1;
            SaleService.action.getInvoiceViaFilter($scope.service.data.storeId, $scope.invoiceFilter.Code, $scope.invoiceFilter.CustomerName, $scope.invoiceFilter.fromDate, $scope.invoiceFilter.toDate,$scope.service.invoiceModal.currentPage,$scope.service.invoiceModal.postPerPage)
                .done(function (result) {

                    $scope.service.data.listInvoice = result['list_invoice'];
                    $scope.service.invoiceModal.totalItems=result['total_record'];
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });
        };
        $scope.$watch('service.activeCart.showMsgNotExistProduct', function () {
            if ($scope.service.activeCart.showMsgNotExistProduct) {
                $timeout(function () {
                    $scope.service.activeCart.showMsgNotExistProduct = false;

                }, 1000);

            }
        }, true);

        $scope.selectOrder = function (orderId) {

            var params = {
                storeId: $scope.service.data.storeId,
                orderId: orderId,
                orderType: $scope.const.invoice.INVOICE_TYPE_INVOICE,
                checkProductExist: true
            };
            $.ajax({
                url: $rootScope.baseUrl + 'customer-invoice' + "/" + 'get-order',
                type: 'GET',
                data: params
            }).done(function (result) {
                if (result['error']) {

                    $scope.service.activeCart.showMsgNotExistProduct = true;
                    $scope.service.activeCart.MsgNotExistProduct = result['message'];
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                }
                else {
                    $scope.close();
                    var currentStaff = null;
                    var currentPriceList = null;
                    angular.forEach($scope.service.data.staffs, function (staff) {
                        if (staff.id == result['user_id']) {
                            currentStaff = staff;

                        }
                    });
                    var price_list_id = result['price_list_id'] == null ? '' : result['price_list_id'];
                    angular.forEach($scope.service.data.priceList, function (price) {

                        if (price.id == price_list_id) {
                            currentPriceList = price;

                        }

                    });
                    var currentSummary = {

                        functionType: $scope.const.invoice.INVOICE_TYPE_RETURN,
                        discount: 0,
                        discountType: 0,
                        totalAmount: 0,// tong tien don hang
                        paidAmount: 0,// tien khach can tra

                        payingAmount: 0,// tien khach tra
                        payRefund: 0, // tien tra lai,
                        payRefundType: 0,// 0: tien thua tra khach,1: cong vao tai khoan
                        payRefundShowAll: false// hien thi khi add client
                    };
                    var products = [];
                    angular.forEach(result['products'], function (product) {
                        product.discountType = 0; //0: vnd, 1: %
                        product.inventory = product.quantity;
                        product.discount_amount = 0;
                        product.quantity = 0;
                        products.push(product);

                    });

                    var summaryList = [];
                    summaryList.push(currentSummary);
                    var title = 'TH' + result['code'];
                    $scope.service.tab.max_tab_id++;
                    var activeCart = {
                        cart_id: $scope.service.tab.max_tab_id,
                        order_id: orderId,
                        name: title,
                        active: true,
                        products: products,
                        category_id: '',
                        productSearch: angular.copy($scope.service.data.products),
                        currentPriceList: currentPriceList,
                        summaryList: summaryList,
                        currentSummary: currentSummary,
                        fromInvoiceId: orderId,
                        invoice_datetime: result['invoice_date'],
                        //customer
                        staff: currentStaff,
                        // pager
                        currentPaged: 1,
                        postPerPage: 4,
                        totalPage: Math.ceil($scope.service.data.products.length / 4),
                        // error
                        showErrorNull: false,
                        current_tab: $scope.const.invoice.PAGE_REFUND

                    };
                    $scope.service.carts.push(activeCart);
                    $scope.service.activeCart = activeCart;

                    if (!$rootScope.$phase) {
                        $rootScope.$apply();
                    }
                }


            });
        }


    })

})();