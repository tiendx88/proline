/**
 * Created by Designer on 10/1/2014.
 */
(function () {

    angular.module('mRetailer').controller('SearchOrderCtrl', function ($scope, $rootScope, $modalInstance, $compile,$timeout, SaleService,ConstService) {

        $scope.service = SaleService;
        $scope.const=ConstService;
        $scope.orderFilter = {
            Code: null,
            CustomerName: null,
            fromDate: null,
            toDate: null
        };
        $scope.close = function () {
            console.log($scope.orderFilter);
            $modalInstance.dismiss('cancel');

        };
        $scope.filterByDateRange = function () {
            SaleService.action.getOrderViaFilter($scope.service.data.storeId, $scope.orderFilter.Code, $scope.orderFilter.CustomerName, $scope.orderFilter.fromDate, $scope.orderFilter.toDate)
                .done(function (result) {

                    $scope.service.data.listOrder = result;
                     if(!$scope.$$phase) {
                        $scope.$apply();
                    }
                });

        };
        $scope.filterByCode = function () {
            if ($scope.orderFilter.Code &&  $scope.orderFilter.Code.length < 3) {
                return;
            }
            SaleService.action.getOrderViaFilter($scope.service.data.storeId, $scope.orderFilter.Code, $scope.orderFilter.CustomerName, $scope.orderFilter.fromDate, $scope.orderFilter.toDate)
                .done(function (result) {

                    $scope.service.data.listOrder = result;
                     if(!$scope.$$phase) {
                        $scope.$apply();
                    }
                });
        };
        $scope.filterByCustomer = function () {
            if ($scope.orderFilter.CustomerName && $scope.orderFilter.CustomerName.length < 3  ) {
                return;
            }
            SaleService.action.getOrderViaFilter($scope.service.data.storeId, $scope.orderFilter.Code, $scope.orderFilter.CustomerName, $scope.orderFilter.fromDate, $scope.orderFilter.toDate)
                .done(function (result) {

                    $scope.service.data.listOrder = result;
                     if(!$scope.$$phase) {
                        $scope.$apply();
                    }
                });
        };
        $scope.selectOrder= function(orderId){
            $scope.close();
            var params={
                storeId:$scope.service.data.storeId,
                orderId:orderId
            };
            $.ajax({
                url : $rootScope.baseUrl + 'customer-invoice' + "/" + 'get-order',
                type : 'GET',
                data : params
            }).done(function (result){


                var currentStaff=null;
                var currentPriceList=null;
                angular.forEach($scope.service.data.staffs, function (staff) {
                    if(staff.id==result['user_id']){
                        currentStaff=staff;

                    }
                });
                var price_list_id= result['price_list_id']== null ? '': result['price_list_id'];
                angular.forEach($scope.service.data.priceList, function (price) {

                    if(price.id==price_list_id){
                        currentPriceList=price;

                    }

                });
                var currentSummary={
                    functionType:$scope.const.invoice.INVOICE_TYPE_ORDER,
                    discount:result['discount'],
                    discountType:0,
                    totalAmount:result['total_amount'],// tong tien don hang
                    paidAmount:result['paid_amount'],// tien khach can tra

                    payingAmount:result['paying_amount'],// tien khach tra
                    payRefund:0 , // tien tra lai,
                    payRefundType:0,// 0: tien thua tra khach,1: cong vao tai khoan
                    payRefundShowAll:false// hien thi khi add client
                };

                var summaryList=[];
                summaryList.push(currentSummary);
                var title='HD'+result['code'];
                $scope.service.tab.max_tab_id++;
                var products=[];
                angular.forEach( result['products'], function (product) {
                    product.discountType=0; //0: vnd, 1: %
                    //product.inventory=product.quantity;
                    products.push(product);

                });
                var activeCart = {
                    cart_id: $scope.service.tab.max_tab_id,
                    order_id:orderId,
                    name: title,
                    active: true,
                    products:products,
                    category_id: '',
                    productSearch: angular.copy($scope.service.data.products),
                    currentPriceList:currentPriceList,
                    summaryList:summaryList,
                    currentSummary:currentSummary,
                    invoice_datetime:result['invoice_date'],
                    //customer
                    staff: currentStaff,
                    // pager
                    currentPage:1,
                    postPerPage:4,
                    totalPage: Math.ceil($scope.service.data.products.length/4),
                    // error
                    showErrorNull:false,
                    current_tab:$scope.const.invoice.PAGE_CONVERT_ORDER

                };
                $scope.service.carts.push(activeCart);
                $scope.service.activeCart=activeCart;
                $timeout(function () {
                    if(!$rootScope.$phase) {
                        $rootScope.$apply();
                    }
                }, 100);
            });
        }


    })

})();