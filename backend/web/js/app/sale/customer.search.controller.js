/**
 * Created by Designer on 10/1/2014.
 */
(function () {

    angular.module('mRetailer').controller('CustomerSearchCtrl', function ($scope, $rootScope, SaleService) {

        $scope.service = SaleService;
        var autocomplete_customer = new Bloodhound({
            datumTokenizer: function (d) {
                 return Bloodhound.tokenizers.whitespace(d.name);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: $scope.service.data.customers

        });
        autocomplete_customer.initialize();

        $('#autocomplete_customer .typeahead').typeahead({
                highlight: true
            },
            {
                name: 'customer_search_id',
                displayKey: 'name',
                source: autocomplete_customer.ttAdapter(),
                templates: {
                    empty: [
                        '<div class="empty-message">',
                        'Không tồn tại',
                        '</div>'
                    ].join('\n'),
                    suggestion: Handlebars.compile('<div><p><strong>{{name}}</strong> </p> </div>')
                }
            }).on('typeahead:selected', function (obj, datum, name) {
                $scope.service.activeCart.customer = datum;
                $('#autocomplete_customer .typeahead').typeahead('val', null);
                $scope.$apply();
            });

    });

})();