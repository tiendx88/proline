/**
 * Created by Designer on 9/26/2014.
 */
(function () {
    angular.module('mRetailer').service('ReceiptService', function ($http, $rootScope, $q, UtilService, ConstService) {


        var model = {

            action: {
                getOrderOfCustomer: getOrderOfCustomer,
                makePayment:makePayment

            }
        };


        function getOrderOfCustomer(customerId) {

            var params = {
                customerId: customerId
            };
            var action = 'get-orders';

            return $.ajax({
                url: $rootScope.baseUrl + "customer" + "/" + action,
                type: 'GET',

                data: params
            });
        };
        function makePayment(receipt){
            var params = new FormData();

            params.append('receipt',JSON.stringify(receipt));
            var  action='make-payment';

            return $.ajax({
                url : $rootScope.baseUrl + "customer-receipt" + "/" + action,
                type : 'POST',
                processData:false,
                contentType:false,
                data : params
            });
        }


        return model;

    });
})();
