/**
 * Created by Designer on 10/8/2014.
 */
/**
 * Created by Designer on 10/4/2014.
 */
(function () {

    angular.module('mRetailer').controller('FilterModalCtrl', function ($scope, $rootScope,$modalInstance, $compile, SaleService) {

        $scope.service = SaleService;


        $scope.ok = function () {
            $modalInstance.close($scope.selected.item);
        };

        $scope.close = function () {

            $modalInstance.dismiss('cancel');
        };
        $scope.selectCategory=function(category){
            $rootScope.service.activeCart.currentPage=1;
            if(category==null)
            {
                $scope.service.activeCart.category_id="";
            }
            else
            {
                $scope.service.activeCart.category_id=category.id;
            }

            $scope.close();
        }
    });
    angular.module('mRetailer').filter('filterByCategory', function($rootScope,SaleService) {
            return function( input, category_id,paged) {

                $rootScope.service = SaleService;

                $rootScope.service.activeCart.currentPage=1;
                if(paged!=null){
                    $rootScope.service.activeCart.currentPage=paged;
                }

                 if(!input) return "";


                 if(category_id==""){
                     $rootScope.service.activeCart.totalPage=Math.ceil(input.length/$rootScope.service.activeCart.postPerPage);
                      return input.slice(($rootScope.service.activeCart.currentPage-1)*$rootScope.service.activeCart.postPerPage,$rootScope.service.activeCart.currentPage*$rootScope.service.activeCart.postPerPage);
                 }
                 var out = [];
                for (var i = 0; i <  input.length; i++){
                    if(input[i].category_id == category_id)
                        out.push(input[i]);
                }
                $rootScope.service.activeCart.totalPage=Math.ceil(out.length/$rootScope.service.activeCart.postPerPage);
                 return out.slice(($rootScope.service.activeCart.currentPage-1)*$rootScope.service.activeCart.postPerPage,$rootScope.service.activeCart.currentPage*$rootScope.service.activeCart.postPerPage);
            };
        });



})();