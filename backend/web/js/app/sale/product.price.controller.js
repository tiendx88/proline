/**
 * Created by Designer on 10/4/2014.
 */
(function () {

    angular.module('mRetailer').controller('ProductPriceCtrl', function ($scope,$timeout, $rootScope, SaleService) {

        $scope.service = SaleService;
        $scope.discountType = 0; // 0: vnd, 1: %
        $scope.discountValue = '';
        $scope.adjustedPrice = '';
        $scope.note='';
        $scope.$watch('note', function () {

                $timeout(function () {
                     console.log($scope.note);
                }, 100);


        }, true);
        $scope.init=function(index){
            $scope.adjustedPrice= $scope.service.activeCart.products[index].adjusted_price;
            $scope.discountType= $scope.service.activeCart.products[index].discountType;
            $scope.discountValue= $scope.service.activeCart.products[index].discountValue;
        }
        $scope.initNote=function(index){

            $scope.note= $scope.service.activeCart.products[index].note;

        }
        function assignAttributes(index){
            $scope.service.activeCart.products[index].discountType = $scope.discountType;
            $scope.service.activeCart.products[index].discountValue = $scope.discountValue;
        }
        $scope.onNoteChange = function (product) {
            var index = $scope.service.activeCart.products.indexOf(product);
            $scope.service.activeCart.products[index].note=$scope.note;
            console.log($scope.note);
        }

        $scope.adjustedPriceChanged = function (product) {
            var index = $scope.service.activeCart.products.indexOf(product);
            $scope.adjustedPrice = $scope.adjustedPrice.trim();
            if ($scope.adjustedPrice.length == 0) {
                $scope.service.activeCart.products[index].adjusted_price = $scope.service.activeCart.products[index].price;
                $scope.discountValue = '';
                return;
            }
            //valid number
            var price_digits = $scope.adjustedPrice.replace(/[^0-9.]/g, '');
            if (price_digits !== $scope.adjustedPrice) {
                $scope.adjustedPrice = parseFloat(price_digits)
            }
            if (price_digits.length == 0)
                return;
            // end valid number


            $scope.service.activeCart.products[index].adjusted_price = $scope.adjustedPrice;
            $scope.discountValue = (product.origin_price - $scope.adjustedPrice) > 0 ? (($scope.discountType == 0) ? (product.price - $scope.adjustedPrice) : ((product.origin_price - $scope.adjustedPrice) * 100 / product.origin_price)) : '';
            assignAttributes(index);

        };
        $scope.discountOnProductChanged = function (product) {
            var index = $scope.service.activeCart.products.indexOf(product);
            if ($scope.discountValue.length == 0) {
                $scope.service.activeCart.products[index].adjusted_price = $scope.service.activeCart.products[index].price;
                $scope.adjustedPrice = $scope.service.activeCart.products[index].price;
                $scope.discountValue = '';
                $scope.service.activeCart.products[index].discountValue = $scope.discountValue;
                return;
            }
            console.log('xx'+$scope.discountValue);

            //valid number
            var discount_digits = $scope.discountValue.replace(/[^0-9.]/g, '');
            if (discount_digits !== $scope.discountValue) {
                $scope.discountValue = parseFloat(discount_digits);
            }
            if (discount_digits.length == 0)
                return;
            // end valid number


            if ($scope.discountType == 1) {
                if ($scope.discountValue > 100) {
                    $scope.discountValue = 100;
                }
                $scope.service.activeCart.products[index].adjusted_price = $scope.service.activeCart.products[index].price * (1 - $scope.discountValue / 100);
                $scope.adjustedPrice = $scope.service.activeCart.products[index].adjusted_price;
            }

            if ($scope.discountType == 0) {
                if ($scope.discountValue > $scope.service.activeCart.products[index].price) {
                    $scope.discountValue = $scope.service.activeCart.products[index].price;
                }
                $scope.service.activeCart.products[index].adjusted_price = $scope.service.activeCart.products[index].price - $scope.discountValue;
                $scope.adjustedPrice = $scope.service.activeCart.products[index].adjusted_price;
            }
            assignAttributes(index);
        };
        $scope.discountTypeChanged = function (product) {
            var index = $scope.service.activeCart.products.indexOf(product);
            if ($scope.discountValue.length == 0) {
                $scope.service.activeCart.products[index].adjusted_price = $scope.service.activeCart.products[index].price;
                $scope.adjustedPrice = $scope.service.activeCart.products[index].adjusted_price;
                $scope.discountValue = '';
                return;
            }
            if ($scope.discountType == 1) {
                if ($scope.discountValue > 100) {
                    $scope.discountValue = 100;
                }
                $scope.service.activeCart.products[index].adjusted_price = $scope.service.activeCart.products[index].price * (1 - $scope.discountValue / 100);
                $scope.adjustedPrice = $scope.service.activeCart.products[index].adjusted_price;
            }

            if ($scope.discountType == 0) {
                if ($scope.discountValue > $scope.service.activeCart.products[index].price) {
                    $scope.discountValue = $scope.service.activeCart.products[index].price;
                }
                $scope.service.activeCart.products[index].adjusted_price = $scope.service.activeCart.products[index].price - $scope.discountValue;
                $scope.adjustedPrice = $scope.service.activeCart.products[index].adjusted_price;
            }
            assignAttributes(index);
        };

    });

})();