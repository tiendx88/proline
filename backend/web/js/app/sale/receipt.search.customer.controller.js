/**
 * Created by Designer on 10/1/2014.
 */
(function () {

    angular.module('mRetailer').controller('CustomerReceiptSearchCtrl', function ($scope, $rootScope, SaleService,ReceiptService) {

        $scope.service = SaleService;
        $scope.receiptService=ReceiptService;


        var autocomplete_customer_receipt = new Bloodhound({
            datumTokenizer: function (d) {
                 return Bloodhound.tokenizers.whitespace(d.name);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: $scope.service.data.customers

        });
        autocomplete_customer_receipt.initialize();

        $('#autocomplete_customer_receipt .typeahead').typeahead({
                highlight: true
            },
            {
                name: 'customer_receipt_search_id',
                displayKey: 'name',
                source: autocomplete_customer_receipt.ttAdapter(),
                templates: {
                    empty: [
                        '<div class="empty-message">',
                        'Không tồn tại',
                        '</div>'
                    ].join('\n'),
                    suggestion: Handlebars.compile('<div><p><strong>{{name}}</strong> </p> </div>')
                }
            }).on('typeahead:selected', function (obj, datum, name) {
                $scope.receiptService.receipt={};
                $scope.receiptService.receipt.customerName=datum.name;
                $scope.receiptService.receipt.customerBalance=datum.balance;
                $scope.receiptService.receipt.customerId=datum.id;
                $('#autocomplete_customer_receipt .typeahead').typeahead('val', null);
                $scope.receiptService.action.getOrderOfCustomer(datum.id)
                    .done(function (result) {
                        $scope.receiptService.receipt.listInvoice=result;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    });




            });

    });

})();