(function () {

    angular.module('mRetailer').controller('SaleController', function ($scope, $rootScope, $modal, $timeout, $compile, $filter, SaleService, ConstService) {

        $scope.service = SaleService;
        $scope.const = ConstService;

        $scope.model = {};
        $scope.service.data.products = serverJson.productJson;
        $scope.service.data.customers = serverJson.customersJson;
        $scope.service.data.priceList = serverJson.priceListJson;
        $scope.service.data.staffs = serverJson.staffListJson;
        $scope.service.data.categories = serverJson.categoryListJson;
        $scope.service.data.currentUserId = serverJson.currentUserId;
        $scope.service.data.functionType = serverJson.functionType;
        $scope.service.data.storeId = serverJson.storeId;
        $scope.btnSave = $scope.const.invoice.BTN_SAVE_LABEL_INVOICE;
        $scope.btnConvertInvoice = $scope.const.invoice.BTN_LABEL_CONVERT_TO_INVOICE;
        $scope.btnCancelOrder = $scope.const.invoice.BTN_LABEL_CANCEL_ORDER;
        $scope.btnRefund = $scope.const.invoice.BTN_LABEL_REFUND;
        // compiler note


        //end stepper
        if ($scope.service.data.priceList.length > 0) {
            $scope.price_list_item = $scope.service.data.priceList[0];
        }

        $scope.getAllProduct = function (price_list_item) {
            SaleService.action.getAllProduct(price_list_item.id)
                .done(function (result) {
                    console.log(result);
                    $scope.service.activeCart.productSearch = result;
                    console.log($scope.service.data.productBloodhound);
                    $scope.service.action.changeResourceTypeHead($scope.service.data.productBloodhound, $scope.service.activeCart.productSearch);
                    // change price in active cart
                    angular.forEach($scope.service.activeCart.products, function (product) {
                        angular.forEach($scope.service.activeCart.productSearch, function (productSearch) {
                            if (product.id == productSearch.id) {
                                product.price = productSearch.price;
                                product.adjusted_price = productSearch.price;
                                product.discount = '';
                                product.discountType = 0;
                                product.discountValue = '';

                            }
                        });
                    });
                    // end change
                    $scope.$apply();
                });

        };
        $scope.removeCartProduct = function (product) {
            SaleService.action.removeCartProduct($scope.service.activeCart.products, product);
        };
        $scope.repeatCartProductDone = function (product) {
            console.log('xxx');
            var index = $scope.service.activeCart.products.indexOf(product);
            $('.note-popover').popover({
                html: true,
                placement: 'bottom',
                content: function () {
                    return $compile('<div ng-controller="ProductNoteCtrl"  ng-init="initNote(' + index + ')">' +
                    '<textarea ng-model="note" ng-change="onNoteChange(service.activeCart.products[' + index + '])" placeholder="Ghi chú" style="width: 200px; height: 45px" class="ng-valid ng-dirty"></textarea>' +
                    '</div>')($scope);
                }
            });
            $('.note-popover').on('shown.bs.popover', function () {
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            });
            $('.adjusted-price-popover').popover({
                html: true,
                placement: 'left',
                content: function () {
                    return $compile('<div ng-controller="ProductPriceCtrl" class="ng-scope" ng-init="init(' + index + ')">' +
                    '<table cellpadding="10">' +
                    '<tr >' +
                    '<td>Giá mới&nbsp;</td>' +
                    '<td><input type="text" style="height:30px" ng-model="adjustedPrice" ng-change="adjustedPriceChanged(service.activeCart.products[' + index + '])" class="ng-valid ng-dirty" ></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>Giảm giá&nbsp;</td>' +
                    '<td>' +
                    '<input type="text" style="height:30px;margin-right:5px"  size=8 ng-model="discountValue" ng-change="discountOnProductChanged(service.activeCart.products[' + index + '])" id="priceInput" class="ng-valid ng-dirty"  >' +
                    '<div class="btn-group btn-group-sm">' +
                    '<label class="btn btn-success" ng-model="discountType" btn-radio="0" uncheckable ng-change="discountTypeChanged(service.activeCart.products[' + index + '])">VND</label>' +
                    '<label class="btn btn-default" ng-model="discountType" btn-radio="1" uncheckable ng-change="discountTypeChanged(service.activeCart.products[' + index + '])">%</label>' +
                    '</div>' +
                    '</td>' +
                    '</tr>' +
                    '</table>' +
                    '</div>')($scope);

                }

            });
            $('.adjusted-price-popover').on('shown.bs.popover', function () {
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            });
            $('.inventory-warring-popover').popover({
                html: true,
                placement: 'right',
                content: function () {
                    return $compile('<div>' +
                    '<span>Tồn: ' + product.inventory + '</span>' +
                    '</div>')($scope);
                }

            });
            $('.inventory-warring-popover').on('shown.bs.popover', function () {
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            });
            if (!$scope.$$phase) {
                $scope.$apply();
            }


        };
        var sumInvoiceAmount = function () {
            var totalAmount = 0;
            if ($scope.service.activeCart.products.length === 0) {
                $scope.service.activeCart.currentSummary.discount = 0;
            }
            $scope.service.activeCart.numb_product = 0;
            angular.forEach($scope.service.activeCart.products, function (product, key) {
                totalAmount += product.adjusted_price * product.quantity;
                $scope.service.activeCart.numb_product += parseInt(product.quantity);
            });
            $scope.service.activeCart.currentSummary.totalAmount = totalAmount;
            $scope.service.activeCart.currentSummary.paidAmount = $scope.service.activeCart.currentSummary.totalAmount - $scope.service.activeCart.currentSummary.discount;
        }
        var validInventory = function () {

            angular.forEach($scope.service.activeCart.products, function (product, key) {
                if (product.quantity >= product.inventory) {
                    product.quantityOver = true;
                    product.quantity = product.inventory;
                }
                else {
                    product.quantityOver = false;
                }
            });

        }

        $scope.selectedProductPrice = function selectedProductPrice(product) {
            var index = $scope.service.activeCart.products.indexOf(product);
            $('.price-changed').show();
        };

        // sumary invoice
        $scope.$watch('service.activeCart.products', function () {
            sumInvoiceAmount();
            validInventory();
        }, true);

        $scope.$watch('service.activeCart.currentSummary.discount', function () {
            sumInvoiceAmount();
        }, true);

        $scope.$watch('service.activeCart.currentSummary.paidAmount', function () {
            $scope.service.activeCart.currentSummary.payingAmount = $scope.service.activeCart.currentSummary.paidAmount;
            $scope.service.activeCart.currentSummary.payRefund = $scope.service.activeCart.currentSummary.payingAmount - $scope.service.activeCart.currentSummary.paidAmount;
        }, true);
        $scope.$watch('service.activeCart.showErrorNull', function () {
            if ($scope.service.activeCart.showErrorNull) {
                $timeout(function () {
                    $scope.service.activeCart.showErrorNull = false;
                }, 2000);

            }
        }, true);
        $scope.$watch('service.activeCart.saveSuccess', function () {
            if ($scope.service.activeCart.saveSuccess) {
                $timeout(function () {
                    $scope.service.activeCart.saveSuccess = false;

                }, 3000);

            }
        }, true);

        $scope.$watch('service.activeCart.saveError', function () {
            if ($scope.service.activeCart.saveError) {
                $timeout(function () {
                    $scope.service.activeCart.saveError = false;
                }, 3000);

            }
        }, true);

        $scope.removeCustomer = function () {
            $scope.service.activeCart.customer = null;

        }
        $scope.payingAmountChanged = function () {

            //valid number
            var paying_digits = $scope.service.activeCart.currentSummary.payingAmount.replace(/[^0-9.]/g, '');

            if (paying_digits !== $scope.service.activeCart.currentSummary.payingAmount) {
                $scope.service.activeCart.currentSummary.payingAmount = parseFloat(paying_digits);
            }
            if (paying_digits.length == 0) {
                return;

            }
            $scope.service.activeCart.currentSummary.payRefund = $scope.service.activeCart.currentSummary.payingAmount - $scope.service.activeCart.currentSummary.paidAmount;

            // end valid number

        };
        $scope.changeInvoiceType = function (order_type) {
            $scope.service.data.functionType = order_type;
            if (order_type == $scope.const.invoice.INVOICE_TYPE_INVOICE) {
                $scope.btnSave = $scope.const.invoice.BTN_SAVE_LABEL_INVOICE;
            }
            if (order_type == $scope.const.invoice.INVOICE_TYPE_ORDER) {
                $scope.btnSave = $scope.const.invoice.BTN_SAVE_LABEL_ORDER;
            }
            var count = 0;
            angular.forEach($scope.service.activeCart.summaryList, function (summary, key) {
                if (summary.functionType == order_type) {
                    count++;
                    $scope.service.activeCart.currentSummary = summary;
                }

            });
            if (count == 0) {
                var currentSummary = {
                    functionType: $scope.service.data.functionType,
                    discount: 0,
                    discountType: 0,
                    totalAmount: $scope.service.activeCart.currentSummary.totalAmount,// tong tien don hang
                    paidAmount: $scope.service.activeCart.currentSummary.paidAmount,// tien khach can tra

                    payingAmount: 0,// tien khach tra
                    payRefund: 0, // tien tra lai,
                    payRefundType: 0,// 0: tien thua tra khach,1: cong vao tai khoan
                    payRefundShowAll: false// hien thi khi add client
                };
                $scope.service.activeCart.summaryList.push(currentSummary);
                $scope.service.activeCart.currentSummary = currentSummary;
            }
            var tab_title = ($scope.service.data.functionType == $scope.const.invoice.INVOICE_TYPE_INVOICE) ? $scope.const.invoice.INVOICE_LABEL : $scope.const.invoice.ORDER_LABEL
            $scope.service.activeCart.name = tab_title + $scope.service.activeCart.cart_id;
            if (!$scope.$$phase) {
                $scope.$apply();
            }


        };
        $scope.openFilterModal = function () {

            var modalInstance = $modal.open({
                templateUrl: 'filterModalContent.html',
                controller: 'FilterModalCtrl',
                size: 'sm',
                resolve: {}
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });

        };

        $scope.openSearchOrder = function () {

            var modalInstance = $modal.open({
                templateUrl: 'searchOrder.html',
                controller: 'SearchOrderCtrl',
                size: 'lg',
                resolve: {}
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
            modalInstance.opened.then(function () {

                $timeout(function () {
                    $scope.initSearchOrder();
                }, 200);

            });
        };
        $scope.openSearchInvoice = function () {

            var modalInstance = $modal.open({
                templateUrl: 'searchInvoice.html',
                controller: 'SearchInvoiceCtrl',
                size: 'lg',
                resolve: {}
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
            modalInstance.opened.then(function () {

                $timeout(function () {
                    $scope.initSearchInvoice();
                }, 200);

            });
        };

        $scope.openCreateReceiptModal = function () {

            var modalInstance = $modal.open({
                templateUrl: 'createReceiptModal.html',
                controller: 'CreateCustomerReceiptCtrl',
                size: 'lg',
                resolve: {}
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
            modalInstance.opened.then(function () {

                $timeout(function () {
                    $scope.initCreateReceipt();
                }, 200);

            });
        };

        $scope.addProductToCart = function (product) {
            for (var i = 0; i < $scope.service.activeCart.products.length; i++) {
                var item = $scope.service.activeCart.products[i];
                if (item.id == product.id) {
                    item.quantity++;
                    return;
                }
            }
            var product_tmp = {};
            //create product
            product_tmp.name = product.name;
            product_tmp.code = product.code;
            product_tmp.id = product.id;
            product_tmp.price = product.price;
            product_tmp.adjusted_price = product.price;
            product_tmp.inventory = product.inventory;
            product_tmp.quantity = 1;
            product_tmp.discountType = 0; //0: vnd, 1: %
            product_tmp.discountValue = '';
            product_tmp.note = '';

            $scope.service.activeCart.products.push(product_tmp);
        };

        $scope.pagingNext = function () {
            if ($scope.service.activeCart.currentPage == $scope.service.activeCart.totalPage) {
                return;
            }
            $scope.service.activeCart.currentPage++;
            $filter('filterByCategory')($scope.service.activeCart.products, $scope.service.activeCart.category_id, $scope.service.activeCart.currentPage);
        };
        $scope.pagingPrev = function () {
            if ($scope.service.activeCart.currentPage == 1) {
                return;
            }
            $scope.service.activeCart.currentPage--;
            $filter('filterByCategory')($scope.service.activeCart.products, $scope.service.activeCart.category_id, $scope.service.activeCart.currentPage);
        };

        $scope.init = function () {
            $(function () {

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var hour = today.getHours();
                var min = today.getMinutes();
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                if (min < 10) {
                    min = '0' + min;
                }
                if (hour < 10) {
                    hour = '0' + hour;
                }
                $('#invoice_datetimepicker').datetimepicker({
                    dayOfWeekStart: 1,
                    value: dd + '/' + mm + '/' + yyyy + ' ' + hour + ':' + min,
                    lang: 'vi',
                    format: 'd/m/Y H:i'
                });
                $scope.service.activeCart.invoice_datetime = dd + '/' + mm + '/' + yyyy + ' ' + hour + ':' + min;
                if ($scope.service.data.functionType == 2) {

                    $scope.btnSave = "Đặt hàng";

                }
                if (!$scope.$$phase) {
                    $scope.$apply();
                }

            });
        };
        $scope.initSearchOrder = function () {
            // $(function () {

            $('#order_from_date').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                format: 'd/m/Y'
            });
            $('#order_to_date').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                format: 'd/m/Y'
            });
            if (!$scope.$$phase) {
                $scope.$apply();
            }
            SaleService.action.getOrderViaFilter($scope.service.data.storeId, null, null, null, null)
                .done(function (result) {

                    $scope.service.data.listOrder = result;
                    console.log($scope.service.data.listOrder);
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });


        };


        $scope.initSearchInvoice = function () {

            $('#invoice_from_date').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                format: 'd/m/Y'
            });
            $('#invoice_to_date').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                format: 'd/m/Y'
            });
            if (!$scope.$$phase) {
                $scope.$apply();
            }
            SaleService.action.getInvoiceViaFilter($scope.service.data.storeId, null, null, null, null,1,5)
                .done(function (result) {

                    $scope.service.data.listInvoice = result['list_invoice'];
                    $scope.service.invoiceModal.totalItems=result['total_record'];
                    $scope.service.invoiceModal.currentPage = 1;
                    $scope.service.invoiceModal.postPerPage=5;
                    console.log($scope.service.invoiceModal.totalItems);
                    console.log($scope.service.data.listInvoice);
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });


        };
        $scope.initCreateReceipt = function () {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var hour = today.getHours();
            var min = today.getMinutes();
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            $('#receipt_datetimepicker').datetimepicker({
                dayOfWeekStart: 1,
                value: dd + '/' + mm + '/' + yyyy + ' ' + hour + ':' + min,
                lang: 'vi',
                format: 'd/m/Y H:i'
            });
            if (!$scope.$$phase) {
                $scope.$apply();
            }



        };

        $scope.saveInvoice = function () {
            if ($scope.service.activeCart.products.length == 0) {
                $scope.service.activeCart.showErrorNull = true;
                return;
            }
            $scope.service.activeCart.storeId = $scope.service.data.storeId;
            var saveResult = $scope.service.action.saveInvoice($scope.service.activeCart, $scope.service.data.functionType);

            saveResult.done(function (result) {
                if ($scope.service.data.functionType == $scope.const.invoice.INVOICE_TYPE_INVOICE) {
                    if (result['error'] == 0) {
                        //  $scope.service.activeCart.saveSuccess = true;
                        $scope.print();
                        $scope.removeActiveCart();
                    }
                    else {
                        $scope.service.activeCart.saveError = true;
                        $scope.service.activeCart.saveErrorMessage = result['message'];
                    }
                }
                else {
                    if (result['error'] == 0) {
                        $scope.service.activeCart.saveSuccess = true;
                        $scope.service.activeCart.saveSuccessMessage = result['message'];


                        $scope.print();
                        $scope.removeActiveCart();
                    }
                    else {
                        $scope.service.activeCart.saveError = true;
                        $scope.service.activeCart.saveErrorMessage = result['message'];
                    }
                }

                if (!$scope.$$phase) {
                    $scope.$apply();
                }

            });

        };
        /**
         * luu tra hang
         */
        $scope.saveRefund = function () {
            if ($scope.service.activeCart.products.length == 0 || $scope.service.activeCart.numb_product == 0) {
                $scope.service.activeCart.showErrorNull = true;
                return;
            }
            var saveRefund = $scope.service.action.saveRefund($scope.service.activeCart);

            saveRefund.done(function (result) {

                if (result['error'] == 0) {
                    $scope.service.activeCart.saveSuccess = true;
                    $scope.service.activeCart.saveSuccessMessage = result['message'];

                    $scope.print();
                    $scope.removeActiveCart();
                }
                else {
                    $scope.service.activeCart.saveError = true;
                    $scope.service.activeCart.saveErrorMessage = result['message'];
                }


                if (!$scope.$$phase) {
                    $scope.$apply();
                }

            });

        };
        $scope.removeActiveCart = function () {
            var index = $scope.service.carts.indexOf($scope.service.activeCart);

            $scope.service.carts.splice(index, 1);
            if ($scope.service.carts.length == 0) {

                var currentStaff = null;
                var currentPriceList = null;
                angular.forEach($scope.service.data.staffs, function (staff) {
                    if (staff.id == $scope.service.data.currentUserId) {
                        currentStaff = staff;

                    }
                });
                angular.forEach($scope.service.data.priceList, function (price) {
                    if (!price.id) {
                        currentPriceList = price;

                    }
                });
                var currentSummary = {
                    functionType: $scope.service.data.functionType,
                    discount: 0,
                    discountType: 0,
                    totalAmount: 0,// tong tien don hang
                    paidAmount: 0,// tien khach can tra

                    payingAmount: 0,// tien khach tra
                    payRefund: 0, // tien tra lai,
                    payRefundType: 0,// 0: tien thua tra khach,1: cong vao tai khoan
                    payRefundShowAll: false// hien thi khi add client
                };
                var max_tab_id = 1;
                $scope.service.tab.max_tab_id = max_tab_id;
                var summaryList = [];
                summaryList.push(currentSummary);
                $scope.title = ($scope.service.data.functionType == $scope.const.invoice.INVOICE_TYPE_INVOICE) ? $scope.const.invoice.INVOICE_LABEL : $scope.const.invoice.ORDER_LABEL;
                var activeCart = {
                    cart_id: max_tab_id,
                    name: $scope.title + max_tab_id,
                    active: true,
                    products: [],
                    category_id: '',
                    productSearch: angular.copy($scope.service.data.products),
                    currentPriceList: currentPriceList,
                    summaryList: summaryList,
                    currentSummary: currentSummary,
                    //customer
                    staff: currentStaff,
                    // pager
                    currentPage: 1,
                    postPerPage: 4,
                    totalPage: Math.ceil($scope.service.data.products.length / 4),
                    // error
                    showErrorNull: false,
                    current_tab: $scope.const.invoice.PAGE_CREATE

                };
                $scope.service.carts.push(activeCart);
                $scope.service.activeCart = activeCart;

            }
            else {
                $scope.service.activeCart = $scope.service.carts[0];
            }
            if (!$scope.$$phase) {
                $scope.$apply();
            }

        }

        $scope.cancelOrder = function () {

            var saveResult = $scope.service.action.cancelOrder($scope.service.activeCart.order_id, $scope.service.data.storeId);

            saveResult.done(function (result) {

                $scope.service.activeCart.saveSuccess = true;
                $scope.service.activeCart.saveSuccessMessage = result['message'];
                $scope.print();
                $scope.removeActiveCart();
                if (!$scope.$$phase) {
                    $scope.$apply();
                }

            });

        };
        $scope.changeToInvoice = function () {

            var saveResult = $scope.service.action.changeToInvoice($scope.service.activeCart.order_id, $scope.service.data.storeId);

            saveResult.done(function (result) {

                $scope.service.activeCart.saveSuccess = true;
                $scope.service.activeCart.saveSuccessMessage = result['message'];
                $scope.print();
                $scope.removeActiveCart();
                if (!$scope.$$phase) {
                    $scope.$apply();
                }

            });

        };

        $scope.print = function () {
            // $('#print').printArea();
            var html = '<div ng-controller="PrintInvoiceCtrl" ng-init="init()" style="margin-left: 100px">'
                + '<div class="row">' +
                '<table>' +
                '<tr>' +
                '<td>Tên cửa hàng</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Ngày bán:</td>' +
                '<td>{{invoice_date}}</td>' +
                '</tr>' +
                '<tr><td colspan="2">Hóa đơn bán hàng</td></tr>' +
                '<tr><td colspan="2"><i>{{cart_name}}</i></td></tr>' +
                '</table>' +
                '<table class="table">' +
                '<thead>' +
                '<tr>' +
                '<th>Sản phẩm</th><th>Giá</th><th>Số lượng</th><th>Thành tiền</th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +

                '<tr ng-repeat="product in products">' +
                '<td>{{product.name}}</td>' +
                '<td>{{product.adjusted_price}}</td>' +
                '<td>{{product.quantity}}</td>' +
                '<td>{{product.adjusted_price*product.quantity}}</td>' +
                '</tr>' +
                '</tbody>' +

                '</table>' +
                '<div style="margin-left: 400px">' +
                '<p>Tổng tiền hàng: {{totalAmount}}</p>' +
                '<p>Chiết khấu: {{discount}}</p>' +
                '<p>Thành tiền: {{totalAmount-discount}}</p>' +
                '</div>' +
                ' </div>'
                + '</div>';

            var template = angular.element(html);

            var linkFn = $compile(template);

            var element = linkFn($scope);
            $timeout(function () {
                element.printArea();
            }, 300);


        };
        $scope.showInventoryWarring = function (product) {

        }


    })

})();