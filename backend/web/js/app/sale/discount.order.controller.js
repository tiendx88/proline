/**
 * Created by Designer on 10/4/2014.
 */
(function () {

    angular.module('mRetailer').controller('DiscountOrderCtrl', function ($scope, $rootScope, $compile, SaleService) {

        $scope.service = SaleService;
        $scope.discountOrderType = 0; // 0: vnd, 1: %
        $scope.discountOrderValue = '';

        $scope.discountOrderValueOld = '';

        $scope.init = function () {
            if($scope.service.activeCart.currentSummary){
                $scope.discountOrderType = $scope.service.activeCart.currentSummary.discountType; // 0: vnd, 1: %
                $scope.discountOrderValue = $scope.service.activeCart.currentSummary.discount;
            }
            else{
                $scope.discountOrderType = 0; // 0: vnd, 1: %
                $scope.discountOrderValue = 0;

            }

         }

        $scope.discountOrderChanged = function () {

            if ($scope.discountOrderValue.length == 0) {
                $scope.service.activeCart.currentSummary.discount = 0;
                return;
            }
            //valid number

            var digits = $scope.discountOrderValue.replace(/[^0-9.]/g, '');
            if (digits !== $scope.discountOrderValue) {
                $scope.discountOrderValue = parseFloat(digits)
            }

            if (digits.length == 0) {
                 return;
            }

            // end valid number
            if ($scope.discountOrderType == 1) {
                if ($scope.discountOrderValue > 100) {
                    $scope.discountOrderValue = 100;
                }
                $scope.service.activeCart.currentSummary.discount = $scope.service.activeCart.currentSummary.totalAmount * $scope.discountOrderValue / 100;
                console.log('sfdfd'+$scope.discountOrderValue);
            }

            if ($scope.discountOrderType == 0) {
                if ($scope.discountOrderValue > $scope.service.activeCart.currentSummary.totalAmount) {
                    $scope.discountOrderValue = $scope.service.activeCart.currentSummary.totalAmount;
                }
                $scope.service.activeCart.currentSummary.discount = $scope.discountOrderValue;
             }

            $scope.service.activeCart.currentSummary.discountType = $scope.discountOrderType;
        };
        $scope.discountOrderTypeChanged = function (discountType) {

            if ($scope.discountOrderValue.length == 0) {
                $scope.service.activeCart.currentSummary.discount = 0;
                return;
            }
            if(discountType==$scope.discountOrderType)
            {
                return;
            }
            $scope.discountOrderType=discountType;
            if ($scope.discountOrderType == 1) {
                $scope.discountOrderValue = $scope.discountOrderValue / $scope.service.activeCart.currentSummary.totalAmount * 100;
            }

            if ($scope.discountOrderType == 0) {
                $scope.discountOrderValue = $scope.discountOrderValue * $scope.service.activeCart.currentSummary.totalAmount / 100;
            }
            $scope.service.activeCart.currentSummary.discountType = $scope.discountOrderType;

        };
        angular.element(document).ready(function () {

            $('.order-discount-popover').popover({
                html: true,
                placement: 'left',
                content: function () {
                    return $compile($("#discount-order-popover").html())($scope);
                }
            });
            $('.order-discount-popover').on('shown.bs.popover', function () {
                $rootScope.$apply();
            })
        });
    });


})();