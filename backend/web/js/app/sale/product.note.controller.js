/**
 * Created by Designer on 10/4/2014.
 */
(function () {

    angular.module('mRetailer').controller('ProductNoteCtrl', function ($scope,$timeout, $rootScope, SaleService) {

        $scope.service = SaleService;
         $scope.note='';
        $scope.$watch('note', function () {

                $timeout(function () {
                     if(!$rootScope.$phase) {
                         $rootScope.$apply();
                    }
                }, 100);


        }, true);

        $scope.initNote=function(index){

            $scope.note= $scope.service.activeCart.products[index].note;

        }

        $scope.onNoteChange = function (product) {
            var index = $scope.service.activeCart.products.indexOf(product);
            $scope.service.activeCart.products[index].note=$scope.note;

        }


    });

})();