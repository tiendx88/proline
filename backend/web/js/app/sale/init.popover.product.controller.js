(function () {

    angular.module('mRetailer').controller('InitPopOverController', function ($scope, $rootScope, $modal, $timeout, $compile, $filter, SaleService,ConstService) {

        $scope.service = SaleService;
        $scope.const=ConstService;

         $scope.init = function (product) {

             console.log('xxx');
            var index = $scope.service.activeCart.products.indexOf(product);
             console.log($scope.service.activeCart.products);
            $('.note-popover').popover({
                html: true,
                placement: 'bottom',
                content: function () {
                    return $compile('<div ng-controller="ProductNoteCtrl"  ng-init="initNote(' + index + ')">' +
                    '<textarea ng-model="note" ng-change="onNoteChange(service.activeCart.products[' + index + '])" placeholder="Ghi chú" style="width: 200px; height: 45px" class="ng-valid ng-dirty"></textarea>' +
                    '</div>')($scope);
                }
            });
            $('.note-popover').on('shown.bs.popover', function () {
                if(!$scope.$$phase) {
                    $scope.$apply();
                }
            });
            $('.adjusted-price-popover').popover({
                html: true,
                placement: 'left',
                content: function () {
                    return $compile('<div ng-controller="ProductPriceCtrl" class="ng-scope" ng-init="init(' + index + ')">' +
                    '<table cellpadding="10">' +
                    '<tr >' +
                    '<td>Giá mới&nbsp;</td>' +
                    '<td><input type="text" style="height:30px" ng-model="adjustedPrice" ng-change="adjustedPriceChanged(service.activeCart.products[' + index + '])" class="ng-valid ng-dirty" ></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>Giảm giá&nbsp;</td>' +
                    '<td>' +
                    '<input type="text" style="height:30px;margin-right:5px"  size=8 ng-model="discountValue" ng-change="discountOnProductChanged(service.activeCart.products[' + index + '])" id="priceInput" class="ng-valid ng-dirty"  >' +
                    '<div class="btn-group btn-group-sm">' +
                    '<label class="btn btn-success" ng-model="discountType" btn-radio="0" uncheckable ng-change="discountTypeChanged(service.activeCart.products[' + index + '])">VND</label>' +
                    '<label class="btn btn-default" ng-model="discountType" btn-radio="1" uncheckable ng-change="discountTypeChanged(service.activeCart.products[' + index + '])">%</label>' +
                    '</div>' +
                    '</td>' +
                    '</tr>' +
                    '</table>' +
                    '</div>')($scope);

                }

            });
            $('.adjusted-price-popover').on('shown.bs.popover', function () {
                if(!$scope.$$phase) {
                    $scope.$apply();
                }
            });
            $('.inventory-warring-popover').popover({
                html: true,
                placement: 'right',
                content: function () {
                    return $compile('<div>' +
                    '<span>Tồn: '+product.inventory+'</span>' +
                    '</div>')($scope);
                }

            });
            $('.inventory-warring-popover').on('shown.bs.popover', function () {
                if(!$scope.$$phase) {
                    $scope.$apply();
                }
            });
             if(!$scope.$$phase) {
                 $scope.$apply();
             }



        };

    });

})();