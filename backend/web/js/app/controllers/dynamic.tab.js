/**
 * Created by Designer on 10/1/2014.
 */
module = angular.module('mRetailer');
module.controller("DynamicTabController", function ($scope, SaleService,ConstService) {
    $scope.service = SaleService;
    $scope.service.tab={};

    $scope.const=ConstService;
    $scope.max_tab_id = 0;

    $scope.title = '';

    var setAllInactive = function () {
        angular.forEach($scope.service.carts, function (cart) {
            cart.active = false;
        });
    };

    var addNewOrder = function () {
        if($scope.service.tab.max_tab_id>$scope.max_tab_id){
            $scope.max_tab_id=$scope.service.tab.max_tab_id;
        }
        $scope.max_tab_id++;
        $scope.service.tab.max_tab_id=$scope.max_tab_id;
        var currentStaff=null;
        var currentPriceList=null;
        angular.forEach($scope.service.data.staffs, function (staff) {
            if(staff.id==$scope.service.data.currentUserId){
                currentStaff=staff;

            }
        });
        angular.forEach($scope.service.data.priceList, function (price) {
            if(!price.id){
                currentPriceList=price;

            }
        });
        var currentSummary={
                functionType:$scope.service.data.functionType,
                discount:0,
                discountType:0,
                totalAmount:0,// tong tien don hang
                paidAmount:0,// tien khach can tra

                payingAmount:0,// tien khach tra
                payRefund:0 , // tien tra lai,
                payRefundType:0,// 0: tien thua tra khach,1: cong vao tai khoan
                payRefundShowAll:false// hien thi khi add client
            };

        var summaryList=[];
        summaryList.push(currentSummary);
        $scope.title=($scope.service.data.functionType==$scope.const.invoice.INVOICE_TYPE_INVOICE)? $scope.const.invoice.INVOICE_LABEL:$scope.const.invoice.ORDER_LABEL;
         var activeCart = {
            cart_id: $scope.max_tab_id,
            name: $scope.title + $scope.max_tab_id,
            active: true,
            products: [],
            category_id: '',
            productSearch: angular.copy($scope.service.data.products),
            currentPriceList:currentPriceList,
            summaryList:summaryList,
            currentSummary:currentSummary,
            //customer
            staff: currentStaff,
            // pager
            currentPage:1,
            postPerPage:4,
            totalPage: Math.ceil($scope.service.data.products.length/4),
            // error
            showErrorNull:false,
             current_tab:$scope.const.invoice.PAGE_CREATE

        };
        $scope.service.carts.push(activeCart);
        $scope.service.activeCart=activeCart;

    };

    var closeTab = function closeTab(cart_id) {

        angular.forEach($scope.service.carts, function (cart) {
            if (cart.cart_id == cart_id) {
                var index = $scope.service.carts.indexOf(cart)
                $scope.service.carts.splice(index, 1);
            }
        });
        if ($scope.service.carts.length == 0) {
            $scope.max_tab_id = 0;
            $scope.addOrderTab();
        }
    };
    var selectTab = function selectTab(cart) {

        $scope.service.activeCart = cart;
        $scope.service.action.changeResourceTypeHead($scope.service.data.productBloodhound, $scope.service.activeCart.productSearch);

    };
    var initTab = function initTab() {
        if ($scope.service.carts.length == 0) {
            $scope.addOrderTab();

        }
    };

    $scope.addOrderTab = function () {
        setAllInactive();
        addNewOrder();

    };
    $scope.closeTab = closeTab;
    $scope.selectTab = selectTab;
    $scope.initTab = initTab;


});