<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'suffix' => '.html',
            'rules' => [
                '/' => 'site/index',
                '<title:[a-zA-Z0-9_ -]+>/<cateId:[a-zA-Z0-9_ -]+>' => 'site/product',
                'Profline-ve-chung-toi' => 'site/about',
                'Lien-he-voi-chung-toi-Profline' => 'site/contact',
                'Tin-tuc-Profline' => 'site/news',
                '<name:[a-zA-Z0-9_ -]+>/<id:[a-zA-Z0-9_ -]+>/<nid:[a-zA-Z0-9_ -]+>' => 'site/news-detail',
                '<lang:[a-zA-Z0-9_ -]+>' => 'site/change-language',
                //'<title:[a-zA-Z0-9_ -]+>/<id:\d+>' => 'product/detail',
                //'<title:[a-zA-Z0-9_ -]+>/<prod:[a-zA-Z0-9_ -]+>' => 'product/detail',
                '<title:[a-zA-Z0-9_ -]+>/<prod:[a-zA-Z0-9_ -]+>/detail/chi-tiet' => 'site/detail',
//                [
//                    'pattern' => '<title:[a-zA-Z0-9_ -]+>',
//                    'route' => 'product/detail',
//                    'suffix' => '.html',
//                ],
            ],
        ],

    ],
    'params' => $params,
];
