<?php
namespace frontend\controllers;

use common\models\About;
use common\models\Category;
use common\models\Contacts;
use common\models\News;
use common\models\NewsSearch;
use common\models\Product;
use common\models\ProductSearch;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup', 'change-language','detail','product'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'change-language'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }



    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $lang = Yii::$app->session->get('lang', '');
        if (empty($lang)) {
            Yii::$app->session->set('lang', 'vi');
        }

        return $this->render('home', [
        ]);
    }

    public function actionProduct($title, $cateId)
    {
        $lang = Yii::$app->session->get('lang', '');
        if (empty($lang)) {
            Yii::$app->session->set('lang', 'vi');
        }
        $list_cate = Category::getListCateChild($cateId);
        //var_dump($list_cate);exit;
        $list_product = Product::getList($cateId);
        $data = [];
        foreach ($list_cate as $cate) {
            /** @var $cate Category */
            $data[] = $cate;
            foreach ($list_product as $p) {
                /** @var $p Product */
                if($cate->category_id == $p->category_id) {
                   // array_push($data[]['pro'], $p);
                    $cate->listProduct[]= $p;
                }
            }
        }

        return $this->render('index', [
            'data' => $data,
            'listCate' => $list_cate,
            'listProduct' => $list_product
        ]);
    }

    public function actionDetail($title,$prod) {
        $id = Yii::$app->request->get('prod', 0);
        $pro = Product::findOne(['id' => $id]);

        return $this->render('detail',['pro' =>$pro]);
    }

    public function actionChangeLanguage()
    {
        $lang = Yii::$app->request->get('lang', 'vi');
        Yii::$app->session->set('lang', $lang);
        //var_dump($lang);exit;
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
       // return $this->redirect(['site/index']);
    }

    public function actionNews() {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->searchPortal(Yii::$app->request->queryParams);

        $paging = new Pagination();
        $paging->totalCount = $dataProvider->getTotalCount();
        $paging->defaultPageSize = 10;

        return $this->render('news_list', [
            'listNews' => $dataProvider->getModels(),
            'paging' => $paging,
        ]);
    }

    public function actionNewsDetail($name, $id,$nid) {
        $news = News::findOne(['id' => $id]);
        return $this->render('news_detail', [
           'model' => $news,
        ]);
    }
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    /*public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }*/
    public function actionContact()
    {
        $model = Contacts::find()->one();
        return $this->render('contact', [
            'contact' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $ab = About::findOne(['status' => About::STATUS_ACTIVE]);
        return $this->render('about', ['about' => $ab]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
