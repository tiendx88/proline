<?php
/**
 * Created by PhpStorm.
 * User: XuanTien
 * Date: 31/10/2016
 * Time: 9:50 AM
 */

namespace frontend\controllers;


use common\models\Category;
use common\models\Product;
use common\models\ProductCateAsm;
use common\models\ProductColorAsm;
use common\models\ProductSizeAsm;
use frontend\models\CustomerForm;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Cookie;

class ProductController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['buy-in-cart', 'sale-off', 'add-to-cart','remove-cart','cart','booking','booking-now','list','buy-now'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'booking' => ['get','post'],
                    'search' => ['get','post'],
                ],
            ],
        ];
    }

    public function actionBuyInCart() {
        $sizes = Yii::$app->request->get('size', []);

        //$cart = Yii::$app->session->set('cart',[]);exit;
        $cart = Yii::$app->session->get('cart',[]);
        if(!empty($cart)) {//da ton tai gio hang
            if(is_array($sizes) && !empty($sizes)) {
                foreach($sizes as $size) {
                    $price_size = explode(',', $size);
                    $color_id = isset($price_size[0]) ? $price_size[0] : 0;
                    $price_size_id = isset($price_size[1]) ? $price_size[1] : 0;
                    $quantity = isset($price_size[2]) ? $price_size[2]: 0;
                    $flg = false;
                    foreach($cart as &$c) {
                        if($c['size']->id == $price_size_id && $c['color']->id == $color_id) {//trùng vs sản phẩm đã mua
                            $c['soluong'] = $quantity;
                            $flg = true;
                            break;
                        } else {//ngược lại ko trùng
                            $flg = false;
                            continue;
                        }
                    }

                    if(!$flg) {
                        $pro_size = ProductSizeAsm::findOne(['id' => $price_size_id]);
                        $pro_color = ProductColorAsm::findOne(['id'=> $color_id]);
                        $product = Product::findOne(['id' => $pro_size->product_id]);
                        $cart[] = ['id'=> $product->id,'pro' => $product, 'soluong' => $quantity ,'size' => $pro_size,'color' => $pro_color];
                    }
                }


            }
        } else {
            if(is_array($sizes) && !empty($sizes)) {
                foreach($sizes as $size) {
                    $price_size = explode(',', $size);
                    $color_id = isset($price_size[0]) ? $price_size[0] : 0;
                    $price_size_id = isset($price_size[1]) ? $price_size[1] : 0;
                    $quantity = isset($price_size[2]) ? $price_size[2]: 0;

                    $pro_color = ProductColorAsm::findOne(['id'=> $color_id]);
                    $pro_size = ProductSizeAsm::findOne(['id' => $price_size_id]);
                    $product = Product::findOne(['id' => $pro_size->product_id]);
                    $cart[] = ['id'=> $product->id,'pro' => $product, 'soluong' => $quantity ,'size' => $pro_size,'color' => $pro_color];
                }
            }
        }


        if(!empty($cart)) {
            Yii::$app->session->set('cart' , $cart);
            return json_encode(['success' => true, 'message' => 'Đặt hàng thành công']);
        } else {
            return json_encode(['success' => false, 'message' => 'Đặt hàng không thành công']);
        }

    }


    public function actionBuyNow() {
        $productId = Yii::$app->request->get('prod_id', 0);
        $sizes = Yii::$app->request->get('size', []);
        //var_dump($sizes);exit;
        /** @var  $product Product */
        $product = Product::findOne(['id' => $productId]);
        if(empty($product)) {
            Yii::$app->session->setFlash('success','Sản phẩm không tồn tại');
            return $this->redirect(['site/index']);
        }
        $cart = [];

        if(is_array($sizes) && !empty($sizes)) {
            foreach($sizes as $size) {
                $price_size = explode(',', $size);
                $color_id = isset($price_size[0]) ? $price_size[0] : 0;
                $price_size_id = isset($price_size[1]) ? $price_size[1] : 0;
                $quantity = isset($price_size[2]) ? $price_size[2]: 0;
                $pro_size = ProductSizeAsm::findOne(['id' => $price_size_id]);
                $pro_color = ProductColorAsm::findOne(['id'=> $color_id]);
                $cart[] = ['id'=> $productId,'pro' => $product, 'soluong' => $quantity ,'size' => $pro_size,'color' => $pro_color];
            }
        }

        if(!empty($cart)) {
            Yii::$app->session->set('cart_now' , $cart);
            return json_encode(['success' => true, 'message' => 'Đặt hàng thành công']);
        } else {
            return json_encode(['success' => false, 'message' => 'Đặt hàng không thành công']);
        }

    }

    public function actionSearch() {
        $keyword = Yii::$app->request->get('keyword', '');
        $load = Yii::$app->request->get('load', '');
        $sort = Yii::$app->request->get('sort', 0);
        $category = new Category();
        $category->category_id = 1000000000;
        $category->category_name = 'tìm kiếm';
        $pageSize = 18;
        if(!empty($keyword)) {
            $keyword = trim($keyword);
            if(!empty($keyword)) {
                Yii::$app->session->set('keyword', $keyword);
            }else {
                Yii::$app->session->set('keyword', null);
            }
        } else {
            Yii::$app->session->set('keyword', null);
        }

        $list = Product::getListSearch($sort, $pageSize, $keyword);

        $paging = new Pagination();
        $paging->totalCount = $list->getTotalCount();
        $paging->defaultPageSize = $pageSize;
        if(!empty($load)) {
            return $this->renderPartial('_list_sort', ['listProduct' => $list->getModels()]);
        }
        return $this->render('listByCate', ['listProduct'=> $list->getModels(), 'paging' => $paging, 'category' => $category,'sort' => $sort]);
    }

    public function actionSaleOff() {
        $load = Yii::$app->request->get('load', '');
        $sort = Yii::$app->request->get('sort', 0);
        $category = new Category();
        $category->category_id = 1000000000;
        $category->category_name = 'Khuyến Mãi';
        $pageSize = 18;
        $list = Product::getListSaleOff($sort, $pageSize);

        $paging = new Pagination();
        $paging->totalCount = $list->getTotalCount();
        $paging->defaultPageSize = $pageSize;
        if(!empty($load)) {
            return $this->renderPartial('_list_sort', ['listProduct' => $list->getModels()]);
        }
        return $this->render('listByCate', ['listProduct'=> $list->getModels(), 'paging' => $paging, 'category' => $category,'sort' => $sort]);
    }

    public function actionList() {
        $load = Yii::$app->request->get('load', '');
        $cateId = Yii::$app->request->get('cateId', 0);
        $cateName = Yii::$app->request->get('cateName', 0);
        $sort = Yii::$app->request->get('sort', 0);
        if(empty($cateId) || $cateId <= 0) {
            Yii::$app->session->setFlash('success', 'Danh mục không hợp lệ, xin vui lòng thử lại.');
            return $this->goHome();
        }

        $category = Category::findOne(['category_id' => $cateId, 'status' => Category::STATUS_ACTIVE]);
        if(empty($category)) {
            Yii::$app->session->setFlash('success', 'Danh mục không hợp lệ, xin vui lòng thử lại.');
            return $this->goHome();
        }

        $list = Product::getListByCate($cateId, $sort);

        $paging = new Pagination();
        $paging->totalCount = $list->getTotalCount();
        $paging->defaultPageSize = 18;
        if(!empty($load)) {
            return $this->renderPartial('_list_sort', ['listProduct' => $list->getModels()]);
        }
        return $this->render('listByCate', ['listProduct'=> $list->getModels(), 'paging' => $paging, 'category' => $category,'sort' => $sort]);
    }

    public function actionDetail($title,$prod) {
        $id = Yii::$app->request->get('prod', 0);
        $pro = Product::findOne(['id' => $id]);

        return $this->render('detail',['pro' =>$pro]);
    }


    public function actionBooking() {
        $role = Yii::$app->request->get('role','');
        if(!empty($role) && $role == 'cart') {//thuc hien theo giỏ hàng
            $cart = Yii::$app->session->get('cart');
        } else {
            $cart = Yii::$app->session->get('cart_now'); //thuc hien mua ngay
        }

        if(null == $cart) {
            Yii::$app->session->setFlash('success', 'Giỏ hàng bị trống, xin lòng thêm sản phẩm vào giỏ');
            return $this->goHome();
        }

        $customer = new CustomerForm();
        if(Yii::$app->request->isPost) {
            $transporter_name = Yii::$app->get('transporter_name','');

            $customer->load(Yii::$app->request->post());
            if($customer->booking($cart)) {
                //send mail to user
                if(!empty($customer->email)) {
                    Yii::$app->mailer->compose('response_booking',['cart'=> $cart, 'customer' => $customer])
                        ->setFrom('thoitrangnhi@thoitrangnhi.com.vn')
                        ->setTo($customer->email)
                        ->setSubject("BabyCity's Shop - Thông tin đơn đặt hàng")
                        ->send();
                }

                //end send mail
                Yii::$app->session->setFlash('cart' , null);
                if(!empty($role) && $role == 'cart') {//thuc hien theo giỏ hàng
                    Yii::$app->session->remove('cart');
                } else {
                    Yii::$app->session->remove('cart_now');
                }


                Yii::$app->session->setFlash('success', 'Đơn hàng của quý khách đã được chấp nhận. Chúng tôi sẽ liên hệ sớm với quý khách. Xin cảm ơn');
                return $this->goHome();
            }
        }
        return $this->render('booking', ['cart' => $cart,'customer'=> $customer]);
    }
    public function actionBookingNow() {

        $cart = Yii::$app->session->get('cart_now');
        if(null == $cart) {
            Yii::$app->session->setFlash('success', 'Giỏ hàng bị trống, xin lòng thêm sản phẩm vào giỏ');
            return $this->goHome();
        }

        $customer = new CustomerForm();
        if(Yii::$app->request->isPost) {
            $customer->load(Yii::$app->request->post());
            if($customer->booking($cart)) {
                //send mail to user
                /*if(!empty($customer->email)) {
                    Yii::$app->mailer->compose('response_booking',['cart'=> $cart, 'customer' => $customer])
                        ->setFrom('thoitrangnhi@thoitrangnhi.com.vn')
                        ->setTo($customer->email)
                        ->setSubject("Phong's shop - Thông tin đơn đặt hàng")
                        ->send();
                }*/

                //end send mail
                Yii::$app->session->remove('cart_now');
                Yii::$app->session->setFlash('success', 'Đơn hàng của quý khách đã được chấp nhận. Chúng tôi sẽ liên hệ sớm với quý khách. Xin cảm ơn');
                return $this->goHome();
            }
        }
        return $this->render('booking', ['cart' => $cart,'customer'=> $customer]);
    }
    public function actionCart() {
        $cart = Yii::$app->session->get('cart');
        return $this->render('cart', ['cart' => $cart]);
    }

    public function actionRemoveCart() {
        $id = Yii::$app->request->get('id', 0);
        $size = Yii::$app->request->get('size', '');
        $cart = Yii::$app->session->get('cart');
        if(empty($cart)) {
            return json_encode(['success' => false, 'message' => 'Giỏ hàng của bạn đang trống.']);
        } else {
            if (is_array($cart)) {
                $found = false;
                foreach($cart as $key=> $val) {
                    if($val['id'] == $id && $val['size']->id == $size) {
                        unset($cart[$key]);
                        $found = true;
                        break;
                    }
                }
                /*for($i = 0; $i < count($cart); $i++) {
                    var_dump($cart);exit;
                    if($cart[$i]['id'] == $id) {echo 'vao';exit;
                        unset($cart[$i]);
                        $found = true;
                        break;
                    }
                }*/

                if($found) {
                    Yii::$app->session->set('cart' , $cart);
                    return json_encode(['success' => true, 'message' => 'Quý khách đã hủy sản phẩm khỏi giỏ hàng thành công.']);
                } else {
                    return json_encode(['success' => false, 'message' => 'Sản phẩm này không có trong giỏ hàng của bạn.']);
                }

            } else {
                return json_encode(['success' => false, 'message' => 'Hệ thống đang bận, xin quý khách vui lòng liên hệ qua hotline']);
            }
        }
    }

    public function actionAddToCart1() {
        $id = Yii::$app->request->get('id', 0);
        $size = Yii::$app->request->get('size', 1);
        $quantity = Yii::$app->request->get('quantity', 1);
        if(empty($id) || !is_numeric($id) || $id <= 0) {
            return json_encode(['success' => false, 'message' => 'Sản phẩm không tồn tại. Xin quý khách vui lòng kiểm tra lại']);
        }
        $cart = Yii::$app->session->get('cart');
        if(empty($cart)) {
            $cart = [];
            $product = Product::findOne(['id' => $id]);
            if(!empty($product)) {
                $cart[] = ['id'=> $id,'pro' => $product, 'soluong' => $quantity ,'size' => $size];
                Yii::$app->session->set('cart' , $cart);
                return json_encode(['success' => true, 'message' => 'Thêm vào giỏ hàng thành công']);
            } else {
                return json_encode(['success' => false, 'message' => 'Sản phẩm không tồn tại. Xin quý khách vui lòng kiểm tra lại']);
            }
        } else {
            if(is_array($cart)) {
                $product = Product::findOne(['id' => $id]);
                if(empty($product)) {
                    return json_encode(['success' => false, 'message' => 'Sản phẩm không tồn tại. Xin quý khách vui lòng kiểm tra lại']);
                } else {
                    $found = false;
                    foreach($cart as &$pro) {
                        if($pro['id'] == $id && $pro['size'] == $size) {
                            $pro['soluong'] = $quantity;
                            $found = true;
                            break;
                        }
                    }

                    if(!$found) {
                        $cart[] = ['id'=> $id,'pro' => $product, 'soluong' => $quantity ,'size' => $size];
                    }
                    Yii::$app->session->set('cart' , $cart);
                    return json_encode(['success' => true, 'message' => 'Thêm vào giỏ hàng thành công']);
                }

            } else {
                return json_encode(['success' => false, 'message' => 'Hệ thống đang bận, xin quý khách vui lòng liên hệ qua hotline']);
            }
        }
    }
    public function actionAddMoreToCart() {
        $id = Yii::$app->request->get('id', 0);
        $size = Yii::$app->request->get('size', 1);
        $quantity = Yii::$app->request->get('quantity', 1);
        if(empty($id) || !is_numeric($id) || $id <= 0) {
            return json_encode(['success' => false, 'message' => 'Sản phẩm không tồn tại. Xin quý khách vui lòng kiểm tra lại']);
        }
        $cart = Yii::$app->session->get('cart');
        if(empty($cart)) {
            $cart = [];
            $product = Product::findOne(['id' => $id]);
            if(!empty($product)) {
                $cart[] = ['id'=> $id,'pro' => $product, 'soluong' => $quantity ,'size' => $size];
                Yii::$app->session->set('cart' , $cart);
                return json_encode(['success' => true, 'message' => 'Thêm vào giỏ hàng thành công']);
            } else {
                return json_encode(['success' => false, 'message' => 'Sản phẩm không tồn tại. Xin quý khách vui lòng kiểm tra lại']);
            }
        } else {
            if(is_array($cart)) {
                $product = Product::findOne(['id' => $id]);
                if(empty($product)) {
                    return json_encode(['success' => false, 'message' => 'Sản phẩm không tồn tại. Xin quý khách vui lòng kiểm tra lại']);
                } else {
                    $found = false;
                    foreach($cart as &$pro) {
                        if($pro['id'] == $id && $pro['size'] == $size) {
                            $pro['soluong'] += $quantity;

                            $found = true;
                            break;
                        }
                    }

                    if(!$found) {
                        $cart[] = ['id'=> $id,'pro' => $product, 'soluong' => $quantity ,'size' => $size];
                    }
                    Yii::$app->session->set('cart' , $cart);
                    return json_encode(['success' => true, 'message' => 'Thêm vào giỏ hàng thành công']);
                }

            } else {
                return json_encode(['success' => false, 'message' => 'Hệ thống đang bận, xin quý khách vui lòng liên hệ qua hotline']);
            }
        }
    }

}