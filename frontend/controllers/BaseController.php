<?php
/**
 * Created by PhpStorm.
 * User: XuanTien
 * Date: 1/27/2016
 * Time: 10:43 PM
 */

namespace frontend\controllers;


use common\helpers\CUtils;
use Yii;
use yii\web\Controller;

class BaseController extends Controller
{
    const _NET_VNP = 'VNP';
    const _NET_VMS = 'VMS';
    const _NET_VIETTEL = 'VT';
    public $network = null;
    public $name = '';
    public $description = '';
    public $keywords = '';
    public $url = '';
    public $thumbnail = '';
    public $img_color = '';
    public $product = '';
    public $product_size = '';
    public $deviceType = '';
    public $detect = '';
    const TYPE_APK = 1;//anroid
    const TYPE_JAR = 2;//java
    const TYPE_IOS = 3;//ios
    const TYPE_PC = 4;//pc
    const TYPE_ANDROID_LABEL = 'Android';//game java
    const TYPE_JAR_LABEL = 'SymbianOS';//game java

    public function __construct($id, $module)
    {
        parent::__construct($id, $module);
        $clientIP = $this->getUserIP();
        $isFind = false;
        $network = '';
        $this->detect = new \Mobile_Detect();
        if ($this->detect->isMobile()) {
            if ($this->detect->isTablet() && $this->detect->is('AndroidOS')) {
                $this->deviceType = self::TYPE_APK;
            } else if ($this->detect->is('iOS')) {
                $this->deviceType = self::TYPE_APK;
            } else if($this->detect->isTablet() && $this->detect->is('iOS')){
                $this->deviceType = self::TYPE_APK;
            } else if ($this->detect->is('iOS')) {
                $this->deviceType = self::TYPE_APK;
            } else if($this->detect->is('SymbianOS')){
                $this->deviceType = self::TYPE_JAR;
            } else {
                if ($this->detect->mobileGrade() === 'A') {
                    $this->deviceType = self::TYPE_APK;
                } else {
                    $this->deviceType = self::TYPE_APK;
                }
            }
        } else {
            $this->deviceType = self::TYPE_PC;
        }
        //kiem tra thuoc nha mang mobiphone
//        $ipsVMS = Yii::$app->params['ip3GVMS'];
//        foreach ($ipsVMS as $ip) {
//            if ($this->cidrMatch($clientIP, $ip)) {
//                $isFind = true;
//                $network = self::_NET_VMS;
//            }
//        }
//
//        if(!$isFind) {
//            //kiem tra thuoc nha mang vinaphone
//            $ipsVNP = Yii::$app->params['ip_vinaphone'];
//            foreach ($ipsVNP as $ip) {
//                if ($this->cidrMatch($clientIP, $ip)) {
//                    $network = self::_NET_VNP;
//                    $isFind = true;
//                }
//            }
//        }
//
//        if(!$isFind) {
//            //kiem tra thuoc nha mang viettel
//            $ipsVT= Yii::$app->params['ip_viettel'];
//            foreach ($ipsVT as $ip) {
//                if ($this->cidrMatch($clientIP, $ip)) {
//                    $network = self::_NET_VIETTEL;
//                }
//            }
//        }

        $this->network = $network;
    }

    /**
     * and net with subnet mask
     *
     * @param $ip
     * @param $range
     * @return bool
     */
    public function cidrMatch($ip, $range) {
        list ($subnet, $bits) = explode('/', $range);
        $ip = ip2long($ip);
        $subnet = ip2long($subnet);
        $mask = -1 << (32 - $bits);
        $subnet &= $mask; # nb: in case the supplied subnet wasn't correctly aligned
        return ($ip & $mask) == $subnet;
    }

    /**
     * get User Ip
     * @return string
     */
    public function getUserIP() {
        if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
                $addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
                return trim($addr[0]);
            } else {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
        else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }
}