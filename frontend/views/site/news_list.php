<?php
/**
 * Created by PhpStorm.
 * User: BIOS
 * Date: 2019-04-05
 * Time: 8:40 AM
 */
use yii\helpers\Url;

/** @var $listNews*/
/** @var $paging */
/** @var $new \common\models\News*/
$lang = Yii::$app->session->get('lang', 'vi');
$more = "read more...";
$data = " News is updating...!";
if($lang == 'vi') {
    $more = "Xem tiếp...";
    $data = "Bản tin đang được cập nhật!";
}
?>

<div class="container wrap-content">
    <section class="list-news">
        <?php if(!empty($listNews)) {?>
        <?php foreach($listNews as $new) {?>
        <article class="d-flex flex-row mb-1">
            <div class="thumb-news">
                <a href="<?=Url::toRoute(['site/news-detail','name' => $new->getTitle(),'id'=> $new->id,'nid'=>$new->id])?>"><img  src="<?= $new->getThumb()?>" height="150px" alt=""></a>
            </div>

            <div class="content-news">

                <h5><?= $new->getName()?></h5>
                <p><?=$new->getShortDesc()?></p>
                <a style="float: right" href="<?=Url::toRoute(['site/news-detail','name' => $new->getTitle(),'id'=> $new->id,'nid'=>$new->id])?>"><?= $more?></a>
            </div>
        </article>
        <?php } ?>
        <?php } else {?>
            <span style="color: red"> <?= $data?></span>
        <?php } ?>


    </section>
    <?= \yii\widgets\LinkPager::widget(['pagination' => $paging]) ?>

</div>