<?php
/**
 * Created by PhpStorm.
 * User: BIOS
 * Date: 2019-04-10
 * Time: 2:14 PM
 */
use yii\helpers\Url;

$lang = Yii::$app->session->get('lang', 'vi');
$title = ($lang == 'vi') ? 'DÒNG SẢN PHẨM' : 'PRODUCT LINE';
?>

<div class="container">
    <section id="top-banner" class="profile-slider">
        <img src="/images/prof-super.png" class="img-fluid" alt="">
    </section>
    <div class="btn-product-line">
        <a href="#">
            <span><?= $title ?></span>
        </a>
    </div>

</div>

<div class="container-fluid ca-list-product">
    <section class="list-line">
        <div class="row">
            <div class="col-md-2">
                <a href="<?= Url::toRoute(['site/product', 'title' => 'ProFline - ' . \common\helpers\VietnameseUtils::removeSigns(str_replace(" ","-", $title)),'cateId'=> 7])?>">
                    <img src="/images/PROFLINE-NAUJA-300x300.png" alt="">
                </a>
            </div>
            <div class="col-md-2">
                <a href="<?= Url::toRoute(['site/product', 'title' => 'ProFline Medtechnology - ' . \common\helpers\VietnameseUtils::removeSigns(str_replace(" ","-", $title)),'cateId'=> 8])?>">
                    <img src="/images/medtech-300x300.png" alt="">
                </a>
            </div>
            <div class="col-md-2">
                <a href="#">
                    <img src="/images/premium-300x300.png" alt="">
                </a>
            </div>
            <div class="col-md-2">
                <a href="#">
                    <img src="/images/perfect-300x300.png" alt="">
                </a>
            </div>
            <div class="col-md-2">
                <a href="#">
                    <img src="/images/bebe-300x300.png" alt="">
                </a>
            </div>
            <div class="col-md-2">
                <a href="#">
                    <img src="/images/sky-pearl-300x300.png" alt="">
                </a>
            </div>
            <div class="col-md-2">
                <a href="#">
                    <img src="/images/amber-300x300.png" alt="">
                </a>
            </div>
        </div>
    </section>
</div>