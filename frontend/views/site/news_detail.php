<?php
/**
 * Created by PhpStorm.
 * User: BIOS
 * Date: 2019-04-05
 * Time: 9:00 AM
 */
/** @var $model \common\models\News */
?>
<div class="wrap-content wrap-content-detail">
    <section id="detail-product" class="container">
        <div class="col-12">
            <h1 class="product-title-detail">
                <?= $model->getName() ?>
            </h1>

            <div class="product-detail">
                <?= $model->getContent() ?>
            </div>
        </div>
    </section>

</div>