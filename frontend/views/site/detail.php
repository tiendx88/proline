<?php
/**
 * Created by PhpStorm.
 * User: BIOS
 * Date: 2019-04-04
 * Time: 11:26 AM
 */
use yii\helpers\Url;
/** @var $pro \common\models\Product */

$lang = Yii::$app->session->get('lang', 'vi');
?>

<div class="wrap-content wrap-content-detail">

<!--    <section id="breadcrumb" class="container">-->
<!--        <div class="container-fluid">-->
<!--            <div class="row">-->
<!--                <div class="col-9">-->
<!--                    <nav aria-label="breadcrumb">-->
<!--                        <ol class="breadcrumb">-->
<!--                            <li class="breadcrumb-item"><a href="#">Home</a></li>-->
<!--                            <li class="breadcrumb-item active" aria-current="page">Library</li>-->
<!--                        </ol>-->
<!--                    </nav>-->
<!--                </div>-->
<!--                <div class="col-3">-->
<!--                    <div class="another-product">-->
<!--                        <a href="#"><i class="fas fa-angle-left"></i></a>-->
<!--                        <a href="#"><i class="fas fa-angle-right"></i> </a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
    <?php if (!empty($pro)) { ?>
    <?php $images = $pro->getImageSlides(); ?>
    <section id="detail-product" class="container">
        <div class="row">
            <div class="col-md-4">
                <section class="slider">
                    <div id="slider" class="flexslider">
                        <ul class="slides">
                            <?php /** @var $img \common\models\Images */ ?>
                            <?php foreach ($images as $img) { ?>
                                <li>
                                    <img src="<?= $pro->getUrlScreenShort($img->url) ?>" <?= Yii::$app->controller->deviceType == 4 ? 'data-imagezoom="true"' : ''?>  class="img-fluid"/>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div id="carousel" class="flexslider">
                        <ul class="slides">
                            <?php foreach ($images as $img) { ?>
                                <li>
                                    <img src="<?= $pro->getUrlScreenShort($img->url) ?>" class="img-fluid"/>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </section>
            </div>
            <div class="col-md-8">
                <h1 class="product-title-detail">
                    <?= $pro->getName()?>
                </h1>
                <div class="product-detail">
                    <p><?= $pro->getSize()?></p>
                    <span>
                        <?= $pro->getFeature()?>
                    </span>
                </div>
            </div>
        </div>
    </section>
    <?php }?>
</div>
<div class="wrap-content-detail">
    <div class="container">

        <div class="row">
            <div class="col-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                            <?=$lang == 'vi' ? 'Mô tả' : 'Description'?>
                        </a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                    </li>-->
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <?= $pro->getDescription();?>
                    </div>
                    <!--<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">Koncentruotas kanalizacijos vamzdžių valiklis yra efektyvus ir veikia labai greitai – suardo plaukus, tirpdo kamščius, muilo likučius, riebalus ir kitas organines sudėtines dalis iš sifonų, vamzdžių ir kanalizacijos. Taip pat pašalina nemalonius kvapus ir veikia antiseptiškai. Produkto sudėtyje nėra chloro turinčių ingredientų – naudojant negaruoja. Reguliariai naudojant pašalina puvėsių ir kitus nemalonius kvapus.</div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">3</div>-->
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="/js/flexslider/modernizr.js"></script>
<script src="/js/imagezoom.js"></script>
<script src="/js/flexslider/jquery.flexslider.js"></script>
<!-- Syntax Highlighter -->
<script type="text/javascript" src="/js/flexslider/shCore.js"></script>
<script type="text/javascript" src="/js/flexslider/shBrushXml.js"></script>
<script type="text/javascript" src="/js/flexslider/shBrushJScript.js"></script>
<script type="text/javascript">
    $(function(){
        SyntaxHighlighter.all();
    });
    $(window).load(function(){
        $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 76,
            itemMargin: 5,
            asNavFor: '#slider'
        });

        $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>