<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

/* @var $cate \common\models\Category */
/* @var $pro\common\models\Product */
/* @var $listCate*/
/* @var $listProduct*/
/* @var $data*/
$lang = Yii::$app->session->get("lang","vi");
$this->title = 'Profline';
?>
<div class="container wrap-content">

    <section id="top-banner">
        <img src="/image/Visi-produktai-1024x299.png" class="img-fluid" alt="">
    </section>

</div>
<div class="container-fluid ca-list-product">
    <?php foreach($data as $item ) {?>
    <?php // var_dump($item['category_name']);exit;?>
    <section class="block-product">
        <div class="title-category-block">
            <div class="container">
                <h3 class="category-title"><em><?= ($lang == 'vi') ? $item['category_name'] : $item['category_name_en']?></em></h3>
            </div>
        </div>
        <div class="list-product container">
            <div class="row">
                <?php $i = 1; $count = count($item['listProduct']);?>
                <?php $col = 6;?>
                <?php if($count == 1) {
                        $col = 12;
                    } else if($count == 2) {
                        $col = 6;
                    } else if($count == 3) {
                        $col = 4;
                    } else if($count >= 4) {
                        $col = 3;
                    }

                ?>
                <?php foreach($item['listProduct'] as $pro) {?>

                    <div class="box-detail col-md-<?=$col?>">
                        <div class="detail-img">
                            <a href="<?= Url::to(['site/detail','title'=> $pro->getTitle(),'prod'=>$pro->id])?>">
                                <img src="<?= $pro->getThumb()?>" alt="">
                            </a>
                        </div>
                        <div class="product_title entry-title">
                            <span><?=$pro->getName()?></span>
                        </div>
                        <div class="product_title entry-title">
                            <span><?= $pro->size?></span>
                        </div>
                    </div>
                <?php $i++; ?>

                <?php } ?>

            </div>

        </div>
    </section>
    <?php } ?>

<!--
    <section class="block-product">
        <div class="title-category-block">
            <div class="container">
                <h3 class="category-title"><em>Thuốc diệt cỏ</em></h3>
            </div>
        </div>
        <div class="list-product container">
            <div class="row">
                <div class="box-detail col-md-12">
                    <div class="detail-img">
                        <img src="http://profline.info/wp-content/uploads/2018/03/universal%C5%ABs-223x300.png" alt="">
                    </div>
                    <div class="product_title entry-title">
                        <span>Skalbimo gelis „Universalus“</span>
                    </div>
                    <div class="product_title entry-title">
                        <span>5L, 1,5 L</span>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="block-product">
        <div class="title-category-block">
            <div class="container">
                <h3 class="category-title"><em>Thuốc diệt cỏ</em></h3>
            </div>
        </div>
        <div class="list-product container">
            <div class="row">
                <div class="box-detail col-md-4">
                    <div class="detail-img">
                        <img src="http://profline.info/wp-content/uploads/2018/03/universal%C5%ABs-223x300.png" alt="">
                    </div>
                    <div class="product_title entry-title">
                        <span>Skalbimo gelis „Universalus“</span>
                    </div>
                    <div class="product_title entry-title">
                        <span>5L, 1,5 L</span>
                    </div>
                </div>
                <div class="box-detail col-md-4">
                    <div class="detail-img">
                        <img src="http://profline.info/wp-content/uploads/2018/03/universal%C5%ABs-223x300.png" alt="">
                    </div>
                    <div class="product_title entry-title">
                        <span>Skalbimo gelis „Universalus“</span>
                    </div>
                    <div class="product_title entry-title">
                        <span>5L, 1,5 L</span>
                    </div>
                </div>
                <div class="box-detail col-md-4">
                    <div class="detail-img">
                        <img src="http://profline.info/wp-content/uploads/2018/03/universal%C5%ABs-223x300.png" alt="">
                    </div>
                    <div class="product_title entry-title">
                        <span>Skalbimo gelis „Universalus“</span>
                    </div>
                    <div class="product_title entry-title">
                        <span>5L, 1,5 L</span>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="block-product">
        <div class="title-category-block">
            <div class="container">
                <h3 class="category-title"><em>Thuốc diệt cỏ</em></h3>
            </div>
        </div>
        <div class="list-product container">
            <div class="row">
                <div class="box-detail col-md-3">
                    <div class="detail-img">
                        <img src="http://profline.info/wp-content/uploads/2018/03/universal%C5%ABs-223x300.png" alt="">
                    </div>
                    <div class="product_title entry-title">
                        <span>Skalbimo gelis „Universalus“</span>
                    </div>
                    <div class="product_title entry-title">
                        <span>5L, 1,5 L</span>
                    </div>
                </div>
                <div class="box-detail col-md-3">
                    <div class="detail-img">
                        <img src="http://profline.info/wp-content/uploads/2018/03/universal%C5%ABs-223x300.png" alt="">
                    </div>
                    <div class="product_title entry-title">
                        <span>Skalbimo gelis „Universalus“</span>
                    </div>
                    <div class="product_title entry-title">
                        <span>5L, 1,5 L</span>
                    </div>
                </div>
                <div class="box-detail col-md-3">
                    <div class="detail-img">
                        <img src="http://profline.info/wp-content/uploads/2018/03/universal%C5%ABs-223x300.png" alt="">
                    </div>
                    <div class="product_title entry-title">
                        <span>Skalbimo gelis „Universalus“</span>
                    </div>
                    <div class="product_title entry-title">
                        <span>5L, 1,5 L</span>
                    </div>
                </div>
                <div class="box-detail col-md-3">
                    <div class="detail-img">
                        <img src="http://profline.info/wp-content/uploads/2018/03/universal%C5%ABs-223x300.png" alt="">
                    </div>
                    <div class="product_title entry-title">
                        <span>Skalbimo gelis „Universalus“</span>
                    </div>
                    <div class="product_title entry-title">
                        <span>5L, 1,5 L</span>
                    </div>
                </div>
            </div>

        </div>
    </section>-->
</div>