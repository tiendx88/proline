<?php

/* @var $this yii\web\View */
/* @var $about \common\models\About */

use yii\helpers\Html;

$this->title = $about->getName();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container wrap-content">
    <section class="block-product">
        <div class="row">
            <div class="col-3">
                <div class="about-logo">
                    <img src="<?= $about->getThumb()?>" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-9">
                <div class="about-content">
                    <h4><em><?= $about->getName()?></em></h4>
                    <?= $about->getContent()?>
                </div>
            </div>
        </div>
    </section>
    <section id="partner">
        <h3><em><?= $about->getPartner()?></em></h3>
        <div class="row">
            <div class="col-3"><img src="./ab_1.png" class="img-fluid" alt=""></div>
            <div class="col-3"><img src="./ab_2.png" class="img-fluid" alt=""></div>
            <div class="col-3"><img src="./ab_3.png" class="img-fluid" alt=""></div>
            <div class="col-3"><img src="./ab_4.png" class="img-fluid" alt=""></div>
        </div>
    </section>
</div>