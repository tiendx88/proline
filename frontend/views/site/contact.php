<?php

/* @var $contact \common\models\Contacts*/
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = $contact->getTitle();
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid wrap-content">
    <section class="block-product">
        <div class="row">
            <div class="col-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.897700320606!2d105.8324560140334!3d21.03677888599402!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135aba15ec15d17%3A0x620e85c2cfe14d4c!2sHo+Chi+Minh+Mausoleum!5e0!3m2!1sen!2s!4v1554305660725!5m2!1sen!2s" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </section>
</div>
<div class="container">
    <section id="contact-detail">
        <div class="row">
            <div class="col-sm-4">
                <div class="feature-icon">
                    <i class="far fa-2x fa-paper-plane"></i>
                </div>
                <div class="feature-content">
                    <?= $contact->getContactUs()?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="feature-icon">
                    <i class="fas fa-2x fa-map-marker-alt"></i>
                </div>
                <div class="feature-content">
                    <?= $contact->getVisit()?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="feature-icon">
                    <i class="fas fa-2x fa-briefcase"></i>
                </div>
                <div class="feature-content">
                    <?= $contact->getWorkWithUs()?>
                </div>
            </div>
        </div>
    </section>
</div>