<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
$route = Yii::$app->controller->route;
$cookies = Yii::$app->request->cookies;
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<link rel="icon" href="../../../../favicon.ico">-->
    <link rel="icon" href="/image/cropped-RATAS-REME-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="/image/cropped-RATAS-REME-180x180.png" />
    <script src="/js/jquery-1.10.2.min.js"></script>
    <script src="/popper.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="/css/flexslider/flexslider.css" type="text/css" media="screen" />
    <link href="/style.css" rel="stylesheet">

</head>
<body>

<?= \app\widgets\Header::widget([]) ?>
<?= $content ?>

<footer id="footer">
    <div class="footer-bar container">
        <div class="footer-bar-content">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <img src="/image/profline-logo-1.png" alt="" width="100px">
                </div>
                <div class="footer-bar-right col-md-4 col-xs-12">
                    <ul class="footer-bar-social nav">
                        <li><a href="https://www.facebook.com/ProflineYourCleanLine/?fref=ts" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i>                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
<!--<script src="/js/jquery-1.10.2.min.js"></script>
<script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
</script>
<script src="bootstrap/js/bootstrap.min.js"></script>-->
</body>
</html>
