<?php
/**
 * Created by PhpStorm.
 * User: BIOS
 * Date: 2019-04-03
 * Time: 2:25 AM
 */
use yii\helpers\Url;
$lang = Yii::$app->session->get('lang', 'vi');
?>

<header class="fixed-top header">
    <div class="container">
        <div class="row">
            <div class="col-md-12 d-none d-lg-block large-logo">
                <a href="<?= Url::to(['site/index'],true)?>">
                    <img src="/image/profline-new.png" width="380px" alt="">
                </a>
            </div>
        </div>

        <nav class="navbar navbar-expand-lg">
            <button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="d-md-block d-lg-none">
                <a class="navbar-brand" href="<?= \yii\helpers\Url::toRoute(['site/index'])?>"><img src="/image/profline-new.png" width="100px" alt=""></a>
            </div>
            <div class="d-md-block d-lg-none">
                <a class="cart" href="#"><i class="fas fa-shopping-cart"></i></a>
            </div>
            <div id="navbarNavDropdown" class="navbar-collapse collapse">
                <ul class="navbar-nav main-menu">
                    <?php // $strUl?>
                    <?php if($lang == 'vi') {?>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= Url::toRoute(['site/index'])?>">Trang chủ <span class="sr-only">(current)</span></a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dòng sản phẩm
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?= Url::toRoute(['site/product','title'=> 'nuoc-ve-sinh','cateId' => 7])?>">Nước vệ sinh</a>
                            <a class="dropdown-item" href="<?= Url::toRoute(['site/product','title'=> 'cong-nghe-trung-gian','cateId' => 8])?>">Công nghệ trung gian</a>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<?= Url::toRoute(['site/about'])?>">Giới thiệu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= Url::toRoute(['site/news'])?>">Tin tức</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= Url::toRoute(['site/contact'])?>">Liên hệ</a>
                    </li>
                    <?php } else {//ngon ngu tieng anh?>
                        <li class="nav-item active">
                            <a class="nav-link" href="<?= Url::toRoute(['site/index'])?>">Home <span class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Product Lines
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?= Url::toRoute(['site/product','title'=> 'profline','cateId' => 7])?>">Profline</a>
                                <a class="dropdown-item" href="<?= Url::toRoute(['site/product','title'=> 'medTechnology','cateId' => 8])?>">+medTechnology</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::toRoute(['site/about'])?>">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::toRoute(['site/news'])?>">News</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::toRoute(['site/contact'])?>">Contact</a>
                        </li>
                    <?php }?>

                    <li class="nav-item">
                        <a class="nav-link" href="<?= \yii\helpers\Url::toRoute(['site/change-language','lang'=>'vi'])?>"><img src="/image/vietnam-flag-xs.png" height="16px" alt=""></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= \yii\helpers\Url::toRoute(['site/change-language','lang'=>'en'])?>"><img src="/image/united-kingdom-flag-xs.png"  height="16px" alt=""></a>
                    </li>
                    <li class="nav-item d-none d-lg-block">
                        <a class="nav-link" href="#"><i class="fas fa-shopping-cart"></i>
                        </a>
                    </li>

                </ul>
            </div>
        </nav>
    </div>
</header>