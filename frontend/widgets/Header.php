<?php
/**
 * Created by PhpStorm.
 * User: Tien
 * Date: 10/18/14
 * Time: 4:55 PM
 */

namespace app\widgets;
use common\models\Keywords;
use common\models\Menu;
use yii\base\Exception;
use yii\bootstrap\Widget;

class Header extends Widget{

    public $category = null;
    public $strUl = '';

    public function init() {
        try {
            $m = new Menu();
            $aryCategories = Menu::find()->andWhere(['status' => Menu::STATUS_ACTIVE])->orderBy('weight asc, category_name asc, parent_id asc')->all();
            if (!empty($aryCategories)) {
                $parentGroup = $m->groupParent($aryCategories);//get group parent
                $listData = $m->groupParentOfParent($parentGroup);//buil tree
                $m->buildCategories($this->strUl, $listData);
            }
        } catch (Exception $ex) {
        }

    }

    public function run() {
        $listMenu = Menu::getListCateParent();
        return $this->render('//header/header', [
            'listCategory' => $this->category,
            'strUl' => $this->strUl,
        ]);
    }
} 